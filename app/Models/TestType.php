<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\TestType
 *
 * @property int id
 * @property string $name
 * @property boolean $active
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class TestType extends Model
{
    use CrudTrait;
    protected $fillable = [
        'name',
        'active',
    ];

    public function scopeActive($query)
    {
        return $query->whereActive(true);
    }
}
