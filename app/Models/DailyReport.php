<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Diameter
 *
 * @property int id
 * @property double|null rnp_plan
 * @property double|null rnp_fact
 * @property double|null m15_plan
 * @property double|null m15_fact
 * @property double|null kit_plan
 * @property double|null kit_fact
 * @property double|null fyks_weght
 * @property double|null rnp_weght
 * @property double|null china_weght
 * @property double|null weight_weght
 * @property double|null gomo_1_1_plane
 * @property double|null gomo_1_1_output
 * @property double|null gomo_1_1_shelf
 * @property double|null gomo_1_2_plane
 * @property double|null gomo_1_2_output
 * @property double|null gomo_1_2_shelf
 * @property double|null gomo_2_1_plane
 * @property double|null gomo_2_1_output
 * @property double|null gomo_2_1_shelf
 * @property double|null gomo_2_2_plane
 * @property double|null gomo_2_2_output
 * @property double|null gomo_2_2_shelf
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon $date
 * @property double|null consumptions_pillars_mg
 * @property double|null consumptions_pillars_si
 * @property double|null consumptions_pillars_cu
 * @property double|null consumptions_ingots_mg
 * @property double|null consumptions_ingots_si
 * @property double|null consumptions_ingots_cu
 */
class DailyReport extends Model
{
    protected $fillable = [
        'rnp_plan',
        'rnp_fact',
        'm15_plan',
        'm15_fact',
        'kit_plan',
        'kit_fact',
        'fyks_weght',
        'rnp_weght',
        'china_weght',
        'weight_weght',
        'gomo_1_1_plane',
        'gomo_1_1_output',
        'gomo_1_1_shelf',
        'gomo_1_2_plane',
        'gomo_1_2_output',
        'gomo_1_2_shelf',
        'gomo_2_1_plane',
        'gomo_2_1_output',
        'gomo_2_1_shelf',
        'gomo_2_2_plane',
        'gomo_2_2_output',
        'gomo_2_2_shelf',
        'date',
        'consumptions_pillars_mg',
        'consumptions_pillars_si',
        'consumptions_pillars_cu',
        'consumptions_ingots_mg',
        'consumptions_ingots_si',
        'consumptions_ingots_cu',
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'date' => 'datetime',
    ];
}
