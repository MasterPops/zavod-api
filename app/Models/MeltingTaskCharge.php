<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MeltingTaskCharge extends Model
{
    protected $table = 'melting_task_charge';

    protected $fillable = [
        'melting_task_id',
        'charge_id',
        'weight',
    ];

    public function charge()
    {
        return $this->belongsTo(Charge::class, 'charge_id');
    }
}
