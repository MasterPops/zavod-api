<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\MeltingTaskUser
 *
 * @property int id
 * @property int melting_task_id
 * @property int user_id
 * @property string role
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class MeltingTaskUser extends Model
{
    protected $fillable = [
        'melting_task_id',
        'user_id',
        'role',
    ];

    public function melting_task()
    {
        return $this->belongsTo(MeltingTask::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
