<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\AdditionalWork
 *
 * @property int id
 * @property string $name
 * @property boolean $active
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class AdditionalWork extends Model
{
    use CrudTrait;
    protected $fillable = [
        'name',
        'active',
        'materials',
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function scopeActive($query)
    {
        return $query->whereActive(true);
    }
}
