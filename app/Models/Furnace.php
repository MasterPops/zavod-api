<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * App\Models\Furnace
 *
 * @property int id
 * @property string $name
 * @property boolean $active
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class Furnace extends Model
{
    use CrudTrait, SoftDeletes;
    protected $fillable = [
        'name',
        'active',
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function scopeActive($query)
    {
        return $query->whereActive(true);
    }
}
