<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Batch
 *
 * @property int id
 * @property int batch_id
 * @property int number
 * @property double weight
 * @property string passport
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class ChargeBatch extends Model
{
    protected $fillable = [
        'batch_id',
        'number',
        'weight',
        'passport',
    ];

    protected $table = 'charge_batch';

    public function batch()
    {
        return $this->belongsTo(Batch::class, 'batch_id');
    }
}
