<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\CostAdditionalMaterial
 *
 * @property int id
 * @property int $melting_task_id
 * @property int $additional_work_id
 * @property string $name
 * @property string $measure
 * @property double|null $value
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class CostAdditionalMaterial extends Model
{
    protected $fillable = [
        'melting_task_id',
        'additional_work_id',
        'name',
        'value',
        'measure',
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function additionalWork()
    {
        return $this->belongsTo(AdditionalWork::class, 'additional_work_id');
    }
}
