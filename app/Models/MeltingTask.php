<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\MeltingTask
 *
 * @property int id
 * @property string|null $number
 * @property int|null $daily_melting_number
 * @property int $furnace_id
 * @property int $master_id
 * @property Carbon|null $planned_melting_date
 * @property Carbon|null $melting_date
 * @property Carbon|null $melt_down_to
 * @property int $product_type_id
 * @property int $chemical_composition_id
 * @property int|null $mark_id
 * @property int|null $month_number
 * @property boolean $weight
 * @property boolean|null $card_created
 * @property string $status
 * @property double output_weight
 * @property double|null output_alloy_grade_percent
 * @property double|null output_trim_weight
 * @property double|null output_trim_percent
 * @property double|null output_reject1_weight
 * @property double|null output_reject1_percent
 * @property double|null output_cut_count
 * @property double|null output_cut_weight
 * @property double|null output_cut_percent
 * @property double|null output_reject2_weight
 * @property double|null output_reject2_percent
 * @property double|null output_shavings_weight
 * @property double|null output_shavings_percent
 * @property double|null output_black_weight
 * @property double|null output_black_percent
 * @property double|null output_white_weight
 * @property double|null output_white_percent
 * @property double|null output_trash_weight
 * @property double|null output_trash_percent
 * @property double|null output_irrevocably_weight
 * @property double|null output_irrevocably_percent
 * @property double|null loss_sum_weight
 * @property double|null loss_sum_percent
 * @property double|null overall_result
 * @property string|null note_black
 * @property string|null note_white
 * @property string|null note_fe
 * @property boolean|null al_l
 * @property boolean|null si_l
 * @property boolean|null fe_l
 * @property boolean|null cu_l
 * @property boolean|null mn_l
 * @property boolean|null mg_l
 * @property boolean|null zn_l
 * @property boolean|null ti_l
 * @property boolean|null ni_l
 * @property boolean|null pb_l
 * @property boolean|null sn_l
 * @property boolean|null cr_l
 * @property boolean|null cd_l
 * @property boolean|null proportions_l
 * @property string brand
 * @property string series
 * @property string|null reason_rejection
 * @property string|null events
 * @property double|null porosity
 * @property string|null slag
 * @property int output_alloy_grade_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $melting_start_at
 * @property Carbon|null $overflow_start_at
 * @property Carbon|null $melting_done_at
 * @property Carbon|null $finished_at
 */
class MeltingTask extends Model
{
    public const PLANNING_STATUS = 'planning';
    public const PREPARATION_STATUS = 'preparation';
    public const STARTED_STATUS = 'started';
    public const OVERFLOW_STATUS = 'overflow';
    public const COMPLETED_STATUS = 'completed';
    public const FAIL_STATUS = 'fail';
    public const GOMO_STATUS = 'gomo';
    public const CANCELLED_STATUS = 'cancelled';

    public const STATUSES = [
        self::PLANNING_STATUS,
        self::PREPARATION_STATUS,
        self::STARTED_STATUS,
        self::OVERFLOW_STATUS,
        self::COMPLETED_STATUS,
        self::FAIL_STATUS,
        self::GOMO_STATUS,
        self::CANCELLED_STATUS,
    ];

    public const STATUSES_FOR_INTERFACE = [
        self::PLANNING_STATUS => 'Планировние',
        self::PREPARATION_STATUS => 'Подготовка',
        self::STARTED_STATUS => 'Печь. Начата плавка',
        self::OVERFLOW_STATUS => 'Печь. Перелив',
        self::COMPLETED_STATUS => 'Плавка завершена',
        self::FAIL_STATUS => 'Авария',
        self::GOMO_STATUS => 'Гомо',
        self::CANCELLED_STATUS => 'Отмена',
    ];

    protected $fillable = [
        'number',
        'daily_melting_number',
        'furnace_id',
        'month_number',
        'planned_melting_date',
        'melting_date',
        'melt_down_to',
        'product_type_id',
        'chemical_composition_id',
        'mark_id',
        'filter',
        'weight',
        'status',
        'note_black',
        'note_white',
        'note_fe',
        'output_weight',
        'finished_at',
        'output_alloy_grade_percent',
        'output_trim_weight',
        'loss_sum_weight',
        'loss_sum_percent',
        'overall_result',
        'output_trim_percent',
        'output_reject1_weight',
        'output_reject1_percent',
        'output_cut_count',
        'output_cut_weight',
        'output_cut_percent',
        'output_reject2_weight',
        'output_reject2_percent',
        'output_shavings_weight',
        'output_shavings_percent',
        'master_id',
        'brand',
        'diameter_id',
        'series',
        'output_alloy_grade_id',
        'card_created',
        'output_black_weight',
        'output_black_percent',
        'output_white_weight',
        'output_white_percent',
        'output_trash_weight',
        'output_trash_percent',
        'output_irrevocably_weight',
        'output_irrevocably_percent',
        'melting_done_at',
        'overflow_start_at',
        'melting_start_at',
        'al_l',
        'si_l',
        'fe_l',
        'cu_l',
        'mn_l',
        'mg_l',
        'zn_l',
        'ti_l',
        'ni_l',
        'pb_l',
        'sn_l',
        'cr_l',
        'cd_l',
        'proportions_l',
        'reason_rejection',
        'events',
        'porosity',
        'slag',
    ];

    protected $casts = [
        'melting_date' => 'datetime',
        'planned_melting_date' => 'datetime',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'melting_done_at' => 'datetime',
        'overflow_start_at' => 'datetime',
        'melting_start_at' => 'datetime',
    ];

    public function furnace(): BelongsTo
    {
        return $this->belongsTo(Furnace::class);
    }

    public function product_type(): BelongsTo
    {
        return $this->belongsTo(ProductType::class);
    }

    public function chemical_composition(): BelongsTo
    {
        return $this->belongsTo(ChemicalComposition::class);
    }

    public function mark(): BelongsTo
    {
        return $this->belongsTo(Mark::class);
    }

    public function generateNumber(): string
    {
        $monthAndYear = Carbon::now()->format('my');

        $lastCurrentMonthTask = self::query()
            ->whereProductTypeId($this->product_type_id)
            ->where('created_at', '>=', now()->startOfMonth())
            ->where('created_at', '<=', now()->endOfMonth())
            ->orderByDesc('month_number')
            ->first();


        $number = 1;

        if (!empty($lastCurrentMonthTask)) {
            $number = $lastCurrentMonthTask->month_number;
        }

        $number++;

        $this->month_number = $number;
        $this->save();

        $code = $this->product_type->code;

        if ($number < 10) {
            $number = '0' . $number;
        } elseif ($number > 99 && $this->product_type->increment_code) {
            $code = (int)$code + (int)(floor($number / 100));
            $number = (int)($number - 100 * floor($number / 100));
            if ($number == 0) {
                $number++;
            }
            if ($number < 10) {
                $number = '0' . $number;
            }
        }


        return $monthAndYear . $code . $number;
    }

    public function charges(): BelongsToMany
    {
        return $this->belongsToMany(Charge::class, 'melting_task_charge')->withPivot([
            'weight'
        ]);
    }

    public function batches()
    {
        return $this->hasMany(Batch::class);
    }

    public function output_alloy_grade()
    {
        return $this->belongsTo(AlloyGrade::class, 'output_alloy_grade_id');
    }

    public function master()
    {
        return $this->belongsTo(User::class, 'master_id');
    }

    public function users()
    {
        return $this->hasMany(MeltingTaskUser::class, 'melting_task_id');
    }

    public function materials(): BelongsToMany
    {
        return $this->belongsToMany(Material::class, 'melting_task_material')->withPivot([
            'value'
        ]);
    }

    public function diameter()
    {
        return $this->belongsTo(Diameter::class, 'diameter_id');
    }

    public function additionalMaterials()
    {
        return $this->hasMany(CostAdditionalMaterial::class, 'melting_task_id');
    }

    public function energyParameters()
    {
        return $this->hasMany(CostEnergyParameter::class, 'melting_task_id');
    }

    public function tests()
    {
        return $this->hasMany(ChemicalTest::class, 'melting_task_id');
    }
}
