<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\ChemicalComposition
 *
 * @property int id
 * @property int $technical_condition_id
 * @property int $alloy_grade_id
 * @property int $diameter_id
 * @property string $al
 * @property string $si
 * @property string $fe
 * @property string $cu
 * @property string $mn
 * @property string $mg
 * @property string $zn
 * @property string $ti
 * @property string $ni
 * @property string $pb
 * @property string $sn
 * @property string $cr
 * @property string $cd
 * @property string $proportions
 * @property string $description
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class ChemicalComposition extends Model
{
    const CHEMICAL_ELEMENTS = [
        'al',
        'si',
        'fe',
        'cu',
        'mn',
        'mg',
        'zn',
        'ti',
        'ni',
        'pb',
        'sn',
        'cr',
        'cd',
    ];
    public const MIN_DIA_VALUE = 1;
    public const MAX_DIA_VALUE = 1000;
    protected $fillable = [
        'technical_condition_id',
        'alloy_grade_id',
        'diameter_id',
        'al',
        'si',
        'fe',
        'cu',
        'mn',
        'mg',
        'zn',
        'ti',
        'ni',
        'pb',
        'sn',
        'cr',
        'cd',
        'proportions',
        'description',
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function technical_condition(): BelongsTo
    {
        return $this->belongsTo(TechnicalCondition::class);
    }

    public function alloy_grade(): BelongsTo
    {
        return $this->belongsTo(AlloyGrade::class);
    }

    public function diameter(): BelongsTo
    {
        return $this->belongsTo(Diameter::class);
    }

    public static function technical_condition_options(): array
    {
        return TechnicalCondition::query()->active()->pluck('name', 'id')->all();
    }

    public static function diameter_options(): array
    {
        return Diameter::query()->active()->pluck('name', 'id')->all();
    }
}
