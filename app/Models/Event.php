<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'user_id',
        'object_name',
        'event_name',
        'value_before',
        'value_after',
    ];

    protected $casts = [
        'created_at' => 'datetime',
    ];

    public const OBJECT_PROD_PLAN = 'План производства';
    public const OBJECT_MELT_CARD = 'Карта плавки';
    public const OBJECT_CHEMICAL = 'Хим. состав';
    public const OBJECT_DAILY_REPORT = 'Суточный отчет';
    public const OBJECT_ADMIN_PANEL = 'Справочники';

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public static function add(int $userId, string|null $objectName, string|null $eventName, string|null $valueBefore, string|null $valueAfter)
    {
        self::create([
            'user_id' => $userId,
            'object_name' => $objectName,
            'event_name' => $eventName,
            'value_before' => $valueBefore,
            'value_after' => $valueAfter,
        ]);
    }
}
