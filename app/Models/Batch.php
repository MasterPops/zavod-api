<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Batch
 *
 * @property int id
 * @property int melting_task_id
 * @property int charge_id
 * @property double clog
 * @property string passport
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class Batch extends Model
{
    protected $fillable = [
        'melting_task_id',
        'charge_id',
        'clog',
        'passport',
    ];

    public function melting_task()
    {
        return $this->belongsTo(MeltingTask::class);
    }

    public function charge()
    {
        return $this->belongsTo(Charge::class);
    }

    public function loads()
    {
        return $this->hasMany(ChargeBatch::class, 'batch_id');
    }
}
