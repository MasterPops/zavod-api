<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\ChemicalTest
 *
 * @property int id
 * @property string $al
 * @property string $si
 * @property string $fe
 * @property string $cu
 * @property string $mn
 * @property string $mg
 * @property string $zn
 * @property string $ti
 * @property string $ni
 * @property string $pb
 * @property string $sn
 * @property string $cr
 * @property string $cd
 * @property string $proportions
 * @property string $description
 * @property int $alloy_grade_id
 * @property int $melting_task_id
 * @property int $test_type_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class ChemicalTest extends Model
{
    protected $fillable = [
        'al',
        'si',
        'fe',
        'cu',
        'mn',
        'mg',
        'zn',
        'ti',
        'ni',
        'pb',
        'sn',
        'cr',
        'cd',
        'proportions',
        'description',
        'alloy_grade_id',
        'melting_task_id',
        'test_type_id',
    ];

    public function alloy_grade()
    {
        return $this->belongsTo(AlloyGrade::class, 'alloy_grade_id');
    }

    public function melting_task()
    {
        return $this->belongsTo(MeltingTask::class, 'melting_task_id');
    }

    public function test_type()
    {
        return $this->belongsTo(TestType::class, 'test_type_id');
    }
}
