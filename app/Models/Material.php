<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Material
 *
 * @property int id
 * @property string $name
 * @property boolean $active
 * @property boolean $display_measure
 * @property boolean $coef
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class Material extends Model
{
    use CrudTrait;

    protected $fillable = [
        'name',
        'active',
        'measure',
        'display_measure',
        'coef',
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public const MEASURE_KG = 'кг';
    public const MEASURE_L = 'л';

    public function scopeActive($query)
    {
        return $query->whereActive(true);
    }
}
