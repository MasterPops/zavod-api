<?php

namespace App\Models;

use App\Scopes\ActiveScope;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * App\Models\AlloyGrade
 *
 * @property int id
 * @property string $name
 * @property boolean $active
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class AlloyGrade extends Model
{
    use CrudTrait, SoftDeletes;

    protected $fillable = [
        'name',
        'active',
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function scopeActive($query)
    {
        return $query->whereActive(true);
    }

    public function getChemicalCompositionsAttribute(): bool|string
    {
        $result = [];
        $chemicalCompositions = ChemicalComposition::whereAlloyGradeId($this->id)->get();

        $chemicalCompositions->each(function ($chemicalComposition) use (&$result) {
            $result[] = $chemicalComposition->toArray();
        });

        return json_encode($result);
    }

    public function getChemicalCompositionsList()
    {
        $technicalConditionIds = ChemicalComposition::whereAlloyGradeId($this->id)
            ->pluck('technical_condition_id')
            ->all();

        return implode(',', TechnicalCondition::whereIn('id', $technicalConditionIds)
            ->pluck('name')
            ->all());
    }
}
