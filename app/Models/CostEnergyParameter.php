<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\CostAdditionalMaterial
 *
 * @property int id
 * @property int $melting_task_id
 * @property int $energy_parameter_id
 * @property string $name
 * @property string $measure
 * @property double|null $start_value
 * @property double|null $end_value
 * @property double|null $difference
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class CostEnergyParameter extends Model
{
    protected $fillable = [
        'melting_task_id',
        'energy_parameter_id',
        'name',
        'start_value',
        'end_value',
        'difference',
        'measure',
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function energyParameter()
    {
        return $this->belongsTo(EnergyParameter::class, 'energy_parameter_id');
    }
}
