<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\ProductType
 *
 * @property int id
 * @property string $name
 * @property boolean $active
 * @property string $code
 * @property boolean $increment_code
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class ProductType extends Model
{
    use CrudTrait;
    protected $fillable = [
        'name',
        'active',
        'code',
        'increment_code',
    ];

    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    public function scopeActive($query)
    {
        return $query->whereActive(true);
    }
}
