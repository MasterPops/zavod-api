<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

/**
 * App\Models\Furnace
 *
 * @property int id
 * @property string $first_name
 * @property string $last_name
 * @property string $second_name
 * @property boolean $active
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles, SoftDeletes, CrudTrait;

    /**
     * Roles
     */
    public const ADMIN_ROLE = 'admin';
    public const DIRECTOR_ROLE = 'director';
    public const MASTER_ROLE = 'master';
    public const TECHNOLOGIST_ROLE = 'technologist';
    public const SMELTER_ROLE = 'smelter';
    public const LABORATORY_ROLE = 'laboratory';
    public const DRIVER_ROLE = 'driver';

    public const ROLES = [
        self::ADMIN_ROLE,
        self::DIRECTOR_ROLE,
        self::MASTER_ROLE,
        self::TECHNOLOGIST_ROLE,
        self::SMELTER_ROLE,
        self::LABORATORY_ROLE,
        self::DRIVER_ROLE,
    ];

    public const ROLES_FOR_FRONT = [
        self::ADMIN_ROLE => 'Администратор',
        self::DIRECTOR_ROLE => 'Директор',
        self::MASTER_ROLE => 'Мастер',
        self::TECHNOLOGIST_ROLE => 'Технолог',
        self::SMELTER_ROLE => 'Плавильщик',
        self::LABORATORY_ROLE => 'Лаборатория',
        self::DRIVER_ROLE => 'Водитель погрузчика',
    ];

    /**
     * Permissons
     */

    public const READ_PRODUCTION_PLANE_PERMISSION = 'read_production_plane';
    public const WRITE_PRODUCTION_PLANE_PERMISSION = 'write_production_plane';
    public const READ_MELTING_CARD_PERMISSION = 'read_melting_card';
    public const WRITE_MELTING_CARD_PERMISSION = 'write_melting_card';
    public const READ_DAILY_REPORT_PERMISSION = 'read_daily_report';
    public const WRITE_DAILY_REPORT_PERMISSION = 'write_daily_report';
    public const READ_CHEMICAL_COMPOSITION_PERMISSION = 'read_chemical_composition';
    public const WRITE_CHEMICAL_COMPOSITION_PERMISSION = 'write_chemical_composition';

    public const PERMISSONS = [
        self::READ_PRODUCTION_PLANE_PERMISSION,
        self::WRITE_PRODUCTION_PLANE_PERMISSION,
        self::READ_MELTING_CARD_PERMISSION,
        self::WRITE_MELTING_CARD_PERMISSION,
        self::READ_DAILY_REPORT_PERMISSION,
        self::WRITE_DAILY_REPORT_PERMISSION,
        self::READ_CHEMICAL_COMPOSITION_PERMISSION,
        self::WRITE_CHEMICAL_COMPOSITION_PERMISSION,
    ];

    public const ADMIN_PERMISSIONS = self::PERMISSONS;
    public const DIRECTOR_PERMISSIONS = [
        self::READ_PRODUCTION_PLANE_PERMISSION,
        self::READ_MELTING_CARD_PERMISSION,
        self::READ_DAILY_REPORT_PERMISSION,
        self::READ_CHEMICAL_COMPOSITION_PERMISSION,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'second_name',
        'first_name',
        'last_name',
        'active',
        'email',
        'password',
    ];

    public const MASTER_PERMISSIONS = [
        self::READ_PRODUCTION_PLANE_PERMISSION,
        self::READ_MELTING_CARD_PERMISSION,
        self::READ_DAILY_REPORT_PERMISSION,
        self::READ_CHEMICAL_COMPOSITION_PERMISSION,
        self::WRITE_MELTING_CARD_PERMISSION,
        self::WRITE_DAILY_REPORT_PERMISSION,
    ];

    public const TECHNOLOGIST_PERMISSIONS = [
        self::READ_PRODUCTION_PLANE_PERMISSION,
        self::WRITE_PRODUCTION_PLANE_PERMISSION,
    ];

    public const LABORATORY_PERMISSIONS = [
        self::READ_CHEMICAL_COMPOSITION_PERMISSION,
        self::WRITE_CHEMICAL_COMPOSITION_PERMISSION,
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
        'created_at' => 'datetime',
        'update_at' => 'datetime',
    ];

    public static function handleRolesForInterface(): array
    {
        $roles = [];

        foreach (self::ROLES as $role) {
            $roles[$role] = trans('model/role.' . $role);
        }

        return $roles;
    }

    public static function handlePermissionsForInterface(): array
    {
        $permissions = [];

        foreach (self::PERMISSONS as $permission) {
            $permissions[$permission] = trans('model/permission.' . $permission);
        }

        return $permissions;
    }

    public function scopeActive($query)
    {
        return $query->whereActive(true);
    }

    public function getFioAttribute(): string
    {
        return $this->second_name . ' ' . mb_substr($this->first_name, 0, 1) . '.' . mb_substr($this->last_name, 0, 1) . '.';
    }

    public function getInitialsAttribute(): string
    {
        return mb_substr($this->first_name, 0, 1) . '.' . mb_substr($this->last_name, 0, 1) . '.';
    }

    public function getRoles()
    {
        $roles = $this->roles->pluck('name')->all();

        $result = [];

        if (!empty($roles)) {
            foreach ($roles as $role) {
                $result[] = User::ROLES_FOR_FRONT[$role];
            }
        }
        return implode(', ', $result);
    }
}
