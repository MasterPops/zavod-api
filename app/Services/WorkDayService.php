<?php

namespace App\Services;

use Carbon\Carbon;

class WorkDayService
{
    public static function getStartDay(Carbon $date = null): Carbon
    {
        if (!$date) {
            $date = now();
        }

        $date1 = $date2 = $date;

        $date1 = $date1->startOfDay()->addHours(9);

        $date2 = $date2->startOfDay()->subDay()->addHours(9);

        if ($date < $date1) {
            $nowStartDay = $date2;
        } else {
            $nowStartDay = $date1;
        }

        return $nowStartDay;
    }

    public static function getEndDay(Carbon $date = null): Carbon
    {
        if (!$date) {
            $date = now();
        }

        $date1 = $date2 = $date;

        $date1 = $date2 = $date;

        $date1 = $date1->startOfDay()->addHours(9);

        $date2 = $date2->startOfDay()->addDay()->addHours(9);

        if ($date < $date1) {
            $nowEndDay = $date1;
        } else {
            $nowEndDay = $date2;
        }

        return $nowEndDay;
    }
}
