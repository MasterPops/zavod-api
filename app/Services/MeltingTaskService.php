<?php

namespace App\Services;

use App\Models\Batch;
use App\Models\MeltingTask;

class MeltingTaskService
{
    public static function calculateWeightAndPercent(int $meltingTaskId)
    {
        $meltingTask = MeltingTask::find($meltingTaskId);

        $batches = Batch::with('loads')
            ->whereMeltingTaskId($meltingTaskId)
            ->get();

        $chargeWeight = 0;

        if (!empty($batches)) {
            $batches->each(function ($batch) use (&$chargeWeight) {
                $batch->loads->each(function ($load) use (&$chargeWeight) {
                    $chargeWeight += $load->weight;
                });
            });
        }

        if ($chargeWeight) {
            $meltingTask->output_alloy_grade_percent = round($meltingTask->output_weight / $chargeWeight * 100, 4);
            $meltingTask->output_trim_percent = round($meltingTask->output_trim_weight / $chargeWeight * 100, 4);
            $meltingTask->output_reject1_percent = round($meltingTask->output_reject1_weight / $chargeWeight * 100, 4);
            $meltingTask->output_reject2_percent = round($meltingTask->output_reject2_weight / $chargeWeight * 100, 4);
            $meltingTask->output_cut_percent = round($meltingTask->output_cut_weight / $chargeWeight * 100, 4);
            $meltingTask->output_shavings_percent = round($meltingTask->output_shavings_weight / $chargeWeight * 100, 4);
            $meltingTask->output_black_percent = round($meltingTask->output_black_weight / $chargeWeight * 100, 4);
            $meltingTask->output_white_percent = round($meltingTask->output_white_weight / $chargeWeight * 100, 4);
            $meltingTask->output_trash_percent = round($meltingTask->output_trash_weight / $chargeWeight * 100, 4);
//            $meltingTask->output_irrevocably_percent = round($meltingTask->output_irrevocably_weight / $chargeWeight * 100, 4);
        }

        $meltingTask->loss_sum_weight = $meltingTask->output_black_weight + $meltingTask->output_white_weight
            + $meltingTask->output_trash_weight + $meltingTask->output_irrevocably_weight;
        $meltingTask->loss_sum_percent = $meltingTask->output_black_percent + $meltingTask->output_white_percent
            + $meltingTask->output_trash_percent + $meltingTask->output_irrevocably_percent;

        $meltingTask->overall_result = $meltingTask->loss_sum_weight + $meltingTask->output_weight
            + $meltingTask->output_trim_weight + $meltingTask->output_reject1_weight
            + $meltingTask->output_reject2_weight + $meltingTask->output_cut_weight
            + $meltingTask->output_shavings_weight;

        $meltingTask->save();
    }

    public static function getPlaneMvg(int $meltingTaskId)
    {
        $batches = Batch::with('loads')
            ->whereMeltingTaskId($meltingTaskId)
            ->get();

        $mvg = 0;
        $chargeWeight = 0;

        if (!empty($batches)) {
            $batches->each(function ($batch) use (&$mvg, &$chargeWeight) {
                $chargeWeight += $batch->loads()->sum('weight');
                $mvg += $batch->loads()->sum('weight') - ($batch->loads()->sum('weight') * ($batch->clog / 100));
            });
        }

        if ($chargeWeight && $mvg) {
            return round($mvg / $chargeWeight * 100, 4);
        } else {
            return 0;
        }

    }

    public static function getFactMvg(int $meltingTaskId)
    {
        $meltingTask = MeltingTask::find($meltingTaskId);

        return $meltingTask->output_alloy_grade_percent + $meltingTask->output_trim_percent
            + $meltingTask->output_reject1_percent + $meltingTask->output_reject2_percent
            + $meltingTask->output_cut_percent + $meltingTask->output_shavings_percent;
    }

    public static function getLossSum(int $meltingTaskId)
    {
        $batches = Batch::with('loads')
            ->whereMeltingTaskId($meltingTaskId)
            ->get();

        $chargeWeight = 0;

        if (!empty($batches)) {
            $batches->each(function ($batch) use (&$mvg, &$chargeWeight) {
                $chargeWeight += $batch->loads()->sum('weight');
            });
        }

        return round($chargeWeight * (MeltingTaskService::getPlaneMvg($meltingTaskId) - MeltingTaskService::getFactMvg($meltingTaskId)) /100, 4);
    }

    public static function getNewLoadNumber(int $meltingTaskId)
    {
        $batches = Batch::with('loads')
            ->whereMeltingTaskId($meltingTaskId)
            ->get();

        $maxNumber = 0;

        if (!empty($batches)) {
            $batches->each(function ($batch) use (&$maxNumber) {
                if ($batch->loads()->max('number') > $maxNumber)
                    $maxNumber = $batch->loads()->max('number');
            });
        }
        return $maxNumber + 1;
    }

    public static function gerChargeSum(int $meltingTaskId)
    {
        $batches = Batch::with('loads')
            ->whereMeltingTaskId($meltingTaskId)
            ->get();

        $chargeWeight = 0;

        if (!empty($batches)) {
            $batches->each(function ($batch) use (&$mvg, &$chargeWeight) {
                $chargeWeight += $batch->loads()->sum('weight');
            });
        }

        return $chargeWeight;
    }
}
