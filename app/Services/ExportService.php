<?php

namespace App\Services;

use App\Http\Resources\MeltingTaskDailyReportResource;
use App\Models\DailyReport;
use App\Models\Furnace;
use App\Models\MeltingTask;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;

class ExportService
{
    public static function exportDailyReport(DailyReport $dailyReport): \Symfony\Component\HttpFoundation\StreamedResponse
    {
        $templateFileName = resource_path() . '/xls/report.xlsx';

        $reader = new Xlsx();

        $spreadsheet = $reader->load($templateFileName);
        $worksheet = $spreadsheet->getActiveSheet();

        $worksheet->setCellValue('B6', $dailyReport->date->format('d.m.Y'));
        $worksheet->setCellValue('B8', Auth::user()->fio ?? 'Test. t.t.');


        $row = 10;

        $furnaces = Furnace::query()->active()->get();

        $furnacesData = [];

        $furnaces->each(function ($furnace) use ($dailyReport, &$furnacesData) {
            $furnacesData[$furnace->name] = [
                'fact' => MeltingTask::where('melting_done_at', '>=', Carbon::parse($dailyReport->date)->startOfDay())
                    ->where('melting_done_at', '<=', Carbon::parse($dailyReport->date)->endOfDay())
                    ->whereFurnaceId($furnace->id)
                    ->count(),
                'planned' => MeltingTask::where('planned_melting_date', '>=', Carbon::parse($dailyReport->date)->startOfDay())
                    ->where('planned_melting_date', '<=', Carbon::parse($dailyReport->date)->endOfDay())
                    ->whereFurnaceId($furnace->id)
                    ->count(),
            ];
        });

//        dd($furnacesData);

        $column = 'A';
        $worksheet->insertNewRowBefore($row);

        foreach ($furnacesData as $key => $furnace) {
            $cellColumnPlan = $column;
            $cellColumnFact = $column;
            $cellColumnFact++;

            self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
                $column++ . $row . ':' . $column++ . $row,
            ], 'A', 'Q');

            $worksheet->setCellValue($cellColumnPlan . $row, $key . ' всего:');

            $worksheet->getStyle($cellColumnPlan . $row)
                ->getFont()
                ->setBold(true)
                ->setSize(12)
                ->setName('Arial');

            $worksheet->setCellValue($cellColumnPlan . $row + 1, 'План');
            $worksheet->setCellValue($cellColumnFact . $row + 1, 'Факт');

            $worksheet->setCellValue($cellColumnPlan . $row + 2, $furnace['planned']);
            $worksheet->setCellValue($cellColumnFact . $row + 2, $furnace['fact']);

            $worksheet->getStyle($cellColumnPlan . $row . ':' . $cellColumnFact . $row + 2)
                ->getBorders()
                ->getAllBorders()
                ->setBorderStyle(Border::BORDER_THIN)
                ->setColor(new Color('000000'));

            $worksheet->getStyle($cellColumnPlan . $row . ':' . $cellColumnFact . $row + 2)
                ->getAlignment()
                ->setHorizontal(Alignment::HORIZONTAL_CENTER)
                ->setVertical(Alignment::VERTICAL_CENTER)
                ->setWrapText(true);

            $worksheet->getStyle($cellColumnPlan . $row + 1 . ':' . $cellColumnFact . $row + 2)
                ->getFont()
                ->setSize(12)
                ->setName('Arial');
        }


        $meltingTasks = MeltingTask::query()
            ->where(function ($q) use ($dailyReport) {
                $q->where('melting_done_at', '>=', Carbon::parse($dailyReport->date)->startOfDay())
                    ->where('melting_done_at', '<=', Carbon::parse($dailyReport->date)->endOfDay())
                    ->whereStatus(MeltingTask::COMPLETED_STATUS);
            })->orWhere(function ($q) use ($dailyReport) {
                $q->where('planned_melting_date', '>=', Carbon::parse($dailyReport->date)->startOfDay())
                    ->where('planned_melting_date', '<=', Carbon::parse($dailyReport->date)->endOfDay())
                    ->whereNot('status', MeltingTask::COMPLETED_STATUS);
            })->get()->groupBy('furnace.name');

        $row = 13;
        $row++;

        $worksheet->insertNewRowBefore($row);

        if (!empty($meltingTasks)) {
            foreach ($meltingTasks as $key => $tasks) {
                self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
                    'A' . $row . ':B' . $row,
                ], 'A', 'B');

                $worksheet->setCellValue('A' . $row, $key);
                $worksheet->getStyle('A' . $row)
                    ->getFont()
                    ->setBold(true)
                    ->setSize(14)
                    ->setName('Arial');

                $worksheet->getStyle('A' . $row . ':B' . $row)
                    ->getBorders()
                    ->getAllBorders()
                    ->setBorderStyle(Border::BORDER_THIN)
                    ->setColor(new Color('000000'));

                $row++;
                $worksheet->insertNewRowBefore($row);
                $worksheet->insertNewRowBefore($row);
                $worksheet->insertNewRowBefore($row);

                self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
                    'A' . $row . ':A' . $row + 1,
                ], 'A', 'A');
                self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
                    'B' . $row . ':B' . $row + 1,
                ], 'B', 'B');
                self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
                    'C' . $row . ':C' . $row + 1,
                ], 'C', 'C');
                self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
                    'D' . $row . ':D' . $row + 1,
                ], 'D', 'D');
                self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
                    'E' . $row . ':E' . $row + 1,
                ], 'E', 'E');
                self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
                    'G' . $row . ':G' . $row + 1,
                ], 'G', 'G');
                self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
                    'F' . $row . ':F' . $row + 1,
                ], 'F', 'F');
                self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
                    'I' . $row . ':K' . $row,
                ], 'I', 'K');
                self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
                    'H' . $row . ':H' . $row + 1,
                ], 'H', 'H');
                self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
                    'L' . $row . ':L' . $row + 1,
                ], 'L', 'L');
                self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
                    'M' . $row . ':M' . $row + 1,
                ], 'M', 'M');
                self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
                    'N' . $row . ':N' . $row + 1,
                ], 'N', 'N');
                self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
                    'O' . $row . ':O' . $row + 1,
                ], 'O', 'O');
                self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
                    'P' . $row . ':R' . $row + 1,
                ], 'P', 'R');
                self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
                    'S' . $row . ':S' . $row + 1,
                ], 'S', 'S');

                $worksheet->setCellValue('A' . $row, 'Номер');
                $worksheet->setCellValue('B' . $row, 'Статус');
                $worksheet->setCellValue('C' . $row, 'ID задания');
                $worksheet->setCellValue('D' . $row, '№ плавки');
                $worksheet->setCellValue('E' . $row, 'План');
                $worksheet->setCellValue('F' . $row, 'Факт');
                $worksheet->setCellValue('G' . $row, 'Вылет по хим.');
                $worksheet->setCellValue('H' . $row, 'Вес,кг');
                $worksheet->setCellValue('I' . $row, 'МВГ, %');
                $worksheet->setCellValue('I' . $row + 1, 'План');
                $worksheet->setCellValue('J' . $row + 1, 'Факт');
                $worksheet->setCellValue('K' . $row + 1, 'Разница');
                $worksheet->setCellValue('L' . $row, 'Причина отклонения МВГф от плана');
                $worksheet->setCellValue('M' . $row, 'Мероприятия');
                $worksheet->setCellValue('N' . $row, 'Балл пор.');
                $worksheet->setCellValue('O' . $row, 'Шлак (Ж/Н/С)');
                $worksheet->setCellValue('P' . $row, 'Поставщик шихты');
                $worksheet->setCellValue('S' . $row, 'Ф.И.О плавильщика');

                $worksheet->getStyle('A' . $row . ':S' . $row + 1)
                    ->getAlignment()
                    ->setHorizontal(Alignment::HORIZONTAL_CENTER)
                    ->setVertical(Alignment::VERTICAL_CENTER)
                    ->setWrapText(true);

                $worksheet->getStyle('A' . $row . ':S' . $row + 1)
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('d0d0d0');

                $worksheet->getStyle('A' . $row . ':S' . $row + 1)
                    ->getFont()
                    ->setBold(true)
                    ->setSize(12)
                    ->setName('Arial');

                $worksheet->getStyle('A' . $row . ':S' . $row + 1)
                    ->getBorders()
                    ->getAllBorders()
                    ->setBorderStyle(Border::BORDER_THIN)
                    ->setColor(new Color('000000'));

                $row++;
                $row++;

                $number = 1;

                foreach ($tasks as $task) {
                    $worksheet->insertNewRowBefore($row);
                    $test = $task->tests()->orderByDesc('created_at')->first();

                    $chemMismatch = [];

                    if (!empty($test)) {
                        $chemicalComposition = $task->chemical_composition;

                        $elements = [
                            'al',
                            'si',
                            'fe',
                            'cu',
                            'mn',
                            'mg',
                            'zn',
                            'ti',
                            'ni',
                            'pb',
                            'sn',
                            'cr',
                            'cd',
                        ];

                        foreach ($elements as $element) {
                            $ccValue = mb_split('-', $chemicalComposition->$element);
                            if (count($ccValue) == 1) {
                                if ((double)$test->$element - (double)$ccValue[0] != 0) {
                                    $chemMismatch[ucfirst($element)] = (double)$test->$element - (double)$ccValue[0];
                                }
                            } elseif (count($ccValue) == 2) {
                                if ((double)$test->$element < (double)$ccValue[0]) {
                                    $chemMismatch[ucfirst($element)] = (double)$test->$element - (double)$ccValue[0];
                                } elseif ((double)$test->$element > (double)$ccValue[1]) {
                                    $chemMismatch[ucfirst($element)] = (double)$test->$element - (double)$ccValue[1];
                                }
                            }
                        }
                    }

                    $chemMismatchString = [];

                    foreach ($chemMismatch as $key => $value) {
                        $chemMismatchString[] = $key . ': '. $value;
                    }

                    $batches = [];

                    if (!empty($task->batches)) {
                        foreach ($task->batches as $batch) {
                            $batches[] = $batch->charge->name;
                        }
                    }

                    $smelters = [];

                    if (!empty($task->users)) {
                        foreach ($task->users as $user) {
                            if ($user->role == 'smelter') {
                                $smelters[] = $user->user->fio;
                            }
                        }
                    }

                    self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
                        'P' . $row . ':R' . $row,
                    ], 'P', 'R');

                    $worksheet->setCellValue('A' . $row, $number++);
                    $worksheet->setCellValue('B' . $row, MeltingTask::STATUSES_FOR_INTERFACE[$task->status]);
                    $worksheet->setCellValue('C' . $row, $task->id);
                    $worksheet->setCellValue('D' . $row, $task->number);
                    $worksheet->setCellValue('E' . $row, optional($task->chemical_composition->alloy_grade)->name);
                    $worksheet->setCellValue('F' . $row, $task->status == MeltingTask::COMPLETED_STATUS ? optional($task->output_alloy_grade)->name : '-');
                    $worksheet->setCellValue('G' . $row, $task->status == MeltingTask::COMPLETED_STATUS ? implode(', ', $chemMismatchString) : '');
                    $worksheet->setCellValue('H' . $row, $task->output_weight);
                    $worksheet->setCellValue('I' . $row, $task->status == MeltingTask::COMPLETED_STATUS ? MeltingTaskService::getPlaneMvg($task->id) : '');
                    $worksheet->setCellValue('J' . $row, $task->status == MeltingTask::COMPLETED_STATUS ? MeltingTaskService::getFactMvg($task->id) : '');
                    $worksheet->setCellValue('K' . $row, $task->status == MeltingTask::COMPLETED_STATUS ? round(MeltingTaskService::getPlaneMvg($task->id) - MeltingTaskService::getFactMvg($task->id), 2) * -1 : '');
                    $worksheet->setCellValue('L' . $row, $task->status == MeltingTask::COMPLETED_STATUS ? $task->reason_rejection : '');
                    $worksheet->setCellValue('M' . $row, $task->status == MeltingTask::COMPLETED_STATUS ? $task->events : '');
                    $worksheet->setCellValue('N' . $row, $task->status == MeltingTask::COMPLETED_STATUS ? $task->porosity : '');
                    $worksheet->setCellValue('O' . $row, $task->status == MeltingTask::COMPLETED_STATUS ? $task->slag : '');
                    $worksheet->setCellValue('P' . $row, implode(', ', $batches));
                    $worksheet->setCellValue('S' . $row, implode(', ', $smelters));

                    $worksheet->getStyle('A' . $row . ':S' . $row + 1)
                        ->getAlignment()
                        ->setHorizontal(Alignment::HORIZONTAL_CENTER)
                        ->setVertical(Alignment::VERTICAL_CENTER)
                        ->setWrapText(true);

                    $worksheet->getStyle('A' . $row . ':S' . $row)
                        ->getFill()
                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('ffffff');

                    $worksheet->getStyle('A' . $row . ':S' . $row)
                        ->getFont()
                        ->setBold(false)
                        ->setSize(12)
                        ->setName('Arial');

                    $worksheet->getStyle('A' . $row+1 . ':S' . $row+1)
                        ->getBorders()
                        ->getAllBorders()
                        ->setBorderStyle(Border::BORDER_NONE);

                    $worksheet->getStyle('A' . $row . ':S' . $row)
                        ->getBorders()
                        ->getAllBorders()
                        ->setBorderStyle(Border::BORDER_THIN)
                        ->setColor(new Color('000000'));
                    $row++;
                }

                $worksheet->insertNewRowBefore($row);
                
                $row++;
            }

        }

        $row++;

        $worksheet->setCellValue('C' . $row, $dailyReport->fyks_weght);
        $worksheet->setCellValue('E' . $row, $dailyReport->rnp_weght);
        $worksheet->setCellValue('G' . $row, $dailyReport->china_weght);
        $worksheet->setCellValue('I' . $row, $dailyReport->weight_weght);

        $row++;
        $row++;
        $row++;

        $worksheet->setCellValue('G' . $row, $dailyReport->consumptions_pillars_mg);
        $worksheet->setCellValue('H' . $row, $dailyReport->consumptions_pillars_si);
        $worksheet->setCellValue('I' . $row, $dailyReport->consumptions_ingots_cu);

        $row++;

        $worksheet->setCellValue('G' . $row, $dailyReport->consumptions_ingots_mg);
        $worksheet->setCellValue('H' . $row, $dailyReport->consumptions_ingots_si);
        $worksheet->setCellValue('I' . $row, $dailyReport->consumptions_ingots_cu);

        $row++;

        $worksheet->setCellValue('B' . $row, $dailyReport->gomo_1_1_plane);
        $worksheet->setCellValue('C' . $row, $dailyReport->gomo_1_1_output);
        $worksheet->setCellValue('D' . $row, $dailyReport->gomo_1_1_shelf);

        $row++;

        $worksheet->setCellValue('B' . $row, $dailyReport->gomo_1_2_plane);
        $worksheet->setCellValue('C' . $row, $dailyReport->gomo_1_2_output);
        $worksheet->setCellValue('D' . $row, $dailyReport->gomo_1_2_shelf);

        $row++;

        $worksheet->setCellValue('B' . $row, $dailyReport->gomo_2_1_plane);
        $worksheet->setCellValue('C' . $row, $dailyReport->gomo_2_1_output);
        $worksheet->setCellValue('D' . $row, $dailyReport->gomo_2_1_shelf);

        $row++;

        $worksheet->setCellValue('B' . $row, $dailyReport->gomo_2_2_plane);
        $worksheet->setCellValue('C' . $row, $dailyReport->gomo_2_2_output);
        $worksheet->setCellValue('D' . $row, $dailyReport->gomo_2_2_shelf);

//

//        $worksheet->getStyle('A' . $row . ':O' . $row)
//            ->getFont()
//            ->setBold(true)
//            ->setSize(10)
//            ->setName('Arial');
//
//        $worksheet->getStyle('A' . $row . ':O' . $row)
//            ->getBorders()
//            ->getAllBorders()
//            ->setBorderStyle(Border::BORDER_THIN)
//            ->setColor(new Color('000000'));
//
//        $worksheet->getStyle('A' . $row . ':N' . $row)
//            ->getAlignment()
//            ->setHorizontal(Alignment::HORIZONTAL_CENTER)
//            ->setVertical(Alignment::VERTICAL_CENTER)
//            ->setWrapText(true);
//
//        $worksheet->setCellValue('A10', Auth::user()->fio ?? 'Test. t.t.');


        for ($i = 1; $i <= $row; $i++) {
            $worksheet->getRowDimension($i)->setRowHeight(-1);
        }

        $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');

        $publicPath = Storage::disk('public')->path('xls/');

        if (!file_exists($publicPath)) {
            mkdir($publicPath);
        }

        $fileName = now()->format('d.m.Y_i_s_') . 'daily-report' . '.xlsx';

        $objWriter->save($publicPath . $fileName);

//        return Storage::disk('public')->url('xls/' . $fileName);
        return Storage::download('public/xls/' . $fileName);
    }

    public static function exportProdPlan(): \Symfony\Component\HttpFoundation\StreamedResponse
    {
        $overdueMeltingTasks = MeltingTask::query()
            ->whereStatus(MeltingTask::PLANNING_STATUS)
            ->where('planned_melting_date', '<', now()->startOfDay())
            ->orderBy('daily_melting_number')
            ->get()
            ->groupBy('furnace.name');

        $plannedMeltingTasks = [];

        $dates = DB::table('melting_tasks')
            ->select('planned_melting_date')
            ->where('status', MeltingTask::PLANNING_STATUS)
            ->where('planned_melting_date', '>=', now()->startOfDay())
            ->groupBy('planned_melting_date')
            ->pluck('planned_melting_date')
            ->all();

        if (!empty($dates)) {
            foreach ($dates as $date) {
                $plannedMeltingTasks[Carbon::parse($date)->format('d.m.Y')] = MeltingTask::query()
                    ->where('planned_melting_date', Carbon::parse($date))
                    ->whereStatus(MeltingTask::PLANNING_STATUS)
                    ->orderBy('daily_melting_number')
                    ->get()
                    ->groupBy('furnace.name');
            }
        } else {
            for ($i = 0; $i < 1; $i++) {
                $plannedMeltingTasks[now()->addDays($i)->format('d.m.Y')] = MeltingTask::query()
                    ->where('planned_melting_date', '>=', now()->startOfDay()->addDays($i))
                    ->where('planned_melting_date', '<', now()->endOfDay()->addDays($i))
                    ->whereStatus(MeltingTask::PLANNING_STATUS)
                    ->orderBy('daily_melting_number')
                    ->get()
                    ->groupBy('furnace.name');
            }
        }

        $templateFileName = resource_path() . '/xls/prodplan.xlsx';

        $reader = new Xlsx();

        $spreadsheet = $reader->load($templateFileName);
        $worksheet = $spreadsheet->getActiveSheet();

        $worksheet->setCellValue('B5', now()->format('d.m.Y'));

        $row = 7;

        if (!empty($overdueMeltingTasks)) {
            $worksheet->setCellValue('A' . $row, 'Просроченные ');

            $worksheet->getStyle('A' . $row)
                ->getFont()
                ->setBold(true)
                ->setSize(13)
                ->setName('Arial');

            $row++;

            foreach ($overdueMeltingTasks as $furnace => $tasks) {
                $worksheet->setCellValue('A' . $row, $furnace);

                $worksheet->getStyle('A' . $row)
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('d0d0d0');

                $worksheet->getStyle('A' . $row)
                    ->getFont()
                    ->setBold(true)
                    ->setSize(14)
                    ->setName('Arial');

                $worksheet->getStyle('A' . $row)
                    ->getBorders()
                    ->getAllBorders()
                    ->setBorderStyle(Border::BORDER_THIN)
                    ->setColor(new Color('000000'));

                $row++;
                self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
                    'A' . $row . ':B' . $row,
                ], 'A', 'B');
                $worksheet->setCellValue('A' . $row, '№ пл.');
                $worksheet->setCellValue('C' . $row, 'ПЕЧЬ');
                $worksheet->setCellValue('D' . $row, 'id Задания');
                $worksheet->setCellValue('E' . $row, 'Статус');
                $worksheet->setCellValue('F' . $row, '№ Карта пл.');
                $worksheet->setCellValue('G' . $row, 'Плановая дата плавки');
                $worksheet->setCellValue('H' . $row, 'Тип изделия');
                $worksheet->setCellValue('I' . $row, 'Марка сплава');
                $worksheet->setCellValue('J' . $row, 'Диметр');
                $worksheet->setCellValue('K' . $row, 'ГОСТ / ТУ');
                $worksheet->setCellValue('L' . $row, 'Маркировка');
                $worksheet->setCellValue('M' . $row, 'Фильтр');
                $worksheet->setCellValue('N' . $row, 'Вес ГП, кг');
                $worksheet->setCellValue('O' . $row, 'Шихта');

                $worksheet->getStyle('A' . $row . ':O' . $row)
                    ->getAlignment()
                    ->setHorizontal(Alignment::HORIZONTAL_CENTER)
                    ->setVertical(Alignment::VERTICAL_CENTER)
                    ->setWrapText(true);

                $worksheet->getStyle('A' . $row . ':O' . $row)
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setARGB('d0d0d0');

                $worksheet->getStyle('A' . $row . ':O' . $row)
                    ->getFont()
                    ->setBold(true)
                    ->setSize(12)
                    ->setName('Arial');

                $worksheet->getStyle('A' . $row . ':O' . $row)
                    ->getBorders()
                    ->getAllBorders()
                    ->setBorderStyle(Border::BORDER_THIN)
                    ->setColor(new Color('000000'));

                $row++;

                foreach ($tasks as $task) {
                    $charges = [];

                    if (!empty($task->charges)) {
                        $task->charges->each(function ($charge) use (&$charges) {
                            $charges[] = $charge->name . '- ' . $charge->pivot->weight . ' кг';
                        });
                    }

                    $worksheet->setCellValue('A' . $row, $task->daily_melting_number);
                    $worksheet->setCellValue('C' . $row, optional($task->furnace)->name);
                    $worksheet->setCellValue('D' . $row, $task->id);
                    $worksheet->setCellValue('E' . $row, MeltingTask::STATUSES_FOR_INTERFACE[$task->status]);
                    $worksheet->setCellValue('F' . $row, $task->number);
                    $worksheet->setCellValue('G' . $row, $task->planned_melting_date ? $task->planned_melting_date->format('d.m.Y') : '-');
                    $worksheet->setCellValue('H' . $row, optional($task->product_type)->name);
                    $worksheet->setCellValue('I' . $row, optional($task->chemical_composition->alloy_grade)->name);
                    $worksheet->setCellValue('J' . $row, optional($task->diameter)->name);
                    $worksheet->setCellValue('K' . $row, optional($task->chemical_composition->technical_condition)->name);
                    $worksheet->setCellValue('L' . $row, optional($task->mark)->name);
                    $worksheet->setCellValue('M' . $row, $task->filter ? 'Да' : 'Нет');
                    $worksheet->setCellValue('N' . $row, $task->weight);
                    $worksheet->setCellValue('O' . $row, implode(', ', $charges));

                    self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
                        'A' . $row . ':B' . $row,
                    ], 'A', 'B');
                    $worksheet->getStyle('A' . $row . ':O' . $row)
                        ->getFont()
                        ->setBold(true)
                        ->setSize(10)
                        ->setName('Arial');

                    $worksheet->getStyle('A' . $row . ':O' . $row)
                        ->getBorders()
                        ->getAllBorders()
                        ->setBorderStyle(Border::BORDER_THIN)
                        ->setColor(new Color('000000'));

                    $worksheet->getStyle('A' . $row . ':N' . $row)
                        ->getAlignment()
                        ->setHorizontal(Alignment::HORIZONTAL_CENTER)
                        ->setVertical(Alignment::VERTICAL_CENTER)
                        ->setWrapText(true);

                    $worksheet->getStyle('O' . $row)
                        ->getAlignment()
                        ->setWrapText(true);

                    $row++;
                }
            }

            $row++;
        }

        if (!empty($plannedMeltingTasks)) {
            $worksheet->setCellValue('A' . $row, 'Запланированные ');

            $worksheet->getStyle('A' . $row)
                ->getFont()
                ->setBold(true)
                ->setSize(13)
                ->setName('Arial');

            $row++;

            foreach ($plannedMeltingTasks as $date => $furnace) {
                foreach ($furnace as $key => $f) {
                    $worksheet->setCellValue('A' . $row, $key);
                    $worksheet->getStyle('A' . $row)
                        ->getFill()
                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('d0d0d0');

                    $worksheet->getStyle('A' . $row)
                        ->getFont()
                        ->setBold(true)
                        ->setSize(14)
                        ->setName('Arial');

                    $worksheet->getStyle('A' . $row)
                        ->getBorders()
                        ->getAllBorders()
                        ->setBorderStyle(Border::BORDER_THIN)
                        ->setColor(new Color('000000'));

                    $row++;

                    $worksheet->setCellValue('A' . $row, 'Дата');
                    $worksheet->setCellValue('B' . $row, '№ пл.');
                    $worksheet->setCellValue('C' . $row, 'ПЕЧЬ');
                    $worksheet->setCellValue('D' . $row, 'id Задания');
                    $worksheet->setCellValue('E' . $row, 'Статус');
                    $worksheet->setCellValue('F' . $row, '№ Карта пл.');
                    $worksheet->setCellValue('G' . $row, 'Плановая дата плавки');
                    $worksheet->setCellValue('H' . $row, 'Тип изделия');
                    $worksheet->setCellValue('I' . $row, 'Марка сплава');
                    $worksheet->setCellValue('J' . $row, 'Диметр');
                    $worksheet->setCellValue('K' . $row, 'ГОСТ / ТУ');
                    $worksheet->setCellValue('L' . $row, 'Маркировка');
                    $worksheet->setCellValue('M' . $row, 'Фильтр');
                    $worksheet->setCellValue('N' . $row, 'Вес ГП, кг');
                    $worksheet->setCellValue('O' . $row, 'Шихта');

                    $worksheet->getStyle('A' . $row . ':O' . $row)
                        ->getAlignment()
                        ->setHorizontal(Alignment::HORIZONTAL_CENTER)
                        ->setVertical(Alignment::VERTICAL_CENTER)
                        ->setWrapText(true);

                    $worksheet->getStyle('A' . $row . ':O' . $row)
                        ->getFill()
                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('d0d0d0');

                    $worksheet->getStyle('A' . $row . ':O' . $row)
                        ->getFont()
                        ->setBold(true)
                        ->setSize(12)
                        ->setName('Arial');

                    $worksheet->getStyle('A' . $row . ':O' . $row)
                        ->getBorders()
                        ->getAllBorders()
                        ->setBorderStyle(Border::BORDER_THIN)
                        ->setColor(new Color('000000'));

                    $row++;

                    $startDateRow = $row;

                    foreach ($f as $task) {
                        $charges = [];

                        if (!empty($task->charges)) {
                            $task->charges->each(function ($charge) use (&$charges) {
                                $charges[] = $charge->name . '- ' . $charge->pivot->weight . ' кг';
                            });
                        }

                        $worksheet->setCellValue('B' . $row, $task->daily_melting_number);
                        $worksheet->setCellValue('C' . $row, optional($task->furnace)->name);
                        $worksheet->setCellValue('D' . $row, $task->id);
                        $worksheet->setCellValue('E' . $row, MeltingTask::STATUSES_FOR_INTERFACE[$task->status]);
                        $worksheet->setCellValue('F' . $row, $task->number);
                        $worksheet->setCellValue('G' . $row, $task->planned_melting_date ? $task->planned_melting_date->format('d.m.Y') : '-');
                        $worksheet->setCellValue('H' . $row, optional($task->product_type)->name);
                        $worksheet->setCellValue('I' . $row, optional($task->chemical_composition->alloy_grade)->name);
                        $worksheet->setCellValue('J' . $row, optional($task->diameter)->name);
                        $worksheet->setCellValue('K' . $row, optional($task->chemical_composition->technical_condition)->name);
                        $worksheet->setCellValue('L' . $row, optional($task->mark)->name);
                        $worksheet->setCellValue('M' . $row, $task->filter ? 'Да' : 'Нет');
                        $worksheet->setCellValue('N' . $row, $task->weight);
                        $worksheet->setCellValue('O' . $row, implode(', ', $charges));

                        $worksheet->getStyle('B' . $row . ':N' . $row)
                            ->getAlignment()
                            ->setHorizontal(Alignment::HORIZONTAL_CENTER)
                            ->setVertical(Alignment::VERTICAL_CENTER)
                            ->setWrapText(true);

                        $worksheet->getStyle('O' . $row)
                            ->getAlignment()
                            ->setWrapText(true);

                        $worksheet->getStyle('A' . $row . ':O' . $row)
                            ->getFont()
                            ->setBold(true)
                            ->setSize(10)
                            ->setName('Arial');

                        $worksheet->getStyle('A' . $row . ':O' . $row)
                            ->getBorders()
                            ->getAllBorders()
                            ->setBorderStyle(Border::BORDER_THIN)
                            ->setColor(new Color('000000'));
                        $row++;
                    }

                    self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
                        'A' . $startDateRow . ':A' . $row - 1,
                    ], 'A', 'A');

                    $worksheet->setCellValue('A' . $startDateRow, $date);
                    $worksheet->getStyle('A' . $startDateRow)
                        ->getAlignment()
                        ->setHorizontal(Alignment::HORIZONTAL_CENTER)
                        ->setVertical(Alignment::VERTICAL_CENTER);
                }

            }
        }

        for ($i = 1; $i <= $row; $i++) {
            $worksheet->getRowDimension($i)->setRowHeight(-1);
        }

        $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');

        $publicPath = Storage::disk('public')->path('xls/');

        if (!file_exists($publicPath)) {
            mkdir($publicPath);
        }

        $fileName = now()->format('d.m.Y_i_s_') . 'prodplan' . '.xlsx';

        $objWriter->save($publicPath . $fileName);

//        return Storage::disk('public')->url('xls/' . $fileName);
        return Storage::download('public/xls/' . $fileName);
    }

    /**
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public static function exportMeltingCard(int $meltingCardId): \Symfony\Component\HttpFoundation\StreamedResponse
    {
        $styleArray = array(
            'borders' => array(
                'outline' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '000000'),
                ),
            ),
        );

        $meltCard = MeltingTask::query()
            ->with([
                'chemical_composition.alloy_grade',
                'batches.loads',
                'tests',
                'materials',
                'additionalMaterials',
                'energyParameters',
            ])->findOrFail($meltingCardId);

        $templateFileName = resource_path() . '/xls/meltcard.xlsx';

        $reader = new Xlsx();

        $spreadsheet = $reader->load($templateFileName);
        $worksheet = $spreadsheet->getActiveSheet();

        $worksheet->setCellValue('C5', $meltCard->number);

        $worksheet->setCellValue('C8', optional($meltCard->chemical_composition->alloy_grade)->name);
        $worksheet->setCellValue('E8', optional($meltCard->product_type)->name);
        $worksheet->setCellValue('G8', optional($meltCard->diameter)->name);
        $worksheet->setCellValue('I8', optional($meltCard->chemical_composition->technical_condition)->name);
        $worksheet->setCellValue('K8', optional($meltCard->mark)->name);
        $worksheet->setCellValue('M8', $meltCard->filter ? 'Да' : 'Нет');
        $worksheet->setCellValue('N8', $meltCard->weight ? $meltCard->weight . ' кг' : '');


        $worksheet->setCellValue('C11', optional($meltCard->output_alloy_grade)->name);
        $worksheet->setCellValue('E11', $meltCard->output_weight);
        $worksheet->setCellValue('G11', round(MeltingTaskService::getPlaneMvg($meltCard->id) - MeltingTaskService::getFactMvg($meltCard->id), 2));
        $worksheet->setCellValue('I11', $meltCard->brand);
        $worksheet->setCellValue('K11', $meltCard->series);


        $worksheet->setCellValue('C13', $meltCard->number);
        $worksheet->setCellValue('C14', $meltCard->planned_melting_date ? $meltCard->planned_melting_date->format('d.m.Y') : '');
        $worksheet->setCellValue('C15', $meltCard->melting_start_at ? $meltCard->melting_start_at->format('d.m.Y') : '');
        $worksheet->setCellValue('C16', $meltCard->melting_done_at ? $meltCard->melting_done_at->format('d.m.Y') : '');

        $worksheet->setCellValue('C18', optional($meltCard->master)->fio);

        $row = 18;

        if (!empty($meltCard->users)) {
            $meltCard->users->each(function ($user) use (&$row, &$worksheet) {
                if (!empty($user->user)) {
                    $row++;

                    $worksheet->insertNewRowBefore($row);
                    self::copyExcelRowFull($worksheet, $worksheet, $row - 1, $row, [
                        'A' . $row . ':B' . $row,
                        'C' . $row . ':D' . $row,
                    ]);

                    $worksheet->setCellValue('A' . $row, User::ROLES_FOR_FRONT[$user->role]);
                    $worksheet->setCellValue('C' . $row, optional($user->user)->fio);
                }

            });
        }

        $row += 2;

        if (!empty($meltCard->charges)) {
            $charges = [];

            $meltCard->charges->each(function ($charge) use (&$charges) {
                $charges[] = $charge->name . ' - ' . $charge->pivot->weight;
            });

            $worksheet->setCellValue('C' . $row, implode(', ', $charges));
        }

        $row += 5;

        $chargeRows = $row;

        if (!$meltCard->batches->isEmpty()) {
            $firstBatch = true;

            $meltCard->batches->each(function ($batch) use ($chargeRows, &$row, &$worksheet, &$firstBatch) {
                if (!$batch->loads->isEmpty()) {
                    if (!$firstBatch) {
                        $worksheet->insertNewRowBefore($row);
                        $row++;

                        self::copyExcelRowFull($worksheet, $worksheet, 25, $row - 1);

                        self::copyExcelRowFull($worksheet, $worksheet, $chargeRows, $row, [
                            'B' . $row . ':F' . $row,
                        ]);

                        $worksheet->insertNewRowBefore($row + 1);
                        self::copyExcelRowFull($worksheet, $worksheet, $chargeRows + 1, $row + 1, [
                            'B' . $row + 1 . ':C' . $row + 1,
                            'D' . $row + 1 . ':E' . $row + 1,
                        ]);

                        $worksheet->insertNewRowBefore($row + 2);
                        self::copyExcelRowFull($worksheet, $worksheet, $chargeRows + 2, $row + 2, [
                            'B' . $row + 2 . ':C' . $row + 2,
                            'D' . $row + 2 . ':E' . $row + 2,
                        ]);

                    } else {
                        $firstBatch = false;
                    }

                    $worksheet->setCellValue('B' . $row, optional($batch->charge)->name);
                    $row += 2;

                    $firstRow = true;

                    $batch->loads->each(function ($load) use ($batch, &$row, &$firstRow, &$worksheet) {
                        if (!$firstRow) {
                            $worksheet->insertNewRowBefore($row);
                            self::copyExcelRowFull($worksheet, $worksheet, $row - 1, $row, [
                                'A' . $row . ':B' . $row,
                                'C' . $row . ':D' . $row,
                            ]);
                        } else {
                            $firstRow = false;

                        }

                        $worksheet->setCellValue('A' . $row, $load->number);
                        $worksheet->setCellValue('B' . $row, $load->weight);
                        $worksheet->setCellValue('D' . $row, $load->passport);
                        $worksheet->setCellValue('F' . $row, $batch->clog);
                        $row++;
                    });
                }
                $worksheet->insertNewRowBefore($row);
                self::copyExcelRowFull($worksheet, $worksheet, 25, $row);
            });
        } else {
            $row += 2;
        }

        $row += 2;


        if (!empty($meltCard->tests)) {
            $elementCells = [
                'C' => 'al_l',
                'D' => 'si_l',
                'E' => 'fe_l',
                'F' => 'cu_l',
                'G' => 'mn_l',
                'H' => 'mg_l',
                'I' => 'zn_l',
                'J' => 'ti_l',
                'K' => 'ni_l',
                'L' => 'pb_l',
                'M' => 'sn_l',
                'N' => 'cr_l',
                'O' => 'cd_l',
            ];

            foreach ($elementCells as $cell => $element) {
                if ($meltCard->$element) {
                    $spreadsheet->getActiveSheet()
                        ->getStyle($cell . $row)
                        ->getFill()
                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('d0d0d0');
                }
            }

            $firstRow = true;
            $row++;

            $elementCells = [
                'C' => 'al',
                'D' => 'si',
                'E' => 'fe',
                'F' => 'cu',
                'G' => 'mn',
                'H' => 'mg',
                'I' => 'zn',
                'J' => 'ti',
                'K' => 'ni',
                'L' => 'pb',
                'M' => 'sn',
                'N' => 'cr',
                'O' => 'cd',
            ];
            $startChemRow = $row;

            $meltCard->tests->each(function ($test) use ($elementCells, $meltCard, &$row, &$firstRow, &$worksheet, &$spreadsheet) {

                if (!$firstRow) {
                    $worksheet->insertNewRowBefore($row);
                    self::copyExcelRowFull($worksheet, $worksheet, $row - 1, $row);
                } else {
                    $firstRow = false;
                }


                $worksheet->setCellValue('A' . $row, $test->created_at->format('d.m.Y H:i'));
                $worksheet->setCellValue('B' . $row, optional($test->test_type)->name);

                foreach ($elementCells as $cell => $element) {
                    $worksheet->setCellValue($cell . $row, str_replace(',', '.', $test->$element));

                }
                $worksheet->setCellValue('P' . $row, $test->proportions);
                $worksheet->setCellValue('R' . $row, optional($test->alloy_grade)->name);
                $worksheet->setCellValue('S' . $row, $test->description);

                foreach ($elementCells as $cell => $element) {
                    $ccElement = explode('-', optional($meltCard->chemical_composition)->$element);

                    if (count($ccElement) == 1) {
                        if ($test->$element > str_replace(',', '.', $ccElement[0]) || $test->$element < str_replace(',', '.', $ccElement[0])) {
                            $spreadsheet->getActiveSheet()
                                ->getStyle($cell . $row)
                                ->getFill()
                                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                ->getStartColor()->setARGB('d0d0d0');
                        }
                    } elseif (count($ccElement) == 2) {
                        if ($test->$element < str_replace(',', '.', $ccElement[0]) || $test->$element > str_replace(',', '.', $ccElement[1])) {
                            $spreadsheet->getActiveSheet()
                                ->getStyle($cell . $row)
                                ->getFill()
                                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                                ->getStartColor()->setARGB('d0d0d0');
                        }
                    }
                }


                $row++;
            });

            self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
                'Q' . $startChemRow . ':Q' . $row-1,
            ], 'Q', 'Q');
            $worksheet->setCellValue('Q' . $startChemRow, $meltCard->porosity);

            $worksheet->getStyle('Q' . $startChemRow . ':Q' . $row-1)
                ->getFont()
                ->setBold(true)
                ->setSize(12)
                ->setName('Arial');

            $worksheet->getStyle('Q' . $startChemRow . ':Q' . $row-1)
                ->getAlignment()
                ->setHorizontal(Alignment::HORIZONTAL_CENTER)
                ->setVertical(Alignment::VERTICAL_CENTER);

            foreach ($elementCells as $cell => $element) {
                $worksheet->setCellValue($cell . $row, str_replace(',', '.', optional($meltCard->chemical_composition)->$element));
            }

            $worksheet->setCellValue('P' . $row, optional($meltCard->chemical_composition)->propotions);
            $worksheet->setCellValue('R' . $row, optional($meltCard->chemical_composition->alloy_grade)->name);
            $worksheet->setCellValue('S' . $row, optional($meltCard->chemical_composition)->description);

        }

        $row += 6;

        $startMaterialRow = $row;

        $maxRows = $row + 5;

        $materials = [
            'total' => 0,
            'l' => 0,
        ];
        $chargeSum = 0;


        if (!empty($meltCard->materials)) {

            $firstRow = true;
            $meltCard->materials->each(function ($material) use (&$firstRow, &$chargeSum, &$worksheet, &$row, &$materials, &$maxRows, $styleArray) {
                $materials['total'] += $material->pivot->value * $material->coef;

                if ($material->measure == 'л') {
                    $materials['l'] += $material->pivot->value;
                }
                self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
                    'A' . $row . ':B' . $row,
                ], 'A', 'D');


                $worksheet->setCellValue('A' . $row, $material->name);
                $worksheet->setCellValue('C' . $row, $material->pivot->value);
                $worksheet->setCellValue('D' . $row, $material->display_measure ?? $material->measure);
                $worksheet->getStyle('A' . $row . ':D' . $row)
                    ->getBorders()
                    ->getAllBorders()
                    ->setBorderStyle(Border::BORDER_THIN)
                    ->setColor(new Color('000000'));

                $worksheet->getStyle('A' . $row . ':D' . $row)
                    ->getFont()
                    ->setBold(true)
                    ->setSize(11)
                    ->setName('Arial');

                $worksheet->getStyle('C' . $row . ':D' . $row)
                    ->getAlignment()
                    ->setHorizontal(Alignment::HORIZONTAL_CENTER);

                $worksheet->getStyle('A' . $row . ':B' . $row)
                    ->getAlignment();
//                    ->setHorizontal(Alignment::HORIZONTAL_LEFT);

                $row++;
                if ($row > $maxRows) {
                    $maxRows = $row;
                }
            });

            self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
                'A' . $row . ':B' . $row + 1,
            ], 'A', 'D');

            $worksheet->setCellValue('A' . $row, 'ИТОГО');
            $worksheet->setCellValue('C' . $row, round($materials['total'], 2));
            $worksheet->setCellValue('D' . $row, 'кг');

            $row++;

            $worksheet->setCellValue('C' . $row, round($materials['l'], 2));
            $worksheet->setCellValue('D' . $row, 'л');

            $row++;


            self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
                'A' . $row . ':B' . $row,
            ], 'A', 'D');

            $worksheet->setCellValue('A' . $row, 'ВСЕГО');

            $meltCard->batches->each(function ($batch) use (&$chargeSum) {
                $chargeSum += $batch->loads->sum('weight');
            });


            $worksheet->setCellValue('C' . $row, round($chargeSum + $materials['total'] - $materials['l'], 2));
            $worksheet->setCellValue('D' . $row, 'кг');

            $worksheet->getStyle('A' . $row - 2 . ':D' . $row)
                ->getBorders()
                ->getAllBorders()
                ->setBorderStyle(Border::BORDER_THIN)
                ->setColor(new Color('000000'));


            $worksheet->getStyle('A' . $row - 2 . ':D' . $row)
                ->getFont()
                ->setBold(true)
                ->setSize(11)
                ->setName('Arial');


            $worksheet->getStyle('A' . $row - 2 . ':D' . $row)
                ->getAlignment()
                ->setHorizontal(Alignment::HORIZONTAL_CENTER)
                ->setVertical(Alignment::VERTICAL_CENTER);

            if ($maxRows < $row) {
                $maxRows = $row;
            }
        }

        if (!empty($meltCard->additionalMaterials)) {
            $row = $startMaterialRow;
            $firstRow = true;
            $meltCard->additionalMaterials->each(function ($material) use (&$firstRow, &$worksheet, &$row, &$maxRows, $styleArray) {
                self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
                    'F' . $row . ':G' . $row,
                    'H' . $row . ':I' . $row,
                ], 'F', 'J');

                $worksheet->setCellValue('F' . $row, $material->name);
                $worksheet->setCellValue('H' . $row, $material->value);
                $worksheet->setCellValue('J' . $row, $material->measure);

                $worksheet->getStyle('F' . $row . ':J' . $row)
                    ->getBorders()
                    ->getAllBorders()
                    ->setBorderStyle(Border::BORDER_THIN)
                    ->setColor(new Color('000000'));


                $worksheet->getStyle('F' . $row . ':J' . $row)
                    ->getFont()
                    ->setBold(true)
                    ->setSize(11)
                    ->setName('Arial');


                $worksheet->getStyle('F' . $row . ':J' . $row)
                    ->getAlignment()
                    ->setHorizontal(Alignment::HORIZONTAL_CENTER);

                $row++;
                if ($row > $maxRows) {
                    $maxRows = $row;
                }
            });
        }

        if (!empty($meltCard->energyParameters)) {
            $row = $startMaterialRow;
            $firstRow = true;
            $differenceSum = 0;
            $meltCard->energyParameters->each(function ($material) use (&$firstRow, &$differenceSum, &$worksheet, &$row, &$maxRows, $styleArray) {
                $differenceSum += $material->difference;

                self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
                    'L' . $row . ':M' . $row,
                ], 'L', 'P');

                $worksheet->setCellValue('L' . $row, $material->name);
                $worksheet->setCellValue('N' . $row, $material->start_value);
                $worksheet->setCellValue('O' . $row, $material->end_value);
                $worksheet->setCellValue('P' . $row, $material->difference);

                $worksheet->getStyle('L' . $row . ':P' . $row)
                    ->getBorders()
                    ->getAllBorders()
                    ->setBorderStyle(Border::BORDER_THIN)
                    ->setColor(new Color('000000'));


                $worksheet->getStyle('L' . $row . ':P' . $row)
                    ->getFont()
                    ->setBold(true)
                    ->setSize(11)
                    ->setName('Arial');


                $worksheet->getStyle('L' . $row . ':P' . $row)
                    ->getAlignment()
                    ->setHorizontal(Alignment::HORIZONTAL_CENTER);

                $row++;
                if ($row > $maxRows) {
                    $maxRows = $row;
                }
            });

            self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
                'L' . $row . ':O' . $row,
            ], 'L', 'P');

            $worksheet->setCellValue('L' . $row, 'ИТОГО');
            $worksheet->setCellValue('P' . $row, round($differenceSum, 2));

            $worksheet->getStyle('L' . $row . ':P' . $row)
                ->getBorders()
                ->getAllBorders()
                ->setBorderStyle(Border::BORDER_THIN)
                ->setColor(new Color('000000'));


            $worksheet->getStyle('L' . $row . ':P' . $row)
                ->getFont()
                ->setBold(true)
                ->setSize(11)
                ->setName('Arial');


            $worksheet->getStyle('L' . $row . ':P' . $row)
                ->getAlignment()
                ->setHorizontal(Alignment::HORIZONTAL_CENTER);

            if ($row > $maxRows) {
                $maxRows = $row;
            }
        }

        $row = $maxRows + 2;

        $startRow = $row;

        self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
            'A' . $row . ':E' . $row,
        ], 'A', 'E');

        $worksheet->setCellValue('A' . $row, 'Продукция');

        $row++;

        self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
            'A' . $row . ':E' . $row,
        ], 'A', 'E');

        $worksheet->setCellValue('A' . $row, 'V. ВЫХОД');

        $row++;

        self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
            'A' . $row . ':B' . $row,
            'C' . $row . ':D' . $row,
        ], 'A', 'E');

        $worksheet->setCellValue('A' . $row, 'Наименование');
        $worksheet->setCellValue('C' . $row, 'Колл-во, кг');
        $worksheet->setCellValue('E' . $row, '%');

        $row++;

        self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
            'A' . $row . ':B' . $row,
            'C' . $row . ':D' . $row,
        ], 'A', 'E');

        $worksheet->setCellValue('A' . $row, optional($meltCard->output_alloy_grade)->name);
        $worksheet->setCellValue('C' . $row, round($meltCard->output_weight, 2));
        $worksheet->setCellValue('E' . $row, round($meltCard->output_alloy_grade_percent, 2));

        $row++;

        self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
            'A' . $row . ':B' . $row,
            'C' . $row . ':D' . $row,
        ], 'A', 'E');

        $worksheet->setCellValue('A' . $row, 'Обрезь (плавка)');
        $worksheet->setCellValue('C' . $row, round($meltCard->output_trim_weight, 2));
        $worksheet->setCellValue('E' . $row, round($meltCard->output_trim_percent, 2));

        $row++;

        self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
            'A' . $row . ':B' . $row,
            'C' . $row . ':D' . $row,
        ], 'A', 'E');

        $worksheet->setCellValue('A' . $row, 'Литейн. брак (плавка)');
        $worksheet->setCellValue('C' . $row, round($meltCard->output_reject1_weight, 2));
        $worksheet->setCellValue('E' . $row, round($meltCard->output_reject1_percent, 2));

        $row++;

        self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
            'A' . $row . ':B' . $row,
            'C' . $row . ':D' . $row,
        ], 'A', 'E');

        $worksheet->setCellValue('A' . $row, 'Стружка (плавка)');
        $worksheet->setCellValue('C' . $row, round($meltCard->output_shavings_weight, 2));
        $worksheet->setCellValue('E' . $row, round($meltCard->output_shavings_percent, 2));

        $row++;

        self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
            'A' . $row . ':B' . $row,
            'C' . $row . ':D' . $row,
        ], 'A', 'E');

        $worksheet->setCellValue('A' . $row, 'Шт. порезано');
        $worksheet->setCellValue('C' . $row, round($meltCard->output_cut_count, 2));

        $row++;

        self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
            'A' . $row . ':B' . $row,
            'C' . $row . ':D' . $row,
        ], 'A', 'E');

        $worksheet->setCellValue('A' . $row, 'Вес порезаного');
        $worksheet->setCellValue('C' . $row, round($meltCard->output_cut_weight, 2));
        $worksheet->setCellValue('E' . $row, round($meltCard->output_cut_percent, 2));

        $row++;

        self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
            'A' . $row . ':B' . $row,
            'C' . $row . ':D' . $row,
        ], 'A', 'E');

        $worksheet->setCellValue('A' . $row, 'Литейный брак (ротор)');
        $worksheet->setCellValue('C' . $row, round($meltCard->output_reject2_weight, 2));
        $worksheet->setCellValue('E' . $row, round($meltCard->output_reject2_percent, 2));

        $row++;

        self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
            'A' . $row . ':B' . $row,
            'C' . $row . ':D' . $row,
        ], 'A', 'E');

        $worksheet->setCellValue('A' . $row, 'Итого получено:');
        $worksheet->setCellValue('C' . $row, round(
            $meltCard->output_weight
            + $meltCard->output_trim_weight
            + $meltCard->output_reject1_weight
            + $meltCard->output_shavings_weight
            + $meltCard->output_cut_weight
            + $meltCard->output_reject2_weight
            , 2));
        $worksheet->setCellValue('E' . $row, round(
            $meltCard->output_alloy_grade_percent
            + $meltCard->output_trim_percent
            + $meltCard->output_reject1_percent
            + $meltCard->output_shavings_percent
            + $meltCard->output_cut_percent
            + $meltCard->output_reject2_percent
            , 2));

        $worksheet->getStyle('A' . $startRow . ':E' . $row)
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(Border::BORDER_THIN)
            ->setColor(new Color('000000'));


        $worksheet->getStyle('A' . $startRow . ':E' . $row)
            ->getFont()
            ->setBold(true)
            ->setSize(11)
            ->setName('Arial');


        $worksheet->getStyle('A' . $startRow . ':E' . $row)
            ->getAlignment()
            ->setHorizontal(Alignment::HORIZONTAL_CENTER);

        $row += 2;

        self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
            'A' . $row . ':E' . $row,
        ], 'A', 'E');

        $worksheet->setCellValue('A' . $row, 'Примечание:');

        $row++;

        self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
            'A' . $row . ':B' . $row,
            'C' . $row . ':E' . $row,
        ], 'A', 'E');

        $worksheet->setCellValue('A' . $row, 'ШЛАК черный');
        $worksheet->setCellValue('C' . $row, $meltCard->note_black);

        $row++;

        self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
            'A' . $row . ':B' . $row,
            'C' . $row . ':E' . $row,
        ], 'A', 'E');

        $worksheet->setCellValue('A' . $row, 'ШЛАК белый АД');
        $worksheet->setCellValue('C' . $row, $meltCard->note_white);

        $row++;

        self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
            'A' . $row . ':B' . $row,
            'C' . $row . ':E' . $row,
        ], 'A', 'E');

        $worksheet->setCellValue('A' . $row, 'Fe');
        $worksheet->setCellValue('C' . $row, $meltCard->note_fe);

        $worksheet->getStyle('A' . $row - 3 . ':E' . $row)
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(Border::BORDER_THIN)
            ->setColor(new Color('000000'));


        $worksheet->getStyle('A' . $row - 3 . ':E' . $row)
            ->getFont()
            ->setBold(true)
            ->setSize(11)
            ->setName('Arial');


        $worksheet->getStyle('A' . $row - 3 . ':E' . $row)
            ->getAlignment()
            ->setHorizontal(Alignment::HORIZONTAL_CENTER);


        $row = $startRow;

        self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
            'G' . $row . ':H' . $row,
            'I' . $row . ':J' . $row,
        ], 'G', 'K');

        $worksheet->setCellValue('G' . $row, 'Наименование');
        $worksheet->setCellValue('I' . $row, 'Кол-во, кг');
        $worksheet->setCellValue('K' . $row, '%');

        $row++;

        self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
            'G' . $row . ':H' . $row,
            'I' . $row . ':J' . $row,
        ], 'G', 'K');

        $worksheet->setCellValue('G' . $row, 'ШЛАК черный');
        $worksheet->setCellValue('I' . $row, $meltCard->output_black_weight);
        $worksheet->setCellValue('K' . $row, $meltCard->output_black_percent);

        $row++;

        self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
            'G' . $row . ':H' . $row,
            'I' . $row . ':J' . $row,
        ], 'G', 'K');

        $worksheet->setCellValue('G' . $row, 'ШЛАК белый АД');
        $worksheet->setCellValue('I' . $row, $meltCard->output_white_weight);
        $worksheet->setCellValue('K' . $row, $meltCard->output_white_percent);

        $row++;

        self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
            'G' . $row . ':H' . $row,
            'I' . $row . ':J' . $row,
        ], 'G', 'K');

        $worksheet->setCellValue('G' . $row, 'Мусор');
        $worksheet->setCellValue('I' . $row, $meltCard->output_trash_weight);
        $worksheet->setCellValue('K' . $row, $meltCard->output_trash_percent);

        $row++;

        self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
            'G' . $row . ':H' . $row,
            'I' . $row . ':J' . $row,
        ], 'G', 'K');


        $outputIrrevocablyWeight = ($chargeSum + $materials['total'] - $materials['l'])
            - ($meltCard->output_weight + $meltCard->output_trim_weight + $meltCard->output_shavings_weight
                + $meltCard->output_reject1_weight + $meltCard->output_reject2_weight + $meltCard->output_cut_weight)
            - $meltCard->output_black_weight - $meltCard->output_white_weight - $meltCard->output_trash_weight;

        $outputIrrevocablyWeight = round($outputIrrevocablyWeight, 2);
        if ($chargeSum) {
            $outputIrrevocablyPercent = round($outputIrrevocablyWeight / $chargeSum * 100);
        } else {
            $outputIrrevocablyPercent = null;
        }


        $worksheet->setCellValue('G' . $row, 'Безвозврат');
        $worksheet->setCellValue('I' . $row, $outputIrrevocablyWeight ?? 0);
        $worksheet->setCellValue('K' . $row, $outputIrrevocablyPercent);

        $row++;

        self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
            'G' . $row . ':H' . $row,
            'I' . $row . ':J' . $row,
        ], 'G', 'K');

        $worksheet->setCellValue('G' . $row, 'Итого потерь');
        $worksheet->setCellValue('I' . $row, round($meltCard->loss_sum_weight + $outputIrrevocablyWeight, 2));
        $worksheet->setCellValue('K' . $row, round($meltCard->loss_sum_percent + $outputIrrevocablyPercent));

        $worksheet->getStyle('G' . $startRow . ':K' . $row)
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(Border::BORDER_THIN)
            ->setColor(new Color('000000'));


        $worksheet->getStyle('G' . $startRow . ':K' . $row)
            ->getFont()
            ->setBold(true)
            ->setSize(11)
            ->setName('Arial');


        $worksheet->getStyle('G' . $startRow . ':K' . $row)
            ->getAlignment()
            ->setHorizontal(Alignment::HORIZONTAL_CENTER);

        $row += 2;

        self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
            'G' . $row . ':H' . $row,
            'I' . $row . ':J' . $row,
        ], 'G', 'K');

        $worksheet->setCellValue('G' . $row, 'Общий итог, кг');
        $worksheet->setCellValue('I' . $row, round($meltCard->overall_result, 2));

        $row++;

        self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
            'G' . $row . ':H' . $row,
            'I' . $row . ':J' . $row,
        ], 'G', 'K');

        $worksheet->setCellValue('G' . $row, 'Расчетные %');
        $worksheet->setCellValue('I' . $row, round(MeltingTaskService::getPlaneMvg($meltCard->id), 2));
        $worksheet->setCellValue('K' . $row, -1 * round(MeltingTaskService::getPlaneMvg($meltCard->id) - MeltingTaskService::getFactMvg($meltCard->id), 2));

        $row++;

        self::copyExcelRowFull($worksheet, $worksheet, $row, $row, [
            'G' . $row . ':H' . $row,
            'I' . $row . ':J' . $row,
        ], 'G', 'K');

        $worksheet->setCellValue('I' . $row, 'КГ');
        $worksheet->setCellValue('K' . $row, -1 * round(MeltingTaskService::getLossSum($meltCard->id), 2));

        $worksheet->getStyle('G' . $row - 2 . ':K' . $row)
            ->getBorders()
            ->getAllBorders()
            ->setBorderStyle(Border::BORDER_THIN)
            ->setColor(new Color('000000'));


        $worksheet->getStyle('G' . $row - 2 . ':K' . $row)
            ->getFont()
            ->setBold(true)
            ->setSize(11)
            ->setName('Arial');


        $worksheet->getStyle('G' . $row - 2 . ':K' . $row)
            ->getAlignment()
            ->setHorizontal(Alignment::HORIZONTAL_CENTER);


        $worksheet->getStyle('A1:T' . $row)
            ->getFont()
            ->setBold(true)
            ->setSize(10)
            ->setName('Arial');

        for ($i = 1; $i <= $row; $i++) {
            $worksheet->getRowDimension($i)->setRowHeight(-1);
        }

        $objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');

        $publicPath = Storage::disk('public')->path('xls/');

        if (!file_exists($publicPath)) {
            mkdir($publicPath);
        }

        $fileName = now()->format('d.m.Y_i_s_') . 'meltcard' . '.xlsx';

        $objWriter->save($publicPath . $fileName);

        return Storage::download('public/xls/' . $fileName);
    }

    public static function copyExcelRowFull(&$ws_from, &$ws_to, $row_from, $row_to, $merge_array = [], $firstColumn = 'A', $lastColumn = false): void
    {
        $ws_to->getRowDimension($row_to)
            ->setRowHeight($ws_from->getRowDimension($row_from)
                ->getRowHeight());

        if (!$lastColumn) {
            $lastColumn = $ws_from
                ->getHighestColumn();
        }


        for ($c = $firstColumn; $c != $lastColumn; ++$c) {
            $cell_from = $ws_from->getCell($c . $row_from);
            $cell_to = $ws_to->getCell($c . $row_to);
            $cell_to->setXfIndex($cell_from->getXfIndex()); // black magic here
            $cell_to->setValue($cell_from->getValue());
        }

        foreach ($merge_array as $merge_range) {
            $ws_to->mergeCells($merge_range);
        }
    }

}
