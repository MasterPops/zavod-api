<?php

namespace App\Providers;

use App\Models\AdditionalWork;
use App\Models\AlloyGrade;
use App\Models\Charge;
use App\Models\ChemicalComposition;
use App\Models\Diameter;
use App\Models\EnergyParameter;
use App\Models\Furnace;
use App\Models\Mark;
use App\Models\Material;
use App\Models\MeltingTask;
use App\Models\ProductType;
use App\Models\TechnicalCondition;
use App\Models\TestType;
use App\Models\User;
use App\Observers\AdditionalWorkObserver;
use App\Observers\AlloyGradeObserver;
use App\Observers\ChargeObserver;
use App\Observers\ChemicalCompositionObserver;
use App\Observers\DiameterObserver;
use App\Observers\EnergyParameterObserver;
use App\Observers\FurnaceObserver;
use App\Observers\MarkObserver;
use App\Observers\MaterialObserver;
use App\Observers\MeltingTaskObserve;
use App\Observers\ProductTypeObserver;
use App\Observers\TechnicalConditionObserver;
use App\Observers\TestTypeObserver;
use App\Observers\UserObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        MeltingTask::observe(MeltingTaskObserve::class);
        User::observe(UserObserver::class);
        Furnace::observe(FurnaceObserver::class);
        AlloyGrade::observe(AlloyGradeObserver::class);
        Charge::observe(ChargeObserver::class);
        TechnicalCondition::observe(TechnicalConditionObserver::class);
        Diameter::observe(DiameterObserver::class);
        Mark::observe(MarkObserver::class);
        ProductType::observe(ProductTypeObserver::class);
        Material::observe(MaterialObserver::class);
        AdditionalWork::observe(AdditionalWorkObserver::class);
        EnergyParameter::observe(EnergyParameterObserver::class);
        TestType::observe(TestTypeObserver::class);
        ChemicalComposition::observe(ChemicalCompositionObserver::class);
    }
}
