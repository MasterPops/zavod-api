<?php

namespace App\Providers;

use App\Library\Backpack\CrudPanel;
use Illuminate\Support\ServiceProvider;

class BackpackServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('crud', function ($app) {
            return new CrudPanel();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

}
