<?php

namespace App\Observers;

use App\Models\Event;
use App\Models\Material;
use Illuminate\Support\Facades\Auth;

class MaterialObserver
{
    /**
     * Handle the Material "created" event.
     */
    public function created(Material $material): void
    {
        $dirty = $material->getDirty();

        $after = [];

        if (!empty($dirty)) {
            foreach ($dirty as $key => $value) {
                if ($key != 'updated_at' && $key != 'created_at') {
                    $after[] = trans('model/material.labels.'.$key) . ': '. $value;
                }
            }
        }

        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Создание материала id: $material->id",
            '', implode(', ', $after));
    }

    /**
     * Handle the Material "updated" event.
     */
    public function updated(Material $material): void
    {
        $dirty = $material->getDirty();

        $before = [];
        $after = [];

        if (!empty($dirty)) {
            foreach ($dirty as $key => $value) {
                if ($key != 'updated_at') {
                    $before[] = trans('model/material.labels.'.$key) . ': '. $material->getOriginal($key);
                    $after[] = trans('model/material.labels.'.$key) . ': '. $value;
                }
            }
        }

        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Редактирование материала id: $material->id",
            implode(', ', $before), implode(', ', $after));
    }

    /**
     * Handle the Material "deleted" event.
     */
    public function deleted(Material $material): void
    {
        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Удаление материала id: $material->id",
            '', '');
    }

    /**
     * Handle the Material "restored" event.
     */
    public function restored(Material $material): void
    {
        //
    }

    /**
     * Handle the Material "force deleted" event.
     */
    public function forceDeleted(Material $material): void
    {
        //
    }
}
