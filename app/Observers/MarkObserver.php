<?php

namespace App\Observers;

use App\Models\Event;
use App\Models\Mark;
use Illuminate\Support\Facades\Auth;

class MarkObserver
{
    /**
     * Handle the Mark "created" event.
     */
    public function created(Mark $mark): void
    {
        $dirty = $mark->getDirty();

        $after = [];

        if (!empty($dirty)) {
            foreach ($dirty as $key => $value) {
                if ($key != 'updated_at' && $key != 'created_at') {
                    $after[] = trans('model/mark.labels.'.$key) . ': '. $value;
                }
            }
        }

        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Создание маркировки id: $mark->id",
            '', implode(', ', $after));
    }

    /**
     * Handle the Mark "updated" event.
     */
    public function updated(Mark $mark): void
    {
        $dirty = $mark->getDirty();

        $before = [];
        $after = [];

        if (!empty($dirty)) {
            foreach ($dirty as $key => $value) {
                if ($key != 'updated_at') {
                    $before[] = trans('model/mark.labels.'.$key) . ': '. $mark->getOriginal($key);
                    $after[] = trans('model/mark.labels.'.$key) . ': '. $value;
                }
            }
        }

        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Редактирование маркировки id: $mark->id",
            implode(', ', $before), implode(', ', $after));
    }

    /**
     * Handle the Mark "deleted" event.
     */
    public function deleted(Mark $mark): void
    {
        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Удаление маркировки id: $mark->id",
            '', '');
    }

    /**
     * Handle the Mark "restored" event.
     */
    public function restored(Mark $mark): void
    {
        //
    }

    /**
     * Handle the Mark "force deleted" event.
     */
    public function forceDeleted(Mark $mark): void
    {
        //
    }
}
