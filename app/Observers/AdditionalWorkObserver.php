<?php

namespace App\Observers;

use App\Models\AdditionalWork;
use App\Models\Event;
use Illuminate\Support\Facades\Auth;

class AdditionalWorkObserver
{
    /**
     * Handle the AdditionalWork "created" event.
     */
    public function created(AdditionalWork $additionalWork): void
    {
        $dirty = $additionalWork->getDirty();

        $after = [];

        if (!empty($dirty)) {
            foreach ($dirty as $key => $value) {
                if ($key != 'updated_at' && $key != 'created_at') {
                    $after[] = trans('model/additional_work.labels.'.$key) . ': '. $value;
                }
            }
        }

        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Создание доп. работы id: $additionalWork->id",
            '', implode(', ', $after));
    }

    /**
     * Handle the AdditionalWork "updated" event.
     */
    public function updated(AdditionalWork $additionalWork): void
    {
        $dirty = $additionalWork->getDirty();

        $before = [];
        $after = [];

        if (!empty($dirty)) {
            foreach ($dirty as $key => $value) {
                if ($key != 'updated_at') {
                    $before[] = trans('model/additional_work.labels.'.$key) . ': '. $additionalWork->getOriginal($key);
                    $after[] = trans('model/additional_work.labels.'.$key) . ': '. $value;
                }
            }
        }

        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Редактирование доп. работы id: $additionalWork->id",
            implode(', ', $before), implode(', ', $after));
    }

    /**
     * Handle the AdditionalWork "deleted" event.
     */
    public function deleted(AdditionalWork $additionalWork): void
    {
        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Удаление доп. работы id: $additionalWork->id",
            '', '');
    }

    /**
     * Handle the AdditionalWork "restored" event.
     */
    public function restored(AdditionalWork $additionalWork): void
    {
        //
    }

    /**
     * Handle the AdditionalWork "force deleted" event.
     */
    public function forceDeleted(AdditionalWork $additionalWork): void
    {
        //
    }
}
