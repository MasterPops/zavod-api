<?php

namespace App\Observers;

use App\Models\AlloyGrade;
use App\Models\Event;
use Illuminate\Support\Facades\Auth;

class AlloyGradeObserver
{
    /**
     * Handle the AlloyGrade "created" event.
     */
    public function created(AlloyGrade $alloyGrade): void
    {
        $dirty = $alloyGrade->getDirty();

        $after = [];

        if (!empty($dirty)) {
            foreach ($dirty as $key => $value) {
                if ($key != 'updated_at' && $key != 'created_at') {
                    $after[] = trans('model/alloy_grade.labels.'.$key) . ': '. $value;
                }
            }
        }

        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Создание марки сплава id: $alloyGrade->id",
            '', implode(', ', $after));
    }

    /**
     * Handle the AlloyGrade "updated" event.
     */
    public function updated(AlloyGrade $alloyGrade): void
    {
        $dirty = $alloyGrade->getDirty();

        $before = [];
        $after = [];

        if (!empty($dirty)) {
            foreach ($dirty as $key => $value) {
                if ($key != 'updated_at') {
                    $before[] = trans('model/alloy_grade.labels.'.$key) . ': '. $alloyGrade->getOriginal($key);
                    $after[] = trans('model/alloy_grade.labels.'.$key) . ': '. $value;
                }
            }
        }

        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Редактирование марки сплава id: $alloyGrade->id",
            implode(', ', $before), implode(', ', $after));
    }

    /**
     * Handle the AlloyGrade "deleted" event.
     */
    public function deleted(AlloyGrade $alloyGrade): void
    {
        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Удаление марки сплава id: $alloyGrade->id",
            '', '');
    }

    /**
     * Handle the AlloyGrade "restored" event.
     */
    public function restored(AlloyGrade $alloyGrade): void
    {
        //
    }

    /**
     * Handle the AlloyGrade "force deleted" event.
     */
    public function forceDeleted(AlloyGrade $alloyGrade): void
    {
        //
    }
}
