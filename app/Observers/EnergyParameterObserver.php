<?php

namespace App\Observers;

use App\Models\EnergyParameter;
use App\Models\Event;
use Illuminate\Support\Facades\Auth;

class EnergyParameterObserver
{
    /**
     * Handle the EnergyParameter "created" event.
     */
    public function created(EnergyParameter $energyParameter): void
    {
        $dirty = $energyParameter->getDirty();

        $after = [];

        if (!empty($dirty)) {
            foreach ($dirty as $key => $value) {
                if ($key != 'updated_at' && $key != 'created_at') {
                    $after[] = trans('model/energy_parameter.labels.'.$key) . ': '. $value;
                }
            }
        }

        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Создание расхода энергии id: $energyParameter->id",
            '', implode(', ', $after));
    }

    /**
     * Handle the EnergyParameter "updated" event.
     */
    public function updated(EnergyParameter $energyParameter): void
    {
        $dirty = $energyParameter->getDirty();

        $before = [];
        $after = [];

        if (!empty($dirty)) {
            foreach ($dirty as $key => $value) {
                if ($key != 'updated_at') {
                    $before[] = trans('model/diameter.labels.'.$key) . ': '. $energyParameter->getOriginal($key);
                    $after[] = trans('model/diameter.labels.'.$key) . ': '. $value;
                }
            }
        }

        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Редактирование расхода энергии id: $energyParameter->id",
            implode(', ', $before), implode(', ', $after));
    }

    /**
     * Handle the EnergyParameter "deleted" event.
     */
    public function deleted(EnergyParameter $energyParameter): void
    {
        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Удаление расхода энергии id: $energyParameter->id",
            '', '');
    }

    /**
     * Handle the EnergyParameter "restored" event.
     */
    public function restored(EnergyParameter $energyParameter): void
    {
        //
    }

    /**
     * Handle the EnergyParameter "force deleted" event.
     */
    public function forceDeleted(EnergyParameter $energyParameter): void
    {
        //
    }
}
