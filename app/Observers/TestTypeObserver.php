<?php

namespace App\Observers;

use App\Models\Event;
use App\Models\TestType;
use Illuminate\Support\Facades\Auth;

class TestTypeObserver
{
    /**
     * Handle the TestType "created" event.
     */
    public function created(TestType $testType): void
    {
        $dirty = $testType->getDirty();

        $after = [];

        if (!empty($dirty)) {
            foreach ($dirty as $key => $value) {
                if ($key != 'updated_at' && $key != 'created_at') {
                    $after[] = trans('model/test_type.labels.'.$key) . ': '. $value;
                }
            }
        }

        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Создание типа теста id: $testType->id",
            '', implode(', ', $after));
    }

    /**
     * Handle the TestType "updated" event.
     */
    public function updated(TestType $testType): void
    {
        $dirty = $testType->getDirty();

        $before = [];
        $after = [];

        if (!empty($dirty)) {
            foreach ($dirty as $key => $value) {
                if ($key != 'updated_at') {
                    $before[] = trans('model/test_type.labels.'.$key) . ': '. $testType->getOriginal($key);
                    $after[] = trans('model/test_type.labels.'.$key) . ': '. $value;
                }
            }
        }

        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Редактирование типа теста id: $testType->id",
            implode(', ', $before), implode(', ', $after));
    }

    /**
     * Handle the TestType "deleted" event.
     */
    public function deleted(TestType $testType): void
    {
        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Удаление типа теста id: $testType->id",
            '', '');
    }

    /**
     * Handle the TestType "restored" event.
     */
    public function restored(TestType $testType): void
    {
        //
    }

    /**
     * Handle the TestType "force deleted" event.
     */
    public function forceDeleted(TestType $testType): void
    {
        //
    }
}
