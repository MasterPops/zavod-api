<?php

namespace App\Observers;

use App\Models\ChemicalComposition;
use App\Models\Event;
use Illuminate\Support\Facades\Auth;

class ChemicalCompositionObserver
{
    /**
     * Handle the ChemicalComposition "created" event.
     */
    public function created(ChemicalComposition $chemicalComposition): void
    {
        $dirty = $chemicalComposition->getDirty();

        $after = [];

        if (!empty($dirty)) {
            foreach ($dirty as $key => $value) {
                if ($key != 'updated_at' && $key != 'created_at') {
                    $after[] = trans('model/chemical_composition.labels.'.$key) . ': '. $value;
                }
            }
        }

        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Создание химической композиции id: $chemicalComposition->id",
            '', implode(', ', $after));
    }

    /**
     * Handle the ChemicalComposition "updated" event.
     */
    public function updated(ChemicalComposition $chemicalComposition): void
    {
        $dirty = $chemicalComposition->getDirty();

        $before = [];
        $after = [];

        if (!empty($dirty)) {
            foreach ($dirty as $key => $value) {
                if ($key != 'updated_at') {
                    $before[] = trans('model/chemical_composition.labels.'.$key) . ': '. $chemicalComposition->getOriginal($key);
                    $after[] = trans('model/chemical_composition.labels.'.$key) . ': '. $value;
                }
            }
        }

        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Редактирование химической композиции id: $chemicalComposition->id",
            implode(', ', $before), implode(', ', $after));
    }

    /**
     * Handle the ChemicalComposition "deleted" event.
     */
    public function deleted(ChemicalComposition $chemicalComposition): void
    {
        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Удаление химической композиции id: $chemicalComposition->id",
            '', '');
    }

    /**
     * Handle the ChemicalComposition "restored" event.
     */
    public function restored(ChemicalComposition $chemicalComposition): void
    {
        //
    }

    /**
     * Handle the ChemicalComposition "force deleted" event.
     */
    public function forceDeleted(ChemicalComposition $chemicalComposition): void
    {
        //
    }
}
