<?php

namespace App\Observers;

use App\Models\Batch;
use App\Models\Event;
use App\Models\MeltingTask;
use App\Services\MeltingTaskService;
use Illuminate\Support\Facades\Auth;

class MeltingTaskObserve
{
    /**
     * Handle the MeltingTask "creating" event.
     */
    public function creating(MeltingTask $meltingTask): void
    {
        $meltingTask->master_id = Auth::id();

        if (!empty($meltingTask->planned_melting_date)) {
            $meltingTask->melt_down_to = $meltingTask->planned_melting_date
                ->startOfDay()
                ->addDay()
                ->addHours(8)
                ->addMinutes(59)
                ->addSeconds(59);
        }
    }

    /**
     * Handle the MeltingTask "updated" event.
     */
    public function updating(MeltingTask $meltingTask): void
    {
        if ($meltingTask->isDirty('daily_melting_number')) {
            Event::add(Auth::id(), Event::OBJECT_PROD_PLAN, 'Изменен порядковый номер плавки',
                $meltingTask->getOriginal('daily_melting_number'), $meltingTask->daily_melting_number);
        }

        if ($meltingTask->isDirty('status')) {
            if ($meltingTask->status != MeltingTask::COMPLETED_STATUS) {
                $meltingTask->melting_done_at = null;
            }
            Event::add(Auth::id(), Event::OBJECT_PROD_PLAN, 'Изменен статус задания',
                MeltingTask::STATUSES_FOR_INTERFACE[$meltingTask->getOriginal('status')],  MeltingTask::STATUSES_FOR_INTERFACE[$meltingTask->status]);
        }

        if ($meltingTask->isDirty('planned_melting_date')) {
            Event::add(Auth::id(), Event::OBJECT_PROD_PLAN, 'Изменена запланированная дата плавки',
                $meltingTask->getOriginal('planned_melting_date'), $meltingTask->planned_melting_date);
        }

        $outputAttributes = [
            'output_weight',
            'output_trim_weight',
            'output_reject1_weight',
            'output_cut_weight',
            'output_reject2_weight',
            'output_shavings_weight',
            'output_black_weight',
            'output_white_weight',
            'output_trash_weight',
            'output_irrevocably_weight',
        ];

        if ($meltingTask->isDirty($outputAttributes)) {
            MeltingTaskService::calculateWeightAndPercent($meltingTask->id);
        }

        if ($meltingTask->isDirty('planned_melting_date')) {
            $meltingTask->melt_down_to = $meltingTask->planned_melting_date
                ->endOfDay();
//                ->startOfDay()
//                ->addDay()
//                ->addHours(8)
//                ->addMinutes(59)
//                ->addSeconds(59);
        }
        if ($meltingTask->isDirty('status') && $meltingTask->status == MeltingTask::OVERFLOW_STATUS) {
            $meltingTask->overflow_start_at = now();
        }

        if ($meltingTask->isDirty('status') && $meltingTask->status == MeltingTask::COMPLETED_STATUS) {
            $meltingTask->melting_done_at = now();
        }

        if ($meltingTask->isDirty('status') && in_array($meltingTask->status, [
                MeltingTask::COMPLETED_STATUS,
                MeltingTask::FAIL_STATUS,
                MeltingTask::CANCELLED_STATUS,
            ])) {
            $meltingTask->finished_at = now();
        }


        if ($meltingTask->isDirty('status') && $meltingTask->status == MeltingTask::STARTED_STATUS) {
            $meltingTask->melting_start_at = now();
            if (empty($meltingTask->melting_date)) {
                $meltingTask->melting_date = now();
            }

            if (empty($meltingTask->daily_melting_number)) {
                $lastCurrentDayTask = MeltingTask::where('melting_date', '>=', now()->startOfDay())
                    ->where('melting_date', '<=', now()->endOfDay())
                    ->whereNotNull('daily_melting_number')
                    ->orderByDesc('daily_melting_number')
                    ->first();

                if (!empty($lastCurrentDayTask)) {
                    $meltingTask->daily_melting_number = $lastCurrentDayTask->daily_melting_number + 1;
                } else {
                    $meltingTask->daily_melting_number = 1;

                }
            }
        }
    }

    /**
     * Handle the MeltingTask "deleting" event.
     */
    public function deleting(MeltingTask $meltingTask): void
    {
        Event::add(Auth::id(), Event::OBJECT_PROD_PLAN, 'Удалено задание на плавку',
            '', $meltingTask->id);
    }

    /**
     * Handle the MeltingTask "restored" event.
     */
    public function restored(MeltingTask $meltingTask): void
    {
        //
    }

    /**
     * Handle the MeltingTask "force deleted" event.
     */
    public function forceDeleted(MeltingTask $meltingTask): void
    {
        //
    }
}
