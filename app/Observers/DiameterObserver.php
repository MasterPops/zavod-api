<?php

namespace App\Observers;

use App\Models\Diameter;
use App\Models\Event;
use Illuminate\Support\Facades\Auth;

class DiameterObserver
{
    /**
     * Handle the Diameter "created" event.
     */
    public function created(Diameter $diameter): void
    {
        $dirty = $diameter->getDirty();

        $after = [];

        if (!empty($dirty)) {
            foreach ($dirty as $key => $value) {
                if ($key != 'updated_at' && $key != 'created_at') {
                    $after[] = trans('model/diameter.labels.'.$key) . ': '. $value;
                }
            }
        }

        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Создание диаметра id: $diameter->id",
            '', implode(', ', $after));
    }

    /**
     * Handle the Diameter "updated" event.
     */
    public function updated(Diameter $diameter): void
    {
        $dirty = $diameter->getDirty();

        $before = [];
        $after = [];

        if (!empty($dirty)) {
            foreach ($dirty as $key => $value) {
                if ($key != 'updated_at') {
                    $before[] = trans('model/diameter.labels.'.$key) . ': '. $diameter->getOriginal($key);
                    $after[] = trans('model/diameter.labels.'.$key) . ': '. $value;
                }
            }
        }

        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Редактирование диаметра id: $diameter->id",
            implode(', ', $before), implode(', ', $after));
    }

    /**
     * Handle the Diameter "deleted" event.
     */
    public function deleted(Diameter $diameter): void
    {
        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Удаление диаметра id: $diameter->id",
            '', '');
    }

    /**
     * Handle the Diameter "restored" event.
     */
    public function restored(Diameter $diameter): void
    {
        //
    }

    /**
     * Handle the Diameter "force deleted" event.
     */
    public function forceDeleted(Diameter $diameter): void
    {
        //
    }
}
