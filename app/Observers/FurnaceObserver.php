<?php

namespace App\Observers;

use App\Models\Event;
use App\Models\Furnace;
use Illuminate\Support\Facades\Auth;

class FurnaceObserver
{
    /**
     * Handle the Furnace "created" event.
     */
    public function created(Furnace $furnace): void
    {
        $dirty = $furnace->getDirty();

        $after = [];

        if (!empty($dirty)) {
            foreach ($dirty as $key => $value) {
                if ($key != 'updated_at' && $key != 'created_at') {
                    $after[] = trans('model/furnace.labels.'.$key) . ': '. $value;
                }
            }
        }

        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Создание печи id: $furnace->id",
            '', implode(', ', $after));
    }

    /**
     * Handle the Furnace "updated" event.
     */
    public function updated(Furnace $furnace): void
    {
        $dirty = $furnace->getDirty();

        $before = [];
        $after = [];

        if (!empty($dirty)) {
            foreach ($dirty as $key => $value) {
                if ($key != 'updated_at') {
                    $before[] = trans('model/furnace.labels.'.$key) . ': '. $furnace->getOriginal($key);
                    $after[] = trans('model/furnace.labels.'.$key) . ': '. $value;
                }
            }
        }

        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Редактирование печи id: $furnace->id",
            implode(', ', $before), implode(', ', $after));
    }

    /**
     * Handle the Furnace "deleted" event.
     */
    public function deleted(Furnace $furnace): void
    {
        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Удаление печи id: $furnace->id",
            '', '');
    }

    /**
     * Handle the Furnace "restored" event.
     */
    public function restored(Furnace $furnace): void
    {
        //
    }

    /**
     * Handle the Furnace "force deleted" event.
     */
    public function forceDeleted(Furnace $furnace): void
    {
        //
    }
}
