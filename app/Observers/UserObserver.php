<?php

namespace App\Observers;

use App\Models\Event;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserObserver
{
    /**
     * Handle the User "created" event.
     */
    public function created(User $user): void
    {
        $dirty = $user->getDirty();

        $after = [];

        if (!empty($dirty)) {
            foreach ($dirty as $key => $value) {
                if ($key != 'updated_at' && $key != 'created_at' && $key != 'password' && $key != 'remember_token') {
                    $after[] = trans('model/user.labels.'.$key) . ': '. $value;
                }
            }
        }
        if (Auth::id() || backpack_user()->id) {
            Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
                "Создание пользователя id: $user->id",
                '', implode(', ', $after));
        }
    }

    /**
     * Handle the User "updated" event.
     */
    public function updated(User $user): void
    {
        $dirty = $user->getDirty();

        $before = [];
        $after = [];

        if (!empty($dirty)) {
            foreach ($dirty as $key => $value) {
                if ($key != 'updated_at' && $key != 'password' && $key != 'remember_token') {
                    $before[] = trans('model/user.labels.'.$key) . ': '. $user->getOriginal($key);
                    $after[] = trans('model/user.labels.'.$key) . ': '. $value;
                }
            }
        }

        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Редактирование пользователя id: $user->id",
            implode(', ', $before), implode(', ', $after));
    }

    /**
     * Handle the User "deleted" event.
     */
    public function deleted(User $user): void
    {
        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Удаление пользователя id: $user->id",
            '', '');
    }

    /**
     * Handle the User "restored" event.
     */
    public function restored(User $user): void
    {
        //
    }

    /**
     * Handle the User "force deleted" event.
     */
    public function forceDeleted(User $user): void
    {
        //
    }
}
