<?php

namespace App\Observers;

use App\Models\Event;
use App\Models\TechnicalCondition;
use Illuminate\Support\Facades\Auth;

class TechnicalConditionObserver
{
    /**
     * Handle the TechnicalCondition "created" event.
     */
    public function created(TechnicalCondition $technicalCondition): void
    {
        $dirty = $technicalCondition->getDirty();

        $after = [];

        if (!empty($dirty)) {
            foreach ($dirty as $key => $value) {
                if ($key != 'updated_at' && $key != 'created_at') {
                    $after[] = trans('model/technical_condition.labels.'.$key) . ': '. $value;
                }
            }
        }

        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Создание ТУ\ГОСТ id: $technicalCondition->id",
            '', implode(', ', $after));
    }

    /**
     * Handle the TechnicalCondition "updated" event.
     */
    public function updated(TechnicalCondition $technicalCondition): void
    {
        $dirty = $technicalCondition->getDirty();

        $before = [];
        $after = [];

        if (!empty($dirty)) {
            foreach ($dirty as $key => $value) {
                if ($key != 'updated_at') {
                    $before[] = trans('model/charge.labels.'.$key) . ': '. $technicalCondition->getOriginal($key);
                    $after[] = trans('model/charge.labels.'.$key) . ': '. $value;
                }
            }
        }

        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Редактирование ТУ\ГОСТ id: $technicalCondition->id",
            implode(', ', $before), implode(', ', $after));
    }

    /**
     * Handle the TechnicalCondition "deleted" event.
     */
    public function deleted(TechnicalCondition $technicalCondition): void
    {
        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Удаление ТУ\ГОСТ id: $technicalCondition->id",
            '', '');
    }

    /**
     * Handle the TechnicalCondition "restored" event.
     */
    public function restored(TechnicalCondition $technicalCondition): void
    {
        //
    }

    /**
     * Handle the TechnicalCondition "force deleted" event.
     */
    public function forceDeleted(TechnicalCondition $technicalCondition): void
    {
        //
    }
}
