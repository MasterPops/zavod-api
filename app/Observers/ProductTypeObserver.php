<?php

namespace App\Observers;

use App\Models\Event;
use App\Models\ProductType;
use Illuminate\Support\Facades\Auth;

class ProductTypeObserver
{
    /**
     * Handle the ProductType "created" event.
     */
    public function created(ProductType $productType): void
    {
        $dirty = $productType->getDirty();

        $after = [];

        if (!empty($dirty)) {
            foreach ($dirty as $key => $value) {
                if ($key != 'updated_at' && $key != 'created_at') {
                    $after[] = trans('model/product_type.labels.'.$key) . ': '. $value;
                }
            }
        }

        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Создание типа продукции id: $productType->id",
            '', implode(', ', $after));
    }

    /**
     * Handle the ProductType "updated" event.
     */
    public function updated(ProductType $productType): void
    {
        $dirty = $productType->getDirty();

        $before = [];
        $after = [];

        if (!empty($dirty)) {
            foreach ($dirty as $key => $value) {
                if ($key != 'updated_at') {
                    $before[] = trans('model/product_type.labels.'.$key) . ': '. $productType->getOriginal($key);
                    $after[] = trans('model/product_type.labels.'.$key) . ': '. $value;
                }
            }
        }

        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Редактирование типа продукции id: $productType->id",
            implode(', ', $before), implode(', ', $after));
    }

    /**
     * Handle the ProductType "deleted" event.
     */
    public function deleted(ProductType $productType): void
    {
        Event::add(Auth::id() ?? backpack_user()->id, Event::OBJECT_ADMIN_PANEL,
            "Удаление типа продукции id: $productType->id",
            '', '');
    }

    /**
     * Handle the ProductType "restored" event.
     */
    public function restored(ProductType $productType): void
    {
        //
    }

    /**
     * Handle the ProductType "force deleted" event.
     */
    public function forceDeleted(ProductType $productType): void
    {
        //
    }
}
