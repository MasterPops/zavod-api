<?php

namespace App\Http\Resources;

use App\Models\ChemicalComposition;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ChemicalCompositionResource extends JsonResource
{
    /**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    public static $wrap = 'chemical_composition';

    /**
     * The "data" collection wrapper that should be applied.
     *
     * @var string
     */
    public static $collectionWrap = 'chemical_compositions';

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        if ($this->resource instanceof ChemicalComposition) {

            return [
                'id' => $this->resource->id,
                'technical_condition_id' => $this->resource->technical_condition_id,
                'technical_condition' => new TechnicalConditionResource($this->resource->technical_condition),
                'alloy_grade_id' => $this->resource->alloy_grade_id,
                'alloy_grade' => new AlloyGradeResource($this->resource->alloy_grade),
                'diameter_id' => $this->resource->diameter_id,
                'diameter' => new DiameterResource($this->resource->diameter),
                'al' => str_replace(',', '.', $this->resource->al),
                'si' => str_replace(',', '.', $this->resource->si),
                'fe' => str_replace(',', '.', $this->resource->fe),
                'cu' => str_replace(',', '.', $this->resource->cu),
                'mn' => str_replace(',', '.', $this->resource->mn),
                'mg' => str_replace(',', '.', $this->resource->mg),
                'zn' => str_replace(',', '.', $this->resource->zn),
                'ti' => str_replace(',', '.', $this->resource->ti),
                'ni' => str_replace(',', '.', $this->resource->ni),
                'pb' => str_replace(',', '.', $this->resource->pb),
                'sn' => str_replace(',', '.', $this->resource->sn),
                'cr' => str_replace(',', '.', $this->resource->cr),
                'cd' => str_replace(',', '.', $this->resource->cd),
                'proportions' => $this->resource->proportions,
                'description' => $this->resource->description,
            ];

        }

        return parent::toArray($request);
    }
}
