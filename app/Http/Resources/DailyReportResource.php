<?php

namespace App\Http\Resources;

use App\Models\DailyReport;
use App\Models\Diameter;
use App\Models\Furnace;
use App\Models\MeltingTask;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class DailyReportResource extends JsonResource
{
    /**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    public static $wrap = 'daily_report';

    /**
     * The "data" collection wrapper that should be applied.
     *
     * @var string
     */
    public static $collectionWrap = 'daily_reports';

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        if ($this->resource instanceof DailyReport) {

            $furnaces = Furnace::query()->active()->get();

            $furnacesData = [];

            $furnaces->each(function ($furnace) use (&$furnacesData) {
                $furnacesData[$furnace->name] = [
                    'fact' => MeltingTask::where('melting_done_at', '>=', Carbon::parse($this->resource->date)->startOfDay())
                        ->where('melting_done_at', '<=', Carbon::parse($this->resource->date)->endOfDay())
                        ->whereFurnaceId($furnace->id)
                        ->count(),
                    'planned' => MeltingTask::where('planned_melting_date', '>=', Carbon::parse($this->resource->date)->startOfDay())
                        ->where('planned_melting_date', '<=', Carbon::parse($this->resource->date)->endOfDay())
                        ->whereFurnaceId($furnace->id)
                        ->count(),
                ];
            });

            $meltingTasks = MeltingTask::query()
                ->where(function ($q) {
                    $q->where('melting_done_at', '>=', Carbon::parse($this->resource->date)->startOfDay())
                        ->where('melting_done_at', '<=', Carbon::parse($this->resource->date)->endOfDay())
                        ->whereStatus(MeltingTask::COMPLETED_STATUS);
                })->orWhere(function ($q) {
                    $q->where('planned_melting_date', '>=', Carbon::parse($this->resource->date)->startOfDay())
                        ->where('planned_melting_date', '<=', Carbon::parse($this->resource->date)->endOfDay())
                        ->whereNot('status', MeltingTask::COMPLETED_STATUS);
                })->get()
                ->groupBy('furnace.name');

            return [
                'id' => $this->resource->id,
                'rnp_plan' => $this->resource->rnp_plan,
                'rnp_fact' => $this->resource->rnp_fact,
                'm15_plan' => $this->resource->m15_plan,
                'm15_fact' => $this->resource->m15_fact,
                'kit_plan' => $this->resource->kit_plan,
                'kit_fact' => $this->resource->kit_fact,
                'fyks_weght' => $this->resource->fyks_weght,
                'rnp_weght' => $this->resource->rnp_weght,
                'china_weght' => $this->resource->china_weght,
                'weight_weght' => $this->resource->weight_weght,
                'gomo_1_1_plane' => $this->resource->gomo_1_1_plane,
                'gomo_1_1_output' => $this->resource->gomo_1_1_output,
                'gomo_1_1_shelf' => $this->resource->gomo_1_1_shelf,
                'gomo_1_2_plane' => $this->resource->gomo_1_2_plane,
                'gomo_1_2_output' => $this->resource->gomo_1_2_output,
                'gomo_1_2_shelf' => $this->resource->gomo_1_2_shelf,
                'gomo_2_1_plane' => $this->resource->gomo_2_1_plane,
                'gomo_2_1_output' => $this->resource->gomo_2_1_output,
                'gomo_2_1_shelf' => $this->resource->gomo_2_1_shelf,
                'gomo_2_2_plane' => $this->resource->gomo_2_2_plane,
                'gomo_2_2_output' => $this->resource->gomo_2_2_output,
                'gomo_2_2_shelf' => $this->resource->gomo_2_2_shelf,
                'tasks' => MeltingTaskDailyReportCollectionResource::make($meltingTasks),
                'date' => $this->resource->date->format('d.m.Y'),
                'consumptions_pillars_mg' => $this->resource->consumptions_pillars_mg,
                'consumptions_pillars_si' => $this->resource->consumptions_pillars_si,
                'consumptions_pillars_cu' => $this->resource->consumptions_pillars_cu,
                'consumptions_ingots_mg' => $this->resource->consumptions_ingots_mg,
                'consumptions_ingots_si' => $this->resource->consumptions_ingots_si,
                'consumptions_ingots_cu' => $this->resource->consumptions_ingots_cu,
                'furnaces_data' => $furnacesData,
            ];

        }

        return parent::toArray($request);
    }
}
