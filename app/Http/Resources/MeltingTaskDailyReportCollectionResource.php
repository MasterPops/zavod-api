<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MeltingTaskDailyReportCollectionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        $data = [];

        foreach ($this->resource as $key => $tasks) {
            $data[] = [
                'furnace_name' => $key,
                'tasks' => MeltingTaskDailyReportResource::collection($tasks),
            ];
        }

        return $data;
    }
}
