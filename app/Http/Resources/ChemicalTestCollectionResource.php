<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ChemicalTestCollectionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        $data = [];

        foreach ($this->resource as $key => $tests) {
            $test = $tests->first();
            $data[] = [
                'melting_task_id' => $key,
                'tests' => ChemicalTestResource::collection($tests),
                'melting_task' => MeltingTaskResource::make($test->melting_task),
                'chemical_composition' => new ChemicalCompositionResource($test->melting_task->chemical_composition),
            ];
        }

        return $data;
    }
}
