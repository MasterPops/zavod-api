<?php

namespace App\Http\Resources;

use App\Models\MeltingTask;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PlannedMeltingTaskCollectionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        $dateGroup = [];

        foreach ($this->resource as $date => $group) {
            $data = [];

            foreach ($group as $key => $tasks) {
                $data[] = [
                    'furnace_name' => $key,
                    'tasks' => MeltingTaskResource::collection($tasks),
                ];
            }

            $dateGroup[$date] = $data;
        }


        return $dateGroup;
    }
}
