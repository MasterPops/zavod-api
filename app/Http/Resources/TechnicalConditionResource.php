<?php

namespace App\Http\Resources;

use App\Models\TechnicalCondition;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TechnicalConditionResource extends JsonResource
{
    /**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    public static $wrap = 'technical_condition';

    /**
     * The "data" collection wrapper that should be applied.
     *
     * @var string
     */
    public static $collectionWrap = 'technical_conditions';

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        if ($this->resource instanceof TechnicalCondition) {

            return [
                'id' => $this->resource->id,
                'name' => $this->resource->name,
            ];

        }

        return parent::toArray($request);
    }
}
