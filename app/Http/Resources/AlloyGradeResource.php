<?php

namespace App\Http\Resources;

use App\Models\AlloyGrade;
use App\Models\Diameter;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AlloyGradeResource extends JsonResource
{
    /**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    public static $wrap = 'alloy_grade';

    /**
     * The "data" collection wrapper that should be applied.
     *
     * @var string
     */
    public static $collectionWrap = 'alloy_grades';

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        if ($this->resource instanceof AlloyGrade) {

            return [
                'id' => $this->resource->id,
                'name' => $this->resource->name,
            ];

        }

        return parent::toArray($request);
    }
}
