<?php

namespace App\Http\Resources;

use App\Models\Mark;
use App\Models\MeltingTaskUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MeltingTaskUserResource extends JsonResource
{
    /**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    public static $wrap = 'user';

    /**
     * The "data" collection wrapper that should be applied.
     *
     * @var string
     */
    public static $collectionWrap = 'users';

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        if ($this->resource instanceof MeltingTaskUser) {

            return [
                'id' => $this->resource->id,
                'user' => new UserResource($this->resource->user),
                'user_id' => $this->resource->user_id,
                'fio' => $this->resource->user ? $this->resource->user->fio : null,
                'role_for_front' => User::ROLES_FOR_FRONT[$this->resource->role],
                'role' => $this->resource->role,
            ];

        }

        return parent::toArray($request);
    }
}
