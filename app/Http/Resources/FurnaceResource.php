<?php

namespace App\Http\Resources;

use App\Models\Furnace;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class FurnaceResource extends JsonResource
{
    /**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    public static $wrap = 'furnace';

    /**
     * The "data" collection wrapper that should be applied.
     *
     * @var string
     */
    public static $collectionWrap = 'furnaces';

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        if ($this->resource instanceof Furnace) {

            return [
                'id' => $this->resource->id,
                'name' => $this->resource->name,
            ];

        }

        return parent::toArray($request);
    }
}

