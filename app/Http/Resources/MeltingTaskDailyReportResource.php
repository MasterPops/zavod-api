<?php

namespace App\Http\Resources;

use App\Models\Diameter;
use App\Models\Furnace;
use App\Models\MeltingTask;
use App\Services\MeltingTaskService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MeltingTaskDailyReportResource extends JsonResource
{
    /**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    public static $wrap = 'melting_task';

    /**
     * The "data" collection wrapper that should be applied.
     *
     * @var string
     */
    public static $collectionWrap = 'melting_tasks';

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        if ($this->resource instanceof MeltingTask) {
            $test = $this->resource->tests()->orderByDesc('created_at')->first();

            $chemMismatch = [];

            if (!empty($test)) {
                $chemicalComposition = $this->resource->chemical_composition;

                $elements = [
                    'al',
                    'si',
                    'fe',
                    'cu',
                    'mn',
                    'mg',
                    'zn',
                    'ti',
                    'ni',
                    'pb',
                    'sn',
                    'cr',
                    'cd',
                ];

                foreach ($elements as $element) {
                    $ccValue = mb_split('-', $chemicalComposition->$element);
                    if (count($ccValue) == 1) {
                        if ((double)$test->$element - (double)$ccValue[0] != 0) {
                            $chemMismatch[ucfirst($element)] = (double)$test->$element - (double)$ccValue[0];
                        }
                    } elseif (count($ccValue) == 2) {
                        if ((double)$test->$element < (double)$ccValue[0]) {
                            $chemMismatch[ucfirst($element)] = (double)$test->$element - (double)$ccValue[0];
                        } elseif ((double)$test->$element > (double)$ccValue[1]) {
                            $chemMismatch[ucfirst($element)] = (double)$test->$element - (double)$ccValue[1];
                        }
                    }
                }
            }

            return [
                'id' => $this->resource->id,
                'furnace' => new FurnaceResource($this->resource->furnace),
                'status' => $this->resource->status,
                'status_for_interface' => MeltingTask::STATUSES_FOR_INTERFACE[$this->resource->status],
                'statuses_for_interface' => MeltingTask::STATUSES_FOR_INTERFACE,
                'number' => $this->resource->number,
                'product_type_id' => $this->resource->product_type_id,
                'product_type' => new ProductTypeResource($this->resource->product_type),
                'weight' => $this->resource->weight,
                'output_weight' => $this->resource->output_weight,
                'output_alloy_grade_id' => $this->resource->output_alloy_grade_id,
                'output_alloy_grade' => new AlloyGradeResource($this->resource->output_alloy_grade),
                'master_id' => $this->resource->master_id,
                'master' => new UserResource($this->resource->master),
                'users' => $this->resource->status == MeltingTask::COMPLETED_STATUS ? MeltingTaskUserResource::collection($this->resource->users) : '',
                'batches' => BatchResource::collection($this->resource->batches),
                'planned_mvg' => $this->resource->status == MeltingTask::COMPLETED_STATUS ? MeltingTaskService::getPlaneMvg($this->resource->id) : '',
                'fact_mvg' => $this->resource->status == MeltingTask::COMPLETED_STATUS ? MeltingTaskService::getFactMvg($this->resource->id) : '',
                'difference_mvg' => $this->resource->status == MeltingTask::COMPLETED_STATUS ? round(MeltingTaskService::getPlaneMvg($this->resource->id) - MeltingTaskService::getFactMvg($this->resource->id), 4) *-1 : '',
                'alloy_grade_plane' => optional($this->resource->chemical_composition->alloy_grade)->name,
                'alloy_grade_output' => $this->resource->status == MeltingTask::COMPLETED_STATUS ? optional($this->resource->output_alloy_grade)->name : '-',
                'tests' => ChemicalTestForMeltingTaskResource::collection($this->resource->tests),
                'chemical_composition' => new ChemicalCompositionResource($this->resource->chemical_composition),
                'chem_mismatch' => $this->resource->status == MeltingTask::COMPLETED_STATUS ? $chemMismatch : '',
                'slag' => $this->resource->status == MeltingTask::COMPLETED_STATUS ? $this->resource->slag : '',
                'reason_rejection' => $this->resource->status == MeltingTask::COMPLETED_STATUS ? $this->resource->reason_rejection : '',
                'porosity' => $this->resource->status == MeltingTask::COMPLETED_STATUS ? $this->resource->porosity : '',
                'events' => $this->resource->status == MeltingTask::COMPLETED_STATUS ? $this->resource->events : '',
            ];
        }

        return parent::toArray($request);
    }
}
