<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MeltingTaskCollectionPaginateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        $data = [];

        foreach ($this->resource as $key => $tasks) {
            $data['data']['data'][] = [
                'furnace_name' => $key,
                'tasks' => MeltingTaskResource::collection($tasks),
            ];
        }

        $data['data']['page'] = $this->resource->page;
        $data['data']['per_page'] = $this->resource->perPage;
        $data['data']['total'] = $this->resource->total;

        return $data;
    }
}
