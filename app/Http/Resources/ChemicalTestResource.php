<?php

namespace App\Http\Resources;

use App\Models\ChemicalTest;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ChemicalTestResource extends JsonResource
{
    /**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    public static $wrap = 'chemical_test';

    /**
     * The "data" collection wrapper that should be applied.
     *
     * @var string
     */
    public static $collectionWrap = 'chemical_tests';

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        if ($this->resource instanceof ChemicalTest) {

            return [
                'id' => $this->resource->id,
                'alloy_grade_id' => $this->resource->alloy_grade_id,
                'alloy_grade' => new AlloyGradeResource($this->resource->alloy_grade),
                'al' => str_replace(',', '.', $this->resource->al),
                'si' => str_replace(',', '.', $this->resource->si),
                'fe' => str_replace(',', '.', $this->resource->fe),
                'cu' => str_replace(',', '.', $this->resource->cu),
                'mn' => str_replace(',', '.', $this->resource->mn),
                'mg' => str_replace(',', '.', $this->resource->mg),
                'zn' => str_replace(',', '.', $this->resource->zn),
                'ti' => str_replace(',', '.', $this->resource->ti),
                'ni' => str_replace(',', '.', $this->resource->ni),
                'pb' => str_replace(',', '.', $this->resource->pb),
                'sn' => str_replace(',', '.', $this->resource->sn),
                'cr' => str_replace(',', '.', $this->resource->cr),
                'cd' => str_replace(',', '.', $this->resource->cd),
                'proportions' => $this->resource->proportions,
                'description' => $this->resource->description,
                'melting_task_id' => $this->resource->melting_task_id,
                'melting_task' => new MeltingTaskResource($this->resource->melting_task),
                'test_type_id' => $this->resource->test_type_id,
                'test_type' => new TestTypeResource($this->resource->test_type),
                'created_at' => $this->resource->created_at->format('d.m.Y'),
                'time' => $this->resource->created_at->format('H:i'),
            ];

        }

        return parent::toArray($request);
    }
}
