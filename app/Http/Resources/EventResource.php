<?php

namespace App\Http\Resources;

use App\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class EventResource extends JsonResource
{
    /**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    public static $wrap = 'event';

    /**
     * The "data" collection wrapper that should be applied.
     *
     * @var string
     */
    public static $collectionWrap = 'events';

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        if ($this->resource instanceof Event) {
            return [
                'id' => $this->resource->id,
                'user_id' => $this->resource->user_id,
                'user' => UserResource::make($this->resource->user),
                'object_name' => $this->resource->object_name,
                'event_name' => $this->resource->event_name,
                'value_before' => $this->resource->value_before,
                'value_after' => $this->resource->value_after,
                'created_at' => $this->resource->created_at->format('d.m.Y H:i'),
            ];
        }

        return parent::toArray($request);
    }
}
