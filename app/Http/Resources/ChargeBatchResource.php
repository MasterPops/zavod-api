<?php

namespace App\Http\Resources;

use App\Models\ChargeBatch;
use App\Models\Mark;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ChargeBatchResource extends JsonResource
{
    /**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    public static $wrap = 'charge_batch';

    /**
     * The "data" collection wrapper that should be applied.
     *
     * @var string
     */
    public static $collectionWrap = 'charge_batches';

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        if ($this->resource instanceof ChargeBatch) {

            return [
                'id' => $this->resource->id,
                'batch_id' => $this->resource->batch_id,
                'number' => $this->resource->number,
                'weight' => $this->resource->weight,
                'passport' => $this->resource->passport,
            ];

        }

        return parent::toArray($request);
    }
}
