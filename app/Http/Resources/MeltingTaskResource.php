<?php

namespace App\Http\Resources;

use App\Models\Diameter;
use App\Models\MeltingTask;
use App\Services\MeltingTaskService;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MeltingTaskResource extends JsonResource
{
    /**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    public static $wrap = 'melting_task';

    /**
     * The "data" collection wrapper that should be applied.
     *
     * @var string
     */
    public static $collectionWrap = 'melting_tasks';

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        if ($this->resource instanceof MeltingTask) {

            $charges = [];

            if (!empty($this->resource->charges)) {
                $this->resource->charges->each(function ($charge) use (&$charges) {
                    $charges[] = [
                        'id' => $charge->id,
                        'name' => $charge->name,
                        'weight' => $charge->pivot->weight,
                    ];
                });
            }

            $materials = [
                'data' => [],
                'total' => 0,
                'l' => 0,
            ];

            if (!empty($this->resource->materials)) {
                $this->resource->materials->each(function ($material) use (&$materials) {
                    $materials['data'][] = [
                        'id' => $material->id,
                        'name' => $material->name,
                        'measure' => $material->display_measure ?? $material->measure,
                        'value' => $material->pivot->value,
                    ];

                    $materials['total'] += $material->pivot->value * $material->coef;

                    if ($material->measure == 'л') {
                        $materials['l'] += $material->pivot->value;
                    }
                });
            }

            $additionalMaterials = $this->resource->additionalMaterials->groupBy('additionalWork.name');
            $energyParameters = $this->resource->energyParameters;
            $energyDifferences = [];

            if (!empty($this->resource->energyParameters->groupBy('energyParameter.name'))) {
                foreach ($this->resource->energyParameters->groupBy('energyParameter.name') as $key => $item) {
                    $energyDifferences[$key] = $item->sum('difference');
                }
            }

            $prevCard = MeltingTask::where('id', '<', $this->resource->id)->orderByDesc('id')->first();
            $nextCard = MeltingTask::where('id', '>', $this->resource->id)->orderBy('id')->first();


            $chargeSum = MeltingTaskService::gerChargeSum($this->resource->id);

            $outputIrrevocablyWeight = ($chargeSum + $materials['total'] - $materials['l'])
                - ($this->resource->output_weight + $this->resource->output_trim_weight + $this->resource->output_shavings_weight
                    + $this->resource->output_reject1_weight + $this->resource->output_reject2_weight + $this->resource->output_cut_weight)
                - $this->resource->output_black_weight - $this->resource->output_white_weight - $this->resource->output_trash_weight;

            if ($chargeSum) {
                $outputIrrevocablyPercent = round($outputIrrevocablyWeight / $chargeSum * 100, 4);
            } else {
                $outputIrrevocablyPercent = null;
            }
            return [
                'id' => $this->resource->id,
                'daily_melting_number' => $this->resource->daily_melting_number,
                'furnace' => new FurnaceResource($this->resource->furnace),
                'status' => $this->resource->status,
                'status_for_interface' => MeltingTask::STATUSES_FOR_INTERFACE[$this->resource->status],
                'statuses_for_interface' => MeltingTask::STATUSES_FOR_INTERFACE,
                'number' => $this->resource->number,
                'planned_melting_date' => $this->resource->planned_melting_date ? $this->resource->planned_melting_date->format('d.m.Y') : null,
                'melting_date' => $this->resource->melting_date ? $this->resource->melting_date->format('d.m.Y') : null,
                'product_type_id' => $this->resource->product_type_id,
                'card_created' => $this->resource->card_created,
                'product_type' => new ProductTypeResource($this->resource->product_type),
                'chemical_composition' => new ChemicalCompositionResource($this->resource->chemical_composition),
                'mark_id' => $this->resource->mark_id,
                'mark' => new MarkResource($this->resource->mark),
                'filter' => $this->resource->filter,
                'weight' => $this->resource->weight,
                'charges' => $charges,
                'materials' => $materials,
                'output_weight' => $this->resource->output_weight,
                'brand' => $this->resource->brand,
                'series' => $this->resource->series,
                'output_alloy_grade_id' => $this->resource->output_alloy_grade_id,
                'output_alloy_grade' => new AlloyGradeResource($this->resource->output_alloy_grade),
                'master_id' => $this->resource->master_id,
                'master' => new UserResource($this->resource->master),
                'users' => MeltingTaskUserResource::collection($this->resource->users),
                'batches' => BatchResource::collection($this->resource->batches),
                'diameter_id' => $this->resource->diameter_id,
                'diameter' => new DiameterResource($this->resource->diameter),
                'additional_materials' => $additionalMaterials,
                'energy_parameters' => $energyParameters,
                'energy_differences' => $energyDifferences,
                'output_alloy_grade_percent' => $this->resource->output_alloy_grade_percent,
                'output_trim_weight' => $this->resource->output_trim_weight,
                'output_trim_percent' => $this->resource->output_trim_percent,
                'output_reject1_weight' => $this->resource->output_reject1_weight,
                'output_reject1_percent' => $this->resource->output_reject1_percent,
                'output_cut_count' => $this->resource->output_cut_count,
                'output_cut_weight' => $this->resource->output_cut_weight,
                'output_cut_percent' => $this->resource->output_cut_percent,
                'output_reject2_weight' => $this->resource->output_reject2_weight,
                'output_reject2_percent' => $this->resource->output_reject2_percent,
                'output_shavings_weight' => $this->resource->output_shavings_weight,
                'output_shavings_percent' => $this->resource->output_shavings_percent,
                'note_black' => $this->resource->note_black,
                'note_white' => $this->resource->note_white,
                'note_fe' => $this->resource->note_fe,
                'output_black_weight' => $this->resource->output_black_weight,
                'output_black_percent' => $this->resource->output_black_percent,
                'output_white_weight' => $this->resource->output_white_weight,
                'output_white_percent' => $this->resource->output_white_percent,
                'output_trash_weight' => $this->resource->output_trash_weight,
                'output_trash_percent' => $this->resource->output_trash_percent,
                'output_irrevocably_weight' => $outputIrrevocablyWeight,
                'output_irrevocably_percent' => $outputIrrevocablyPercent,
                'loss_sum_weight' => $this->resource->loss_sum_weight,
                'loss_sum_percent' => $this->resource->loss_sum_percent,
                'overall_result' => $this->resource->overall_result,
                'planned_mvg' => round(MeltingTaskService::getPlaneMvg($this->resource->id), 2),
                'fact_mvg' => round(MeltingTaskService::getFactMvg($this->resource->id), 2),
                'difference_mvg' => round(MeltingTaskService::getPlaneMvg($this->resource->id) - MeltingTaskService::getFactMvg($this->resource->id),2),
                'loss_sum' => round(MeltingTaskService::getLossSum($this->resource->id),2),
                'charge_sum' => $chargeSum,
                'melting_start_at' => $this->resource->melting_start_at ? $this->resource->melting_start_at->format('H:i') : null,
                'melting_start_date' => $this->resource->melting_start_at ? $this->resource->melting_start_at->format('d.m.Y') : null,
                'overflow_start_at' => $this->resource->overflow_start_at ? $this->resource->overflow_start_at->format('H:i') : null,
                'melting_done_at' => $this->resource->melting_done_at ? $this->resource->melting_done_at->format('d.m.Y H:i') : null,
                'melting_done_date' => $this->resource->melting_done_at ? $this->resource->melting_done_at->format('d.m.Y') : null,
                'prev_card' => $prevCard ? $prevCard->id : null,
                'next_card' => $nextCard ? $nextCard->id : null,
                'porosity' => $this->resource->porosity,
                'tests' => ChemicalTestForMeltingTaskResource::collection($this->resource->tests),
                'al_l' => $this->resource->al_l,
                'si_l' => $this->resource->si_l,
                'fe_l' => $this->resource->fe_l,
                'cu_l' => $this->resource->cu_l,
                'mn_l' => $this->resource->mn_l,
                'mg_l' => $this->resource->mg_l,
                'zn_l' => $this->resource->zn_l,
                'ti_l' => $this->resource->ti_l,
                'ni_l' => $this->resource->ni_l,
                'pb_l' => $this->resource->pb_l,
                'sn_l' => $this->resource->sn_l,
                'cr_l' => $this->resource->cr_l,
                'cd_l' => $this->resource->cd_l,
                'proportions_l' => $this->resource->proportions_l,
            ];
        }

        return parent::toArray($request);
    }
}
