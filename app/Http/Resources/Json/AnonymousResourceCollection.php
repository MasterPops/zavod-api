<?php

namespace App\Http\Resources\Json;

use Illuminate\Http\Resources\Json\AnonymousResourceCollection as IlluminateAnonymousResourceCollection;
use Illuminate\Support\Collection as SupportCollection;

class AnonymousResourceCollection extends IlluminateAnonymousResourceCollection
{

    /**
     * Create an HTTP response that represents the object.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function toResponse($request)
    {
        if ($this->resource instanceof SupportCollection) {
            return (new CollectionResourceResponse($this))->toResponse($request);
        }

        return parent::toResponse($request);
    }

    /**
     * Create a paginate-aware HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    protected function preparePaginatedResponse($request)
    {
        if ($this->preserveAllQueryParameters) {
            $this->resource->appends($request->query());
        } elseif (!is_null($this->queryParameters)) {
            $this->resource->appends($this->queryParameters);
        }

        return (new PaginatedResourceResponse($this))->toResponse($request);
    }

    /**
     * Get the default data wrapper for the resource.
     *
     * @return string
     */
    public function wrapper()
    {
        return property_exists($this->collects, 'collectionWrap') ? $this->collects::$collectionWrap : null;
    }

}
