<?php

namespace App\Http\Resources;

use App\Models\ChemicalTest;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ChemicalTestForMeltingTaskResource extends JsonResource
{
    /**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    public static $wrap = 'chemical_test';

    /**
     * The "data" collection wrapper that should be applied.
     *
     * @var string
     */
    public static $collectionWrap = 'chemical_tests';

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        if ($this->resource instanceof ChemicalTest) {

            return [
                'id' => $this->resource->id,
                'alloy_grade_id' => $this->resource->alloy_grade_id,
                'alloy_grade' => new AlloyGradeResource($this->resource->alloy_grade),
                'al' => $this->resource->al,
                'si' => $this->resource->si,
                'fe' => $this->resource->fe,
                'cu' => $this->resource->cu,
                'mn' => $this->resource->mn,
                'mg' => $this->resource->mg,
                'zn' => $this->resource->zn,
                'ti' => $this->resource->ti,
                'ni' => $this->resource->ni,
                'pb' => $this->resource->pb,
                'sn' => $this->resource->sn,
                'cr' => $this->resource->cr,
                'cd' => $this->resource->cd,
                'proportions' => $this->resource->proportions,
                'description' => $this->resource->description,
                'test_type_id' => $this->resource->test_type_id,
                'test_type' => new TestTypeResource($this->resource->test_type),
                'created_at' => $this->resource->created_at->format('d.m.Y'),
                'time' => $this->resource->created_at->format('H:i'),
            ];

        }

        return parent::toArray($request);
    }
}
