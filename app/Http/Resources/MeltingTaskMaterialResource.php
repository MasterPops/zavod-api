<?php

namespace App\Http\Resources;

use App\Models\Mark;
use App\Models\MeltingTaskUser;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MeltingTaskMaterialResource extends JsonResource
{
    /**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    public static $wrap = 'material';

    /**
     * The "data" collection wrapper that should be applied.
     *
     * @var string
     */
    public static $collectionWrap = 'materials';

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        if ($this->resource instanceof MeltingTaskUser) {

            return [
                'id' => $this->resource->id,
                'user' => new UserResource($this->resource->user),
                'role' => $this->resource->role,
            ];

        }

        return parent::toArray($request);
    }
}
