<?php

namespace App\Http\Resources;

use App\Models\Batch;
use App\Models\Mark;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class BatchResource extends JsonResource
{
    /**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    public static $wrap = 'batch';

    /**
     * The "data" collection wrapper that should be applied.
     *
     * @var string
     */
    public static $collectionWrap = 'batches';

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        if ($this->resource instanceof Batch) {

            return [
                'id' => $this->resource->id,
                'charge_id' => $this->resource->charge_id,
                'charge' => new ChargeResource($this->resource->charge),
                'clog' => $this->resource->clog,
                'loads' => ChargeBatchResource::collection($this->resource->loads),
            ];

        }

        return parent::toArray($request);
    }
}
