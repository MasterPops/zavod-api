<?php

namespace App\Http\Resources;

use App\Models\Material;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MaterialResource extends JsonResource
{
    /**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    public static $wrap = 'measure';

    /**
     * The "data" collection wrapper that should be applied.
     *
     * @var string
     */
    public static $collectionWrap = 'measures';

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        if ($this->resource instanceof Material) {
            return [
                'id' => $this->resource->id,
                'name' => $this->resource->name,
                'measure' => $this->resource->measure,
            ];
        }

        return parent::toArray($request);
    }
}
