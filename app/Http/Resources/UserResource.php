<?php

namespace App\Http\Resources;

use App\Models\Mark;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    public static $wrap = 'user';

    /**
     * The "data" collection wrapper that should be applied.
     *
     * @var string
     */
    public static $collectionWrap = 'marks';

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        if ($this->resource instanceof User) {
            $firstRole = $this->resource->roles()->first();

            if (!empty($firstRole)) {
                $role = User::ROLES_FOR_FRONT[$firstRole->name];
            } else {
                $role = 'Пользователь';
            }
            return [
                'id' => $this->resource->id,
                'first_name' => $this->resource->first_name,
                'last_name' => $this->resource->last_name,
                'second_name' => $this->resource->second_name,
                'fio' => $this->resource->fio,
                'initials' => $this->resource->initials,
                'roles' => $this->resource->roles()->pluck('name')->all(),
                'role' => $role,
            ];
        }

        return parent::toArray($request);
    }
}
