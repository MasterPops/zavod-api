<?php

namespace App\Http\Requests\Admin;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $userId = request()->route('id');
        $emailRules = null;

        if (!empty($userId)) {
            $user = User::find($userId);
            if (!empty($user)) {
                $emailRules = function ($attribute, $value, $fail) use ($user) {
                    if ($value !== $user->email && User::whereEmail($value)->exists()) {
                        $fail('Пользователь с таким ' . $attribute . ' уже существует');
                    }
                };
            }
        } else {
            $emailRules = 'unique:users,email';
        }

        return [
            'first_name' => 'required|string|min:3|max:255',
            'last_name' => 'nullable|string|min:3|max:255',
            'second_name' => 'required|string|min:3|max:255',
            'active' => 'required|boolean',
            'email' => [
                'email',
                $emailRules,
            ],
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //
        ];
    }
}
