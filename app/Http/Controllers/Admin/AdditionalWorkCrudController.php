<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AdditionalWorkRequest;
use App\Models\ChemicalComposition;
use App\Models\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class AdditionalWorkCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AdditionalWorkCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
//    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\AdditionalWork::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/additional-work');
        CRUD::setEntityNameStrings(trans('model/additional_work.additional_work_singular'), trans('model/additional_work.additional_work_plural'));
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::setFromDb(); // set columns from db columns.

        $this->crud->removeColumn('materials');

        if (config('app.env') == 'production') {
            $this->crud->removeButton('delete');
        }

        if (!backpack_user()->hasRole([
            User::ADMIN_ROLE,
            User::TECHNOLOGIST_ROLE,
        ])) {
            $this->crud->removeButton('create');
            $this->crud->removeButton('update');
        } else {
            $this->crud->addColumn([
                'name'  => 'active',
                'type'  => 'switch'
            ]);
        }

        $this->crud->addColumn([
            'name'  => 'active',
            'type'  => 'switch'
        ]);

        /**
         * Columns can be defined using the fluent syntax:
         * - CRUD::column('price')->type('number');
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(AdditionalWorkRequest::class);
        CRUD::setFromDb(); // set fields from db columns.

        $this->crud->addField([
            'name' => 'materials',
            'label' => 'Материалы',
            'type' => 'repeat',
            'fields' => [
                [
                    'name' => 'name',
                    'type' => 'text',
                    'label' => 'Наименование',
                    'wrapper' => ['class' => 'form-group col-md-8'],
                ],
                [
                    'name' => 'measure',
                    'label' => 'Единица измерения',
                    'type' => 'select_from_array',
                    'options' => [
                        'л' => 'л',
                    ],
                    'allows_null' => false,
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
            ],

            // optional
            'new_item_label' => 'Добавить материал',
            'init_rows' => 0,
        ]);

        /**
         * Fields can be defined using the fluent syntax:
         * - CRUD::field('price')->type('number');
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
