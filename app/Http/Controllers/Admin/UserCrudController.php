<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\UserCreateRequest;
use App\Http\Requests\Admin\UserUpdateRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Support\Facades\Hash;

/**
 * Class UserCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class UserCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
//    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation { update as traitUpdate; }


    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\User::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/user');
        CRUD::setEntityNameStrings(trans('model/user.user_singular'), trans('model/user.user_plural'));
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
//        CRUD::setFromDb(); // set columns from db columns.
        CRUD::addColumn([
            'name' => 'second_name',
            'label' => 'Фамилия',
            'type' => 'text',
        ]);
        CRUD::addColumn([
            'name' => 'first_name',
            'label' => 'Имя',
            'type' => 'text',
        ]);

        CRUD::addColumn([
            'name' => 'last_name',
            'label' => 'Отчество',
            'type' => 'text',
        ]);


        CRUD::addColumn([
            'name' => 'active',
            'label' => 'Активность',
            'type' => 'check',
        ]);

        CRUD::addColumn([
            'name' => 'roles',
            'label' => 'Роли',
            'type' => 'model_function',
            'function_name' => 'getRoles',
        ]);
        /**
         * Columns can be defined using the fluent syntax:
         * - CRUD::column('price')->type('number');
         */

        if (config('app.env') == 'production') {
            $this->crud->removeButton('delete');
        }

        if (!backpack_user()->hasRole([
            \App\Models\User::ADMIN_ROLE,
            \App\Models\User::DIRECTOR_ROLE,
            \App\Models\User::TECHNOLOGIST_ROLE,
        ])) {
            $this->crud->removeButton('create');
            $this->crud->removeButton('update');
        }
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(UserCreateRequest::class);
        CRUD::setFromDb(); // set fields from db columns.
        $this->createFields();
        $this->setupFields();

        /**
         * Fields can be defined using the fluent syntax:
         * - CRUD::field('price')->type('number');
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        CRUD::setValidation(UserUpdateRequest::class);
        CRUD::setFromDb(); // set fields from db columns.
        $this->updateFields();
        $this->setupFields();
    }

    public function setupFields()
    {
        $this->crud->addField([
            'type' => 'select_multiple',
            'name' => 'roles',
            'attribute' => 'title',
        ]);
    }

    public function updateFields()
    {
    }

    public function createFields()
    {

    }

    public function update()
    {
        $request = $this->crud->getRequest();
        $request->request->remove('password_confirmation');
        $request->request->remove('roles_show');
        $request->request->remove('permissions_show');

        // Encrypt password if specified.
        if ($request->input('password')) {
            $request->request->set('password', Hash::make($request->input('password')));
        } else {
            $request->request->remove('password');
        }

        // do something after save
        return $this->traitUpdate();
    }

}
