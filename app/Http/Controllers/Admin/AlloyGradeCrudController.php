<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\AlloyGradeRequest;
use App\Models\AlloyGrade;
use App\Models\ChemicalComposition;
use App\Models\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class AlloyGradeCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AlloyGradeCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
//    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\AlloyGrade::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/alloy-grade');
        CRUD::setEntityNameStrings(trans('model/alloy_grade.alloy_grade_singular'), trans('model/alloy_grade.alloy_grade_plural'));

        AlloyGrade::saved(function ($entry) {
            $request = $this->crud->validateRequest();

            $this->setChemicalCompositions($request->get('chemical_compositions'), $entry->id);
        });
    }

    public function setChemicalCompositions($value, $id)
    {
        $chemicalCompositionsData = json_decode($value, true);

        if (!empty($chemicalCompositionsData)) {
            foreach ($chemicalCompositionsData as $chemicalComposition) {
                foreach ($chemicalComposition as $key => &$value) {
                    if ($key != 'id') {
                        if (!filled($value)) {
                            $value = null;
                        }
                    }
                }

                if (filled($chemicalComposition['id'])) {
                    ChemicalComposition::whereId($chemicalComposition['id'])->update($chemicalComposition);
                } else {
                    $chemicalComposition['alloy_grade_id'] = $id;
                    ChemicalComposition::create($chemicalComposition);
                }
            }
        }
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::setFromDb(); // set columns from db columns.

        CRUD::addColumn([
            'name' => 'technical_conditions',
            'label' => 'ТУ/ГОСТы',
            'type' => 'model_function',
            'function_name' => 'getChemicalCompositionsList',
        ]);

        if (config('app.env') == 'production') {
            $this->crud->removeButton('delete');
        }

        if (!backpack_user()->hasRole([
            User::ADMIN_ROLE,
            User::TECHNOLOGIST_ROLE,
        ])) {
            $this->crud->removeButton('create');
            $this->crud->removeButton('update');
        }

        /**
         * Columns can be defined using the fluent syntax:
         * - CRUD::column('price')->type('number');
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(AlloyGradeRequest::class);
        CRUD::setFromDb(); // set fields from db columns.
        $this->setupFields();


        /**
         * Fields can be defined using the fluent syntax:
         * - CRUD::field('price')->type('number');
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function setupFields()
    {
        $chemicalElementFields = [];

        foreach (ChemicalComposition::CHEMICAL_ELEMENTS as $element) {

            $chemicalElementFields[] = [
                'name' => $element,
                'type' => 'text',
                'label' => trans("model/chemical_composition.labels.$element"),
                'wrapper' => ['class' => 'form-group col-md-1'],
            ];
        }

        $this->crud->addField([
            'name' => 'chemical_compositions',
            'label' => trans('model/technical_condition.labels.chemical_compositions'),
            'type' => 'repeat',
            'fields' => array_merge([
                [
                    'name' => 'id',
                    'type' => 'hidden',
                ],
                [
                    'name' => 'technical_condition_id',
                    'label' => trans('model/chemical_composition.labels.technical_condition'),
                    'type' => 'select_from_array',
                    'options' => ChemicalComposition::technical_condition_options(),
                    'allows_null' => false,
                    'wrapper' => ['class' => 'form-group col-md-2'],
                ],
//                [
//                    'name' => 'diameter_id',
//                    'label' => trans('model/chemical_composition.labels.dia'),
//                    'type' => 'select_from_array',
//                    'options' => ChemicalComposition::diameter_options(),
//                    'allows_null' => true,
//                    'wrapper' => ['class' => 'form-group col-md-2'],
//                ],
            ], $chemicalElementFields, [
                [
                    'name' => 'proportions',
                    'type' => 'text',
                    'label' => trans('model/chemical_composition.labels.proportions'),
                    'wrapper' => ['class' => 'form-group col-md-3'],
                ],
                [
                    'name' => 'description',
                    'type' => 'text',
                    'label' => trans('model/chemical_composition.labels.description'),
                    'wrapper' => ['class' => 'form-group col-md-4'],
                ],
            ]),

            // optional
            'new_item_label' => trans('model/technical_condition.labels.add_chemical_composition'),
            'init_rows' => 0,
        ]);
    }
}
