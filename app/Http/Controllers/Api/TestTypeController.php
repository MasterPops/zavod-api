<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\TestTypeResource;
use App\Models\TestType;
use Illuminate\Http\Request;

class TestTypeController extends Controller
{
    public function search(Request $request)
    {
        $value = $request->get('value') ?? '';

        return TestTypeResource::collection(TestType::active()
            ->where('name', 'like', "%$value%")
            ->paginate($request->get('per_page')));
    }

    public function read(int $id)
    {
        $testType = TestType::find($id);

        return TestTypeResource::make($testType);
    }
}
