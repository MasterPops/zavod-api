<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ChargeResource;
use App\Models\Charge;
use Illuminate\Http\Request;

class ChargeController extends Controller
{
    public function search(Request $request)
    {
        $value = $request->get('value') ?? '';

        return Charge::active()
            ->where('name', 'like', "%$value%")
            ->paginate($request->get('per_page'));
    }

    public function index()
    {
        $charges = Charge::active()
            ->orderBy('name')
            ->get();

        return ChargeResource::collection($charges);
    }
}
