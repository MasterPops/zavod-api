<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\AlloyGradeResource;
use App\Http\Resources\FurnaceResource;
use App\Http\Resources\MeltingTaskCollectionResource;
use App\Http\Resources\MeltingTaskResource;
use App\Http\Resources\PlannedMeltingTaskCollectionResource;
use App\Http\Resources\ProductTypeResource;
use App\Models\AdditionalWork;
use App\Models\AlloyGrade;
use App\Models\Batch;
use App\Models\ChargeBatch;
use App\Models\ChemicalComposition;
use App\Models\CostAdditionalMaterial;
use App\Models\CostEnergyParameter;
use App\Models\Diameter;
use App\Models\EnergyParameter;
use App\Models\Event;
use App\Models\Furnace;
use App\Models\Mark;
use App\Models\Material;
use App\Models\MeltingTask;
use App\Models\MeltingTaskCharge;
use App\Models\MeltingTaskUser;
use App\Models\ProductType;
use App\Models\User;
use App\Services\ExportService;
use App\Services\MeltingTaskService;
use App\Services\WorkDayService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Mockery\Exception;

class MeltingTaskController extends Controller
{
    protected Carbon $nowStartDay;
    protected Carbon $nowEndDay;

    public function __construct()
    {
        $this->nowStartDay = WorkDayService::getStartDay();
        $this->nowEndDay = WorkDayService::getEndDay();
        $this->nowStartDay = now()->startOfDay();
        $this->nowEndDay = now()->endOfDay();
    }

    public function numberSearch(Request $request)
    {
        $value = $request->get('value') ?? '';

        return MeltingTaskResource::collection(MeltingTask::where('number', 'like', "%$value%")
            ->paginate($request->get('per_page')));
    }

    public function getFromNumber(int $number)
    {
        $meltingTask = MeltingTask::whereNumber($number)->first();

        if (!empty($meltingTask)) {
            return $meltingTask->id;
        } else {
            return null;
        }
    }

    public function overdue(Request $request)
    {
        $overdueMeltingTasks = MeltingTask::query()
            ->whereStatus(MeltingTask::PLANNING_STATUS)
            ->where('planned_melting_date', '<', now()->startOfDay())
            ->orderBy('daily_melting_number')
            ->get();

        return new MeltingTaskCollectionResource($overdueMeltingTasks->groupBy('furnace.name'));
    }

    public function working(Request $request)
    {
        $workingMeltingTasks = MeltingTask::query()
            ->whereIn('status', [
                MeltingTask::PREPARATION_STATUS,
                MeltingTask::STARTED_STATUS,
                MeltingTask::OVERFLOW_STATUS,
                MeltingTask::FAIL_STATUS,
                MeltingTask::GOMO_STATUS,
                MeltingTask::CANCELLED_STATUS,
            ])->orderBy('daily_melting_number')
            ->get();

        return new MeltingTaskCollectionResource($workingMeltingTasks->groupBy('furnace.name'));

    }

    public function planned(Request $request)
    {
        $plannedMeltingTasks = [];

        $dates = DB::table('melting_tasks')
            ->select('planned_melting_date')
            ->where('status', MeltingTask::PLANNING_STATUS)
            ->where('planned_melting_date', '>=', now()->startOfDay())
            ->groupBy('planned_melting_date')
            ->pluck('planned_melting_date')
            ->all();

        if (!empty($dates)) {
            foreach ($dates as $date) {
                $plannedMeltingTasks[Carbon::parse($date)->format('d.m.Y')] = MeltingTask::query()
                    ->where('planned_melting_date', Carbon::parse($date))
                    ->whereStatus(MeltingTask::PLANNING_STATUS)
                    ->orderBy('daily_melting_number')
                    ->get()
                    ->groupBy('furnace.name');
            }
        } else {
            for ($i = 0; $i < 1; $i++) {
                $plannedMeltingTasks[now()->addDays($i)->format('d.m.Y')] = MeltingTask::query()
                    ->where('planned_melting_date', '>=', now()->startOfDay()->addDays($i))
                    ->where('planned_melting_date', '<', now()->endOfDay()->addDays($i))
                    ->whereStatus(MeltingTask::PLANNING_STATUS)
                    ->orderBy('daily_melting_number')
                    ->get()
                    ->groupBy('furnace.name');
            }
        }

        return new PlannedMeltingTaskCollectionResource($plannedMeltingTasks);

    }

    public function withoutDate(Request $request)
    {
        $withoutDateMeltingTasks = MeltingTask::query()
            ->whereNull('planned_melting_date')
            ->orderBy('daily_melting_number')
            ->get();

        return new MeltingTaskCollectionResource($withoutDateMeltingTasks->groupBy('furnace.name'));

    }

    public function done(Request $request)
    {
        $perPage = $request->get('per_page') ?? 20;

        $doneTasks = MeltingTask::query()
            ->whereStatus(MeltingTask::COMPLETED_STATUS)
            ->orderByDesc('melting_done_at')
            ->orderBy('daily_melting_number')
            ->paginate($perPage);


        return MeltingTaskResource::collection($doneTasks);
    }

    /**
     * @throws ValidationException
     */
    public function save(Request $request)
    {
        $request->validate([
            'furnace_id' => 'required|integer|exists:furnaces,id',
            'diameter_id' => 'nullable|integer|exists:diameters,id',
            'planned_melting_date' => 'date',
            'product_type_id' => 'required|integer|exists:product_types,id',
            'chemical_composition_id' => 'required|integer|exists:chemical_compositions,id',
            'mark_id' => 'nullable|integer|exists:marks,id',
            'filter' => 'required|integer|in:1,0',
            'weight' => 'required',
            'charges' => 'nullable|array',
            'charges.*.weight' => 'nullable',
            'charges.*.id' => 'required',
        ]);


        $meltingCardData = $request->only([
            'daily_melting_number',
            'furnace_id',
            'diameter_id',
            'product_type_id',
            'chemical_composition_id',
            'mark_id',
            'filter',
            'weight',
            'al_l',
            'si_l',
            'fe_l',
            'cu_l',
            'mn_l',
            'mg_l',
            'zn_l',
            'ti_l',
            'ni_l',
            'pb_l',
            'sn_l',
            'cr_l',
            'cd_l',
            'proportions_l',
        ]);

        if (!isset($meltingCardData['mark_id']) || !$meltingCardData['mark_id']) {
            $meltingCardData['mark_id'] = env('NOT_MARKING_ID', 1);
        }


        if (!empty($request->get('planned_melting_date'))) {
            $date = Carbon::parse($request->get('planned_melting_date'))->format('d.m.Y');

            $meltingCardData['planned_melting_date'] = Carbon::parse($date)->startOfDay();
        }

        $meltingTask = MeltingTask::create($meltingCardData);

        Event::add(Auth::id(), Event::OBJECT_PROD_PLAN, 'Создано задание в систему', '', $meltingTask->id);


        if (!empty($request->get('charges'))) {
            $charges = $request->get('charges');

            $syncData = [];

            foreach ($charges as $charge) {
                if (isset($syncData[$charge['id']])) {
                    $syncData[$charge['id']] = [
                        'weight' => $syncData[$charge['id']]['weight'] += $charge['weight'] ?? 0
                    ];
                } else {
                    $syncData[$charge['id']] = [
                        'weight' => $charge['weight'] ?? null
                    ];
                }
            }

            if (!empty($syncData)) {
                $meltingTask->charges()->sync($syncData);
            }
        }
    }

    public function update(Request $request)
    {


        try {
            $meltingTask = MeltingTask::findOrFail($request->id);
            $request->validate([
                'status' => [
                    'string',
                    Rule::in(MeltingTask::STATUSES)
                ],
            ]);
            $data = $request->only([
                'status',
                'output_weight',
                'brand',
                'series',
                'output_alloy_grade_id',
                'daily_melting_number',
                'planned_melting_date',
                'mark_id',
                'furnace_id',
                'product_type_id',
                'chemical_composition_id',
                'filter',
                'weight',
                'diameter_id',
                'output_trim_weight',
                'output_reject1_weight',
                'output_cut_weight',
                'output_shavings_weight',
                'output_reject2_weight',
                'output_cut_count',
                'note_black',
                'note_white',
                'note_fe',
                'output_black_weight',
                'output_black_percent',
                'output_white_weight',
                'output_white_percent',
                'output_trash_weight',
                'output_trash_percent',
                'output_irrevocably_weight',
                'output_irrevocably_percent',
                'al_l',
                'si_l',
                'fe_l',
                'cu_l',
                'mn_l',
                'mg_l',
                'zn_l',
                'ti_l',
                'ni_l',
                'pb_l',
                'sn_l',
                'cr_l',
                'cd_l',
                'proportions_l',
                'slag',
                'events',
                'reason_rejection',
                'porosity',
                'charges' => 'nullable|array',
                'charges.*.weight' => 'nullable',
                'charges.*.id' => 'required',
            ]);
            if (isset($data['reason_rejection']) && $meltingTask->reason_rejection != $data['reason_rejection']) {
                Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $meltingTask->melting_done_at->format('d.m.Y') . ')',
                    "Изменение значения \"Причина отклонения МВГ от плана\"",
                    $meltingTask->reason_rejection, $data['reason_rejection']);
            }
            if (isset($data['events']) && $meltingTask->events != $data['events']) {
                Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $meltingTask->melting_done_at->format('d.m.Y') . ')',
                    "Изменение значения \"Мероприятия\"",
                    $meltingTask->events, $data['events']);
            }
            if (isset($data['slag']) && $meltingTask->slag != $data['slag']) {
                Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $meltingTask->melting_done_at->format('d.m.Y') . ')',
                    "Изменение значения \"Шлак\"",
                    $meltingTask->slag, $data['slag']);
            }
            if (isset($data['output_weight']) && $meltingTask->output_weight != $data['output_weight']) {
                $outputAlloyGrade = AlloyGrade::find($meltingTask->output_alloy_grade_id);

                Event::add(Auth::id(), Event::OBJECT_MELT_CARD . ' (' . $meltingTask->number . ')',
                    "Изменение значения \"Выход продукции\": $outputAlloyGrade->name",
                    $meltingTask->output_weight, $data['output_weight']);
            }
            if (isset($data['output_alloy_grade_id']) && $meltingTask->output_alloy_grade_id != $data['output_alloy_grade_id']) {
                $outputAlloyGrade = AlloyGrade::find($data['output_alloy_grade_id']);

                Event::add(Auth::id(), Event::OBJECT_MELT_CARD . ' (' . $meltingTask->number . ')',
                    "Изменение значения \"Марка полученного сплава\"",
                    optional($meltingTask->output_alloy_grade)->name, $outputAlloyGrade->name);
            }
            if (isset($data['brand']) && $meltingTask->brand != $data['brand']) {
                Event::add(Auth::id(), Event::OBJECT_MELT_CARD . ' (' . $meltingTask->number . ')',
                    "Изменение значения \"Клеймо\"",
                    $meltingTask->brand, $data['brand']);
            }
            if (isset($data['series']) && $meltingTask->series != $data['series']) {
                Event::add(Auth::id(), Event::OBJECT_MELT_CARD . ' (' . $meltingTask->number . ')',
                    "Изменение значения \"Серия\"",
                    $meltingTask->series, $data['series']);
            }
            if (isset($data['output_trim_weight']) && $meltingTask->output_trim_weight != $data['output_trim_weight']) {
                Event::add(Auth::id(), Event::OBJECT_MELT_CARD . ' (' . $meltingTask->number . ')',
                    "Изменение значения \"Обрезь (плавка)\"",
                    $meltingTask->output_trim_weight, $data['output_trim_weight']);
            }
            if (isset($data['output_reject1_weight']) && $meltingTask->output_reject1_weight != $data['output_reject1_weight']) {
                Event::add(Auth::id(), Event::OBJECT_MELT_CARD . ' (' . $meltingTask->number . ')',
                    "Изменение значения \"Литейн. брак (плавка)\"",
                    $meltingTask->output_reject1_weight, $data['output_reject1_weight']);
            }
            if (isset($data['output_shavings_weight']) && $meltingTask->output_shavings_weight != $data['output_shavings_weight']) {
                Event::add(Auth::id(), Event::OBJECT_MELT_CARD . ' (' . $meltingTask->number . ')',
                    "Изменение значения \"Стружка (плавка)\"",
                    $meltingTask->output_shavings_weight, $data['output_shavings_weight']);
            }
            if (isset($data['output_cut_count']) && $meltingTask->output_cut_count != $data['output_cut_count']) {
                Event::add(Auth::id(), Event::OBJECT_MELT_CARD . ' (' . $meltingTask->number . ')',
                    "Изменение значения \"Шт. порезано\"",
                    $meltingTask->output_cut_count, $data['output_cut_count']);
            }
            if (isset($data['output_cut_weight']) && $meltingTask->output_cut_weight != $data['output_cut_weight']) {
                Event::add(Auth::id(), Event::OBJECT_MELT_CARD . ' (' . $meltingTask->number . ')',
                    "Изменение значения \"Вес порезаного\"",
                    $meltingTask->output_cut_weight, $data['output_cut_weight']);
            }
            if (isset($data['output_reject2_weight']) && $meltingTask->output_reject2_weight != $data['output_reject2_weight']) {
                Event::add(Auth::id(), Event::OBJECT_MELT_CARD . ' (' . $meltingTask->number . ')',
                    "Изменение значения \"Литейный брак (ротор)\"",
                    $meltingTask->output_reject2_weight, $data['output_reject2_weight']);
            }
            if (isset($data['note_black']) && $meltingTask->note_black != $data['note_black']) {
                Event::add(Auth::id(), Event::OBJECT_MELT_CARD . ' (' . $meltingTask->number . ')',
                    "Изменение значения \"Шлак черный\"",
                    $meltingTask->note_black, $data['note_black']);
            }
            if (isset($data['note_white']) && $meltingTask->note_white != $data['note_white']) {
                Event::add(Auth::id(), Event::OBJECT_MELT_CARD . ' (' . $meltingTask->number . ')',
                    "Изменение значения \"Шлак белый\"",
                    $meltingTask->note_white, $data['note_white']);
            }
            if (isset($data['note_fe']) && $meltingTask->note_fe != $data['note_fe']) {
                Event::add(Auth::id(), Event::OBJECT_MELT_CARD . ' (' . $meltingTask->number . ')',
                    "Изменение значения \"Fe\"",
                    $meltingTask->note_fe, $data['note_fe']);
            }
            if (isset($data['output_black_weight']) && $meltingTask->output_black_weight != $data['output_black_weight']) {
                Event::add(Auth::id(), Event::OBJECT_MELT_CARD . ' (' . $meltingTask->number . ')',
                    "Изменение значения \"Шлак черный (вес)\"",
                    $meltingTask->output_black_weight, $data['output_black_weight']);
            }
            if (isset($data['output_white_weight']) && $meltingTask->output_white_weight != $data['output_white_weight']) {
                Event::add(Auth::id(), Event::OBJECT_MELT_CARD . ' (' . $meltingTask->number . ')',
                    "Изменение значения \"Шлак белый (вес)\"",
                    $meltingTask->output_white_weight, $data['output_white_weight']);
            }
            if (isset($data['output_trash_weight']) && $meltingTask->output_trash_weight != $data['output_trash_weight']) {
                Event::add(Auth::id(), Event::OBJECT_MELT_CARD . ' (' . $meltingTask->number . ')',
                    "Изменение значения \"Мусор (вес)\"",
                    $meltingTask->output_trash_weight, $data['output_trash_weight']);
            }
            $beforeData = [];
            $afterData = [];
            if (!empty($request->get('materials'))) {
                $materials = $request->get('materials');

                $syncData = [];

                foreach ($materials as $material) {
                    $oldMaterial = $meltingTask->materials()->whereMaterialId($material['id'])->first();

                    $oldValue = null;
                    if (!empty($oldMaterial)) {
                        $oldValue = $oldMaterial->pivot->value;
                    }


                    if ($oldValue != $material['value']) {
                        Event::add(Auth::id(), Event::OBJECT_MELT_CARD . ' (' . $meltingTask->number . ')',
                            "Изменение количества материала: $oldMaterial->name",
                            $oldValue, $material['value']);
                    }

                    $syncData[$material['id']] = [
                        'value' => $material['value'],
                    ];
                }

                if (!empty($syncData)) {
                    $meltingTask->materials()->sync($syncData);
                }
            }
            if (!empty($request->get('additional_materials'))) {
                $additionalMaterials = $request->get('additional_materials');

                if (!empty($additionalMaterials)) {
                    foreach ($additionalMaterials as $workType) {
                        if (!empty($workType)) {
                            foreach ($workType as $material) {
                                $addMaterial = CostAdditionalMaterial::find($material['id']);

                                $oldValue = null;
                                if (!empty($addMaterial)) {
                                    $oldValue = $addMaterial->value;
                                }

                                if ($oldValue != $material['value']) {
                                    Event::add(Auth::id(), Event::OBJECT_MELT_CARD . ' (' . $meltingTask->number . ')',
                                        "Изменение количества материала: $addMaterial->name",
                                        $oldValue, $material['value']);
                                }
                                CostAdditionalMaterial::whereId($material['id'])->update([
                                    'value' => $material['value'],
                                ]);
                            }
                        }
                    }
                }
            }
            if (!empty($request->get('charges'))) {
                $oldCharges = MeltingTaskCharge::whereMeltingTaskId($meltingTask->id)->get();

                $oldCharges->each(function ($charge) use (&$beforeData) {
                    $beforeData['Шихта'][] = $charge->charge->name . ' - ' . $charge->weight;
                });

                $charges = $request->get('charges');

                $syncData = [];

                foreach ($charges as $charge) {
                    if (isset($syncData[$charge['id']])) {
                        $syncData[$charge['id']] = [
                            'weight' => $syncData[$charge['id']]['weight'] += $charge['weight']
                        ];
                    } else {
                        $syncData[$charge['id']] = [
                            'weight' => $charge['weight']
                        ];
                    }
                }

                if (!empty($syncData)) {
                    $meltingTask->charges()->sync($syncData);
                }

                $newCharges = MeltingTaskCharge::whereMeltingTaskId($meltingTask->id)->get();

                $newCharges->each(function ($charge) use (&$afterData) {
                    $afterData['Шихта'][] = $charge->charge->name . ' - ' . $charge->weight;
                });
            }
            if (!empty($request->get('energy_parameters'))) {
                $energyParameters = $request->get('energy_parameters');

                if (!empty($energyParameters)) {
                    foreach ($energyParameters as $parameter) {
                        if ($parameter['end_value'] > $parameter['start_value']) {
                            $difference = $parameter['end_value'] - $parameter['start_value'];

                            $parameterModel = EnergyParameter::find($parameter['energy_parameter_id']);

                            if (!empty($parameterModel)) {
                                $energyMaterials = $parameterModel->materials;

                                $energyMaterials = json_decode($energyMaterials, true);

                                if (!empty($energyMaterials)) {
                                    foreach ($energyMaterials as $material) {
                                        if ($material['name'] == $parameter['name'] && $material['coef'] != 1) {
                                            $difference *= $material['coef'];
                                        }
                                    }
                                }
                            }
                        } else {
                            $difference = 0;
                        }
                        $cost = CostEnergyParameter::find($parameter['id']);

                        if ($cost->start_value != $parameter['start_value'] || $cost->end_value != $parameter['end_value']) {

                            Event::add(Auth::id(), Event::OBJECT_MELT_CARD . ' (' . $meltingTask->number . ')',
                                "Изменение значения расхода энергии: $cost->name",
                                "Значение до: " . ($cost->start_value ?? '-') . " , значение после: " . ($cost->end_value ?? '-'), 'Значение до: ' . ($parameter['start_value'] ?? '-') .
                                ', значение после: ' . ($parameter['end_value'] ?? '-'));
                        }

                        $cost->update([
                            'start_value' => $parameter['start_value'],
                            'end_value' => $parameter['end_value'],
                            'difference' => $difference,
                        ]);
                    }
                }
            }
            if (isset($request->users)) {
                $users = $request->get('users');

                $notDeleteIds = [];

                foreach ($users as $user) {
                    $taskUser = MeltingTaskUser::whereUserId($user['user_id'] ?? null)
                        ->whereMeltingTaskId($meltingTask->id)
                        ->first();


                    if (!empty($taskUser)) {
                        if ($taskUser->role != $user['role']) {
                            Event::add(Auth::id(), Event::OBJECT_MELT_CARD . ' (' . $meltingTask->number . ')',
                                "Обновление роли пользователя: " . $taskUser->user->fio,
                                $taskUser->role ? User::ROLES_FOR_FRONT[$taskUser->role] : '-', User::ROLES_FOR_FRONT[$user['role']] ?? '-');

                            $taskUser->update([
                                'role' => $user['role'],
                                'user_id' => $user['user_id'],
                            ]);
                        }
                    } else {
                        if (!empty($user['role'])) {
                            $taskUser = MeltingTaskUser::create([
                                'user_id' => $user['user_id'] ?? null,
                                'role' => $user['role'],
                                'melting_task_id' => $meltingTask->id,
                            ]);

                            Event::add(Auth::id(), Event::OBJECT_MELT_CARD . ' (' . $meltingTask->number . ')',
                                "Добавление пользователя: " . $taskUser->user->fio,
                                '', 'Роль: ' . $user['role'] ? User::ROLES_FOR_FRONT[$user['role']] : '-');
                        }
                    }
                    $notDeleteIds[] = $taskUser->id;
                }

                $deletedUsers = MeltingTaskUser::whereNotIn('id', $notDeleteIds)
                    ->whereMeltingTaskId($meltingTask->id)
                    ->get();

                if (!empty($deletedUsers)) {
                    $deletedUsers->each(function ($deletedUser) use ($meltingTask) {
                        if (!empty($deletedUser->user)) {
                            Event::add(Auth::id(), Event::OBJECT_MELT_CARD . ' (' . $meltingTask->number . ')',
                                "Удаление пользователя: " . $deletedUser->user->fio,
                                '', 'Роль: ' . ($deletedUser->role ? User::ROLES_FOR_FRONT[$deletedUser->role] : '-'));
                        }
                    });
                }

                MeltingTaskUser::whereNotIn('id', $notDeleteIds)
                    ->whereMeltingTaskId($meltingTask->id)
                    ->delete();

            }
            if ($request->get('daily_melting_number') && $request->get('planned_melting_date')) {
                $plannedMeltingTask = MeltingTask::where('planned_melting_date', Carbon::parse($request->get('planned_melting_date'))->startOfDay())
                    ->where('daily_melting_number', $request->get('daily_melting_number'))
                    ->first();

                if (!empty($plannedMeltingTask)) {
                    $plannedMeltingTask->update([
                        'daily_melting_number' => $meltingTask->daily_melting_number,
                    ]);
                }
            }
            if ($request->get('done_time')) {
                $data['melting_done_at'] = Carbon::parse($request->get('done_time'));
            }
            if ($request->get('chemical_composition_id')) {
                $newCc = ChemicalComposition::with('alloy_grade')
                    ->find($request->get('chemical_composition_id'));

                if (!empty($newCc)) {
                    if ($newCc->alloy_grade->id != $meltingTask->chemical_composition->alloy_grade->id) {
                        $beforeData['Марка сплава'] = $meltingTask->chemical_composition->alloy_grade->name;
                        $afterData['Марка сплава'] = $newCc->alloy_grade->name;
                    }

                    if ($newCc->technical_condition->id != $meltingTask->chemical_composition->technical_condition->id) {
                        $beforeData['ГОСТ/ТУ'] = $meltingTask->chemical_composition->technical_condition->name;
                        $afterData['ГОСТ/ТУ'] = $newCc->technical_condition->name;
                    }
                }
            }
            if ($request->get('product_type_id')) {
                $pt = ProductType::find($request->get('product_type_id'));

                if (!empty($pt)) {
                    if ($pt->id != $meltingTask->product_type->id) {
                        $beforeData['Тип изделия'] = $meltingTask->product_type->name;
                        $afterData['Тип изделия'] = $pt->name;
                    }
                }
            }
            $diameterId = $meltingTask->diameter_id;
            $furnaceId = $meltingTask->furnace_id;
            $markId = $meltingTask->mark_id;
            $weight = $meltingTask->weight;
            $filter = $meltingTask->filter;
            $meltingTask->update($data);
            $meltingTask->fresh();
            if ($meltingTask->furnace_id != $furnaceId) {
                $d = Furnace::find($furnaceId);

                if (!empty($d)) {
                    if ($d->id != $meltingTask->furnace->id) {
                        $beforeData['Печь'] = $d->name;
                        $afterData['Печь'] = optional($meltingTask->furnace)->name;
                    }
                }
            }
            if ($meltingTask->mark_id != $markId) {
                $d = Mark::find($markId);

                if (!empty($d)) {
                    if ($d->id != $meltingTask->mark->id) {
                        $beforeData['Маркировка'] = $d->name;
                        $afterData['Маркировка'] = optional($meltingTask->mark)->name;
                    }
                }
            }
            if ($meltingTask->weight != $weight) {
                $beforeData['Вес Гп, кг'] = $weight;
                $afterData['Вес Гп, кг'] = $meltingTask->weight;
            }
            if ($meltingTask->filter != $filter) {
                $beforeData['Фильтр'] = $filter ? 'Да' : 'Нет';
                $afterData['Фильтр'] = $meltingTask->filter ? 'Да' : 'Нет';
            }
            if ($meltingTask->diameter_id != $diameterId) {
                $d = Diameter::find($diameterId);

                if (!empty($d)) {
                    if ($d->id != $meltingTask->product_type->id) {
                        $beforeData['Диаметр столба'] = $d->name;
                        $afterData['Диаметр столба'] = optional($meltingTask->diameter)->name;
                    }
                } elseif (!empty($meltingTask->diameter)) {
                    $beforeData['Диаметр столба'] = null;
                    $afterData['Диаметр столба'] = optional($meltingTask->diameter)->name;
                }
            }
            if (!empty($beforeData) && !empty($afterData)) {
                $chargesBefore = implode(', ', $beforeData['Шихта']);
                $chargesAfter = implode(', ', $afterData['Шихта']);

                $beforeData['Шихта'] = $afterData['Шихта'] = null;

                $otherBefore = [];

                foreach ($beforeData as $key => $value) {
                    if ($value) {
                        $otherBefore[] = "$key: $value";
                    }
                }

                $otherBefore[] = "Шихта: $chargesBefore";

                $otherAfter = [];

                foreach ($afterData as $key => $value) {
                    if ($value) {
                        $otherAfter[] = "$key: $value";
                    }
                }

                $otherAfter[] = "Шихта: $chargesAfter";

                $otherBefore = implode(', ', $otherBefore);
                $otherAfter = implode(', ', $otherAfter);


                Event::add(Auth::id(), Event::OBJECT_PROD_PLAN, 'Изменено задание на плавку',
                    $otherBefore, $otherAfter);
            }
            return 1;
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    public
    function massDouble(Request $request)
    {
        $ids = $request->get('ids');

        if (!empty($ids)) {
            $meltingTaskIds = [];
            foreach ($ids as $key => $id) {
                if ($id) {
                    $meltingTaskIds[] = $key;
                }
            }

            $meltingTasks = MeltingTask::whereIn('id', $meltingTaskIds)->get();

            if (!empty($meltingTasks)) {
                $meltingTasks->each(function ($task) {
                    $newMeltingTask = MeltingTask::create($task->only([
                        'furnace_id',
                        'planned_melting_date',
                        'product_type_id',
                        'diameter_id',
                        'chemical_composition_id',
                        'mark_id',
                        'filter',
                        'weight',
                        'status',
                    ]));

                    Event::add(Auth::id(), Event::OBJECT_PROD_PLAN, 'Создано задание в систему', '', $newMeltingTask->id);


                    $batchs = Batch::whereMeltingTaskId($task->id)->get();

                    if (!empty($batchs)) {
                        $batchs->each(function ($batch) use ($newMeltingTask) {
                            $newBatch = Batch::create([
                                'melting_task_id' => $newMeltingTask->id,
                                'charge_id' => $batch->charge_id,
                                'clog' => $batch->clog,
                                'passport' => $batch->passport,
                            ]);

                            $loads = ChargeBatch::whereBatchId($batch->id)->get();

                            if (!empty($loads)) {
                                $loads->each(function ($load) use ($newBatch) {
                                    ChargeBatch::create([
                                        'batch_id' => $newBatch->id,
                                        'number' => $load->number,
                                        'weight' => $load->weight,
                                    ]);
                                });
                            }
                        });
                    }

                    $charges = MeltingTaskCharge::whereMeltingTaskId($task->id)->get();

                    if (!empty($charges)) {
                        $charges->each(function ($charge) use ($newMeltingTask) {
                            MeltingTaskCharge::create([
                                'melting_task_id' => $newMeltingTask->id,
                                'charge_id' => $charge->charge_id,
                                'weight' => $charge->weight,
                            ]);
                        });
                    }
                });
            }
        }

        return true;
    }

    public
    function massDelete(Request $request)
    {
        $ids = $request->get('ids');

        if (!empty($ids)) {
            $meltingTaskIds = [];
            foreach ($ids as $key => $id) {
                if ($id) {
                    $meltingTaskIds[] = $key;
                }
            }

            $meltingTasks = MeltingTask::whereIn('id', $meltingTaskIds)->get();

            $meltingTasks->each(function ($task) {
                $task->delete();
            });
        }

        return true;
    }

    public
    function index(Request $request)
    {
        $perPage = $request->get('per_page') ?? 20;
        $query = MeltingTask::query()
            ->with('chemical_composition');


        if (!empty($request->get('date'))) {
            $startDay = WorkDayService::getStartDay(Carbon::parse($request->get('date'))->startOfDay()->addHours(9));
            $endDay = WorkDayService::getEndDay(Carbon::parse($request->get('date'))->startOfDay()->addHours(9));
            $query->where('melt_down_to', '>=', $startDay)
                ->where('melt_down_to', '<', $endDay);
        }

        if (!empty($request->get('furnace_id'))) {
            $query->whereFurnaceId($request->get('furnace_id'));
        }

        if (!empty($request->get('status'))) {
            $query->whereStatus($request->get('status'));
        }

        if (!empty($request->get('product_type_id'))) {
            $query->whereProductTypeId($request->get('product_type_id'));
        }

        if (!empty($request->get('alloy_grade_id'))) {
            $alloyGradeId = $request->get('alloy_grade_id');

            $query->whereHas('chemical_composition', function ($q) use ($alloyGradeId) {
                $q->whereAlloyGradeId($alloyGradeId);
            });
        }

        if (!empty($request->get('order_by')) && $request->get('order_by') != 'chemical_composition.alloy_grade_id') {
            $query->orderBy($request->get('order_by'));
        }


        if (!empty($request->get('order_by')) && $request->get('order_by') == 'chemical_composition.alloy_grade_id') {
            $query->orderBy($request->get('order_by'));
        }

        $plannedMeltingTasks = $query->paginate($perPage);


        return MeltingTaskResource::collection($plannedMeltingTasks);
    }

    public
    function filters()
    {
        $statuses = [];

        foreach (MeltingTask::STATUSES_FOR_INTERFACE as $key => $status) {
            $statuses[] = [
                'id' => $key,
                'name' => $status,
            ];
        }

        return [
            'statuses' => $statuses,
            'furnaces' => FurnaceResource::collection(Furnace::active()->get()),
            'product_types' => ProductTypeResource::collection(ProductType::active()->get()),
            'alloy_grades' => AlloyGradeResource::collection(AlloyGrade::active()->get()),
        ];
    }

    public
    function massDate(Request $request)
    {
        $ids = $request->get('ids');
        $date = $request->get('date');

        if (!empty($ids)) {
            $meltingTaskIds = [];
            foreach ($ids as $key => $id) {
                if ($id) {
                    $meltingTaskIds[] = $key;
                }
            }

            $meltingTasks = MeltingTask::whereIn('id', $meltingTaskIds)->update([
                'planned_melting_date' => Carbon::parse($date),
            ]);
        }

        return true;
    }

    public
    function read($id, Request $request)
    {
        MeltingTaskService::calculateWeightAndPercent($id);

        $meltingTask = MeltingTask::findOrFail($id);

        if (!$meltingTask->card_created && $request->get('create_card')) {
            $activeMaterials = Material::query()->active()->get();

            $meltingTask->materials()->sync($activeMaterials->pluck('id')->all());


            $activeAdditionalWorks = AdditionalWork::query()->active()->get();

            if (!empty($activeAdditionalWorks)) {
                $activeAdditionalWorks->each(function ($work) use ($meltingTask) {
                    if (!empty($work->materials)) {
                        $materials = json_decode($work->materials, true);
                        if (!empty($materials)) {
                            foreach ($materials as $material) {
                                CostAdditionalMaterial::create([
                                    'additional_work_id' => $work->id,
                                    'melting_task_id' => $meltingTask->id,
                                    'name' => $material['name'],
                                    'measure' => $material['measure'],
                                ]);
                            }
                        }
                    }
                });
            }

            $activeEnergyParameters = EnergyParameter::query()->active()->get();

            if (!empty($activeEnergyParameters)) {
                $activeEnergyParameters->each(function ($parameter) use ($meltingTask) {
                    if (!empty($parameter->materials)) {
                        $materials = json_decode($parameter->materials, true);
                        if (!empty($materials)) {
                            foreach ($materials as $material) {
                                CostEnergyParameter::create([
                                    'energy_parameter_id' => $parameter->id,
                                    'melting_task_id' => $meltingTask->id,
                                    'name' => $material['name'],
                                    'measure' => $material['measure'],
                                ]);
                            }
                        }
                    }
                });

                $meltingTask->update([
                    'output_alloy_grade_id' => $meltingTask->chemical_composition->alloy_grade_id,
                ]);
            }

            $meltingTask->update([
                'card_created' => true,
                'number' => $meltingTask->generateNumber(),
            ]);

            Event::add(Auth::id(), Event::OBJECT_PROD_PLAN, 'Создана карта плавки', '', $meltingTask->number);
        }

        $meltingTask = MeltingTask::findOrFail($id);

        return new MeltingTaskResource($meltingTask);

    }

    public
    function options(): array
    {
        return [
            'not_marking_id' => env('NOT_MARKING_ID', 1),
        ];
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function downloadXls(int $meltingCardId)
    {
        return ExportService::exportMeltingCard($meltingCardId);
    }

    public function downloadProdPlan()
    {
        return ExportService::exportProdPlan();
    }
}
