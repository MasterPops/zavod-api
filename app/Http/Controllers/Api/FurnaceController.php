<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Furnace;
use Illuminate\Http\Request;

class FurnaceController extends Controller
{
    public function search(Request $request)
    {
        $value = $request->get('value') ?? '';

        return Furnace::active()
            ->where('name', 'like', "%$value%")
            ->paginate($request->get('per_page'));
    }
}
