<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Batch;
use App\Models\ChargeBatch;
use App\Models\Event;
use App\Models\MeltingTask;
use App\Services\MeltingTaskService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BatchController extends Controller
{
    public function delete(Request $request)
    {
        $batch = Batch::with('charge')->find($request->id);

        $chargeName = optional($batch->charge)->name;
        $clog = (int)$batch->clog;

        Event::add(Auth::id(), Event::OBJECT_MELT_CARD . ' (' . $batch->melting_task->number . ')',
            "Удалена шихта $chargeName, Процент засора: $clog%",
            '', '');

        $batch->delete();

        return 1;
    }

    public function update(Request $request)
    {
        $batch = Batch::with('charge')->findOrFail($request->id);

        $chargeName = optional($batch->charge)->name;
        $clog = (int)$batch->clog;

        $data = $request->only([
            'charge_id',
            'clog',
        ]);

        if (isset($data['clog']) && $batch->clog != $data['clog']) {
            Event::add(Auth::id(), Event::OBJECT_MELT_CARD . ' (' . $batch->melting_task->number . ')',
                "Изменен процент засора шихты",
                (int)$batch->clog, $data['clog']);
        }

        $loads = $request->get('loads');

        if (!empty($loads)) {
            $issetLoadIds = [];
            foreach ($loads as $load) {
                $load['batch_id'] = $request->id;

                $load['weight'] = (double)$load['weight'];
                if (isset($load['id'])) {
                    $issetLoadIds[] = $load['id'];
                    $loadUpdate = ChargeBatch::find($load['id']);

                    $oldWeight = $loadUpdate->weight;

                    $number = $load['number'];

                    if ($oldWeight != $load['weight']) {
                        Event::add(Auth::id(), Event::OBJECT_MELT_CARD . ' (' . $batch->melting_task->number . ')',
                            "Добавлена шихта $chargeName, Процент засора: $clog%, Номер загрузки:$number",
                            $oldWeight, $load['weight']);
                    }


                    $loadUpdate->update($load);

                } else {
                    $load['number'] = MeltingTaskService::getNewLoadNumber($batch->melting_task_id);
                    $newLoad = ChargeBatch::create($load);
                    $issetLoadIds[] = $newLoad->id;
                }
            }

            if (!empty($issetLoadIds)) {
                ChargeBatch::whereBatchId($request->id)->whereNotIn('id', $issetLoadIds)->delete();
            }
        }

        return $batch->update($data);
    }

    public function duplicate(Request $request)
    {
        $batch = Batch::findOrFail($request->id);

        $newBatchData = $batch->only([
            'melting_task_id',
            'clog',
            'charge_id',
        ]);

        $newBatch = Batch::create($newBatchData);

        $loads = ChargeBatch::whereBatchId($request->id)->get();

        if (!empty($loads)) {
            $loads->each(function ($load) use ($newBatch) {
                $number = MeltingTaskService::getNewLoadNumber($newBatch->melting_task_id);

                ChargeBatch::create([
                    'batch_id' => $newBatch->id,
                    'number' => $number,
                    'weight' => $load->weight,
                    'passport' => $load->passport,
                ]);

                $chargeName = $newBatch->charge->name;
                $clog = $newBatch->clog;

                Event::add(Auth::id(), Event::OBJECT_MELT_CARD . ' (' . $newBatch->melting_task->number . ')',
                    "Добавлена шихта $chargeName, Процент засора: $clog%, Номер загрузки:$number",
                    '0', $load->weight);
            });
        }

        return true;
    }

    public function create(Request $request)
    {
        return Batch::create([
            'melting_task_id' => $request->id
        ]);
    }
}
