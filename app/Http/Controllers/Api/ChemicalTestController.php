<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ChemicalTestCollectionResource;
use App\Models\AlloyGrade;
use App\Models\ChemicalTest;
use App\Models\Event;
use App\Models\MeltingTask;
use App\Models\TestType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChemicalTestController extends Controller
{
    public function index()
    {
        $tests = ChemicalTest::where('created_at', '>=', now()->startOfDay())
            ->where('created_at', '<=', now()->endOfDay())
            ->get()
            ->groupBy('melting_task_id');

        return new ChemicalTestCollectionResource($tests);
    }

    public function create(Request $request)
    {
        $data = $request->get('test');

        $meltingTask = MeltingTask::find($data['melting_task_id'] ?? null);

        $testType = TestType::find($data['test_type_id'] ?? null);

        $alloyGrade = AlloyGrade::find($data['alloy_grade_id'] ?? null);

        $after = 'al: ' . ($data['al'] ?? '-') . ', si: ' . ($data['si'] ?? '-') . ', fe: ' . ($data['fe'] ?? '-')
            . ', cu: ' . ($data['cu'] ?? '-') . ', mn: ' . ($data['mn'] ?? '-') . ', mg: ' . ($data['mg'] ?? '-')
            . ', zn: ' . ($data['zn'] ?? '-') . ', ti: ' . ($data['ti'] ?? '-') . ', ni: ' . ($data['ni'] ?? '-')
            . ', pb: ' . ($data['pb'] ?? '-') . ', sn: ' . ($data['sn'] ?? '-') . ', cr: ' . ($data['cr'] ?? '-')
            . ', cd: ' . ($data['si'] ?? '-') . ', cd: ' . ($data['si'] ?? '-') . ', Mg\Si: ' . ($data['proportions'] ?? '-')
            . ', примечание: ' . ($data['description'] ?? '-') . ', марка сплава: ' . ($alloyGrade->name ?? '-') . ', тип теста: ' . ($testType->name ?? '-');

        ChemicalTest::create($data);

        $meltingTask->update([
            'porosity' => $request->get('porosity'),
        ]);

        Event::add(Auth::id(), Event::OBJECT_CHEMICAL . ' (' . $meltingTask->number . ')',
            "Добавлена проба хим. состава",
            '', $after);
    }
}
