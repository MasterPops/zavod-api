<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\DailyReportResource;
use App\Models\DailyReport;
use App\Models\Event;
use App\Services\ExportService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DailyReportController extends Controller
{
    public function exportDailyReport($id)
    {
        $dailyReport = DailyReport::findOrFail($id);

        return ExportService::exportDailyReport($dailyReport);
    }
    public function read(Request $request)
    {
        if (isset($request->date)) {
            $date = Carbon::parse($request->get('date'));
        } else {
            $date = now();
        }

        $dailyReport = DailyReport::where('date', '>=', Carbon::parse($date)->startOfDay())
            ->where('date', '<=', Carbon::parse($date)->endOfDay())
            ->first();

        if (empty($dailyReport)) {
            $dailyReport = DailyReport::create([
                'date' => $date
            ]);
        }

        return DailyReportResource::make($dailyReport);
    }

    public function update(int $id, Request $request)
    {
        $data = $request->only([
            'rnp_plan',
            'rnp_fact',
            'm15_plan',
            'm15_fact',
            'kit_plan',
            'kit_fact',
            'fyks_weght',
            'rnp_weght',
            'china_weght',
            'weight_weght',
            'gomo_1_1_plane',
            'gomo_1_1_output',
            'gomo_1_1_shelf',
            'gomo_1_2_plane',
            'gomo_1_2_output',
            'gomo_1_2_shelf',
            'gomo_2_1_plane',
            'gomo_2_1_output',
            'gomo_2_1_shelf',
            'gomo_2_2_plane',
            'gomo_2_2_output',
            'gomo_2_2_shelf',
            'consumptions_pillars_mg',
            'consumptions_pillars_si',
            'consumptions_pillars_cu',
            'consumptions_ingots_mg',
            'consumptions_ingots_si',
            'consumptions_ingots_cu',
        ]);


        $dailyReport = DailyReport::find($id);

        if (isset($data['consumptions_ingots_si']) && $data['consumptions_ingots_si'] != $dailyReport->consumptions_ingots_si) {
            Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $dailyReport->date->format('d.m.Y') . ')',
                "Изменено значение Расход (Чушка, Si)",
                $dailyReport->consumptions_ingots_si, $data['consumptions_ingots_si']);
        }

        if (isset($data['consumptions_ingots_cu']) && $data['consumptions_ingots_cu'] != $dailyReport->consumptions_ingots_cu) {
            Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $dailyReport->date->format('d.m.Y') . ')',
                "Изменено значение Расход (Чушка, Cu)",
                $dailyReport->consumptions_ingots_cu, $data['consumptions_ingots_cu']);
        }

        if (isset($data['consumptions_ingots_mg']) && $data['consumptions_ingots_mg'] != $dailyReport->consumptions_ingots_mg) {
            Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $dailyReport->date->format('d.m.Y') . ')',
                "Изменено значение Расход (Чушка, Mg90)",
                $dailyReport->consumptions_ingots_mg, $data['consumptions_ingots_mg']);
        }

        if (isset($data['consumptions_pillars_si']) && $data['consumptions_pillars_si'] != $dailyReport->consumptions_pillars_si) {
            Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $dailyReport->date->format('d.m.Y') . ')',
                "Изменено значение Расход (столбы, Si)",
                $dailyReport->consumptions_pillars_si, $data['consumptions_pillars_si']);
        }

        if (isset($data['consumptions_pillars_cu']) && $data['consumptions_pillars_cu'] != $dailyReport->consumptions_pillars_cu) {
            Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $dailyReport->date->format('d.m.Y') . ')',
                "Изменено значение Расход (столбы, Cu)",
                $dailyReport->consumptions_pillars_cu, $data['consumptions_pillars_cu']);
        }

        if (isset($data['consumptions_pillars_mg']) && $data['consumptions_pillars_mg'] != $dailyReport->consumptions_pillars_mg) {
            Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $dailyReport->date->format('d.m.Y') . ')',
                "Изменено значение Расход (столбы, Mg90)",
                $dailyReport->consumptions_pillars_mg, $data['consumptions_pillars_mg']);
        }

        if (isset($data['rnp_plan']) && $data['rnp_plan'] != $dailyReport->rnp_plan) {
            Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $dailyReport->date->format('d.m.Y') . ')',
                "Изменено значение РНП всего (План)",
                $dailyReport->rnp_plan, $data['rnp_plan']);
        }

        if (isset($data['rnp_fact']) && $data['rnp_fact'] != $dailyReport->rnp_fact) {
            Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $dailyReport->date->format('d.m.Y') . ')',
                "Изменено значение РНП всего (Факт)",
                $dailyReport->rnp_fact, $data['rnp_fact']);
        }

        if (isset($data['m15_plan']) && $data['m15_plan'] != $dailyReport->m15_plan) {
            Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $dailyReport->date->format('d.m.Y') . ')',
                "Изменено значение М15 всего (План)",
                $dailyReport->m15_plan, $data['m15_plan']);
        }

        if (isset($data['m15_fact']) && $data['m15_fact'] != $dailyReport->m15_fact) {
            Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $dailyReport->date->format('d.m.Y') . ')',
                "Изменено значение М15 всего (Факт)",
                $dailyReport->m15_fact, $data['m15_fact']);
        }

        if (isset($data['kit_plan']) && $data['kit_plan'] != $dailyReport->kit_plan) {
            Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $dailyReport->date->format('d.m.Y') . ')',
                "Изменено значение КИТ всего (План)",
                $dailyReport->kit_plan, $data['kit_plan']);
        }

        if (isset($data['kit_fact']) && $data['kit_fact'] != $dailyReport->kit_fact) {
            Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $dailyReport->date->format('d.m.Y') . ')',
                "Изменено значение КИТ всего (Факт)",
                $dailyReport->kit_fact, $data['kit_fact']);
        }

        if (isset($data['kit_fact']) && $data['kit_fact'] != $dailyReport->kit_fact) {
            Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $dailyReport->date->format('d.m.Y') . ')',
                "Изменено значение КИТ всего (Факт)",
                $dailyReport->kit_fact, $data['kit_fact']);
        }

        if (isset($data['fyks_weght']) && $data['fyks_weght'] != $dailyReport->fyks_weght) {
            Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $dailyReport->date->format('d.m.Y') . ')',
                "Изменено значение Весы кг. (Фукс)",
                $dailyReport->fyks_weght, $data['fyks_weght']);
        }

        if (isset($data['rnp_weght']) && $data['rnp_weght'] != $dailyReport->rnp_weght) {
            Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $dailyReport->date->format('d.m.Y') . ')',
                "Изменено значение Весы кг. (РНП)",
                $dailyReport->rnp_weght, $data['rnp_weght']);
        }

        if (isset($data['weight_weght']) && $data['weight_weght'] != $dailyReport->weight_weght) {
            Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $dailyReport->date->format('d.m.Y') . ')',
                "Изменено значение Весы кг. (Весовая)",
                $dailyReport->weight_weght, $data['weight_weght']);
        }

        if (isset($data['gomo_1_1_shelf']) && $data['gomo_1_1_shelf'] != $dailyReport->gomo_1_1_shelf) {
            Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $dailyReport->date->format('d.m.Y') . ')',
                "Изменено значение Время выхода на полку Гомо (Гомо 1.1 Факт. Полка)",
                $dailyReport->gomo_1_1_shelf, $data['gomo_1_1_shelf']);
        }

        if (isset($data['gomo_1_2_shelf']) && $data['gomo_1_2_shelf'] != $dailyReport->gomo_1_2_shelf) {
            Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $dailyReport->date->format('d.m.Y') . ')',
                "Изменено значение Время выхода на полку Гомо (Гомо 1.2 Факт. Полка)",
                $dailyReport->gomo_1_2_shelf, $data['gomo_1_2_shelf']);
        }

        if (isset($data['gomo_2_1_shelf']) && $data['gomo_2_1_shelf'] != $dailyReport->gomo_2_1_shelf) {
            Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $dailyReport->date->format('d.m.Y') . ')',
                "Изменено значение Время выхода на полку Гомо (Гомо 2.1 Факт. Полка)",
                $dailyReport->gomo_2_1_shelf, $data['gomo_2_1_shelf']);
        }

        if (isset($data['gomo_2_2_shelf']) && $data['gomo_2_2_shelf'] != $dailyReport->gomo_2_2_shelf) {
            Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $dailyReport->date->format('d.m.Y') . ')',
                "Изменено значение Время выхода на полку Гомо (Гомо 2.2 Факт. Полка)",
                $dailyReport->gomo_2_2_shelf, $data['gomo_2_2_shelf']);
        }

        if (isset($data['gomo_1_1_output']) && $data['gomo_1_1_output'] != $dailyReport->gomo_1_1_output) {
            Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $dailyReport->date->format('d.m.Y') . ')',
                "Изменено значение Время выхода на полку Гомо (Гомо 1.1 Факт. Выход на полку)",
                $dailyReport->gomo_1_1_output, $data['gomo_1_1_output']);
        }

        if (isset($data['gomo_1_2_output']) && $data['gomo_1_2_output'] != $dailyReport->gomo_1_2_output) {
            Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $dailyReport->date->format('d.m.Y') . ')',
                "Изменено значение Время выхода на полку Гомо (Гомо 1.2 Факт. Выход на полку)",
                $dailyReport->gomo_1_2_output, $data['gomo_1_2_output']);
        }

        if (isset($data['gomo_2_1_output']) && $data['gomo_2_1_output'] != $dailyReport->gomo_2_1_output) {
            Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $dailyReport->date->format('d.m.Y') . ')',
                "Изменено значение Время выхода на полку Гомо (Гомо 2.1 Факт. Выход на полку)",
                $dailyReport->gomo_2_1_output, $data['gomo_2_1_output']);
        }

        if (isset($data['gomo_2_2_output']) && $data['gomo_2_2_output'] != $dailyReport->gomo_2_2_output) {
            Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $dailyReport->date->format('d.m.Y') . ')',
                "Изменено значение Время выхода на полку Гомо (Гомо 2.2 Факт. Выход на полку)",
                $dailyReport->gomo_2_2_output, $data['gomo_2_2_output']);
        }

        if (isset($data['gomo_1_1_plane']) && $data['gomo_1_1_plane'] != $dailyReport->gomo_1_1_plane) {
            Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $dailyReport->date->format('d.m.Y') . ')',
                "Изменено значение Время выхода на полку Гомо (Гомо 1.1 план)",
                $dailyReport->gomo_1_1_plane, $data['gomo_1_1_plane']);
        }

        if (isset($data['gomo_1_2_plane']) && $data['gomo_1_2_plane'] != $dailyReport->gomo_1_2_plane) {
            Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $dailyReport->date->format('d.m.Y') . ')',
                "Изменено значение Время выхода на полку Гомо (Гомо 1.2 план)",
                $dailyReport->gomo_1_2_plane, $data['gomo_1_2_plane']);
        }

        if (isset($data['gomo_2_1_plane']) && $data['gomo_2_1_plane'] != $dailyReport->gomo_2_1_plane) {
            Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $dailyReport->date->format('d.m.Y') . ')',
                "Изменено значение Время выхода на полку Гомо (Гомо 2.1 план)",
                $dailyReport->gomo_2_1_plane, $data['gomo_2_1_plane']);
        }

        if (isset($data['gomo_2_2_plane']) && $data['gomo_2_2_plane'] != $dailyReport->gomo_2_2_plane) {
            Event::add(Auth::id(), Event::OBJECT_DAILY_REPORT . ' (' . $dailyReport->date->format('d.m.Y') . ')',
                "Изменено значение Время выхода на полку Гомо (Гомо 2.2 план)",
                $dailyReport->gomo_2_2_plane, $data['gomo_2_2_plane']);
        }

        $dailyReport->update($data);
        $dailyReport->fresh();

        return DailyReportResource::make($dailyReport);
    }
}
