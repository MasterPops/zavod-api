<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\Diameter;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $value = $request->get('value') ?? '';

        $query = User::active()
            ->where(function ($q) use ($value) {
                $q->where('first_name', 'LIKE', "%$value%")
                    ->orWhere('last_name', 'LIKE', "%$value%")
                    ->orWhere('second_name', 'LIKE', "%$value%");
            });

        if (!empty($request->get('role'))) {
            $query->whereHas('roles', function ($q) use ($request) {
                $q->whereName($request->get('role'));
            });
        }
        return UserResource::collection($query->paginate($request->get('per_page')));
    }
    public function roles()
    {
        $roles = [];

        $roles[] = [
            'id' => User::SMELTER_ROLE,
            'name' => User::ROLES_FOR_FRONT[User::SMELTER_ROLE],
        ];

        $roles[] = [
            'id' => User::DRIVER_ROLE,
            'name' => User::ROLES_FOR_FRONT[User::DRIVER_ROLE],
        ];

        return ['data' => $roles];
    }

    public function profile()
    {
        return UserResource::make(Auth::user());
    }
}
