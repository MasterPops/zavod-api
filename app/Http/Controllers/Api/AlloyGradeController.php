<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\AlloyGradeResource;
use App\Models\AlloyGrade;
use App\Models\Diameter;
use Illuminate\Http\Request;

class AlloyGradeController extends Controller
{
    public function read(int $id)
    {
        $alloyGrade = AlloyGrade::find($id);

        return AlloyGradeResource::make($alloyGrade);
    }

    public function index(Request $request)
    {
        return AlloyGradeResource::collection(AlloyGrade::active()->orderBy('name')->get());
    }

    public function search(Request $request)
    {
        $value = $request->get('value') ?? '';

        return AlloyGradeResource::collection(AlloyGrade::active()
            ->where('name', 'like', "%$value%")
            ->paginate($request->get('per_page')));
    }
}
