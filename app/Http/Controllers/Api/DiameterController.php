<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Diameter;
use Illuminate\Http\Request;

class DiameterController extends Controller
{
    public function search(Request $request)
    {
        $value = $request->get('value') ?? '';

        return Diameter::active()
            ->where('name', 'like', "%$value%")
            ->paginate($request->get('per_page'));
    }
}
