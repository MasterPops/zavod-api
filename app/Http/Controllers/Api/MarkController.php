<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Mark;
use Illuminate\Http\Request;

class MarkController extends Controller
{
    public function search(Request $request)
    {
        $value = $request->get('value') ?? '';

        return Mark::active()
            ->where('name', 'like', "%$value%")
            ->paginate($request->get('per_page'));
    }
}
