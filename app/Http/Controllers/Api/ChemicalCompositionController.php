<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ChemicalCompositionResource;
use App\Models\ChemicalComposition;
use Illuminate\Http\Request;

class ChemicalCompositionController extends Controller
{
    public function search(Request $request)
    {
        $value = $request->get('value') ?? '';



        $query =  ChemicalComposition::where(function ($q) use ($value) {
            $q->whereHas('alloy_grade', function ($q) use ($value) {
                $q->where('name', 'like', "%$value%");
            })->orWhere('description', 'like', "%$value%");
        });

        if (!empty($request->get('alloy_grade_id'))) {
            $query->whereAlloyGradeId($request->get('alloy_grade_id'));
        }

        $result = $query->paginate($request->get('per_page'));

        return ChemicalCompositionResource::collection($result);
    }
}
