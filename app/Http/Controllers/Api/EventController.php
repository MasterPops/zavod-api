<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\EventResource;
use App\Models\Event;
use App\Models\Furnace;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function index(Request $request)
    {

        $query = Event::query()->orderByDesc('created_at');

        if (!empty($request->get('date'))) {
            $query->where('created_at', '>=', Carbon::parse($request->get('date'))->startOfDay())
                ->where('created_at', '<=', Carbon::parse($request->get('date'))->endOfDay());
        }

        $events = $query->paginate(30);

        return EventResource::collection($events);

    }
}
