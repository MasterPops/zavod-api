<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\TechnicalCondition;
use Illuminate\Http\Request;

class TechnicalConditionController extends Controller
{
    public function search(Request $request)
    {
        $value = $request->get('value') ?? '';

        return TechnicalCondition::active()
            ->where('name', 'like', "%$value%")
            ->paginate($request->get('per_page'));
    }
}
