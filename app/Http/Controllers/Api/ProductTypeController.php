<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ProductType;
use Illuminate\Http\Request;

class ProductTypeController extends Controller
{
    public function search(Request $request)
    {
        $value = $request->get('value') ?? '';

        return ProductType::active()
            ->where('name', 'like', "%$value%")
            ->paginate($request->get('per_page'));
    }
}
