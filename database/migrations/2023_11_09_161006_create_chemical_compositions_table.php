<?php

use App\Models\ChemicalComposition;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('chemical_compositions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('technical_condition_id');
            $table->unsignedBigInteger('alloy_grade_id');
            $table->unsignedBigInteger('diameter_id')->nullable();

            $table->foreign('technical_condition_id')
                ->references('id')
                ->on('technical_conditions')
                ->onDelete('cascade');

            $table->foreign('alloy_grade_id')
                ->references('id')
                ->on('alloy_grades')
                ->onDelete('cascade');

            $table->foreign('diameter_id')
                ->references('id')
                ->on('diameters')
                ->onDelete('cascade');


            foreach (ChemicalComposition::CHEMICAL_ELEMENTS as $element) {
                $table->string($element)->nullable();
            }

            $table->string('proportions')->nullable();
            $table->string('description')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('chemical_compositions');
    }
};
