<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('melting_tasks', function (Blueprint $table) {
            $table->unsignedBigInteger('diameter_id')->nullable()->after('furnace_id');

            $table->foreign('diameter_id')
                ->references('id')
                ->on('diameters')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('melting_tasks', function (Blueprint $table) {
            //
        });
    }
};
