<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('melting_task_charge', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('melting_task_id');
            $table->unsignedBigInteger('charge_id');
            $table->double('weight')->nullable();

            $table->foreign('melting_task_id')
                ->references('id')
                ->on('melting_tasks')
                ->onDelete('cascade');

            $table->foreign('charge_id')
                ->references('id')
                ->on('charges')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('melting_task_charge');
    }
};
