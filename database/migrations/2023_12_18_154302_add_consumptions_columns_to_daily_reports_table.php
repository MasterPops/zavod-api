<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('daily_reports', function (Blueprint $table) {
            $table->double('consumptions_pillars_mg')->nullable();
            $table->double('consumptions_pillars_si')->nullable();
            $table->double('consumptions_pillars_cu')->nullable();
            $table->double('consumptions_ingots_mg')->nullable();
            $table->double('consumptions_ingots_si')->nullable();
            $table->double('consumptions_ingots_cu')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('daily_reports', function (Blueprint $table) {
            //
        });
    }
};
