<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('melting_tasks', function (Blueprint $table) {
            $table->double('output_alloy_grade_percent')
                ->nullable()
                ->after('output_alloy_grade_id');

            $table->double('output_trim_weight')->nullable();
            $table->double('output_trim_percent')->nullable();

            $table->double('output_reject1_weight')->nullable();
            $table->double('output_reject1_percent')->nullable();

            $table->double('output_cut_count')->nullable();

            $table->double('output_cut_weight')->nullable();
            $table->double('output_cut_percent')->nullable();

            $table->double('output_reject2_weight')->nullable();
            $table->double('output_reject2_percent')->nullable();

            $table->double('output_shavings_weight')->nullable();
            $table->double('output_shavings_percent')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('melting_tasks', function (Blueprint $table) {
            //
        });
    }
};
