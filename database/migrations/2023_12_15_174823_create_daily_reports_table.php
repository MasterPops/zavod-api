<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('daily_reports', function (Blueprint $table) {
            $table->id();
            $table->double('rnp_plan')->nullable();
            $table->double('rnp_fact')->nullable();
            $table->double('m15_plan')->nullable();
            $table->double('m15_fact')->nullable();
            $table->double('kit_plan')->nullable();
            $table->double('kit_fact')->nullable();
            $table->double('fyks_weght')->nullable();
            $table->double('rnp_weght')->nullable();
            $table->double('china_weght')->nullable();
            $table->double('weight_weght')->nullable();
            $table->double('gomo_1_1_plane')->nullable();
            $table->double('gomo_1_1_output')->nullable();
            $table->double('gomo_1_1_shelf')->nullable();
            $table->double('gomo_1_2_plane')->nullable();
            $table->double('gomo_1_2_output')->nullable();
            $table->double('gomo_1_2_shelf')->nullable();
            $table->double('gomo_2_1_plane')->nullable();
            $table->double('gomo_2_1_output')->nullable();
            $table->double('gomo_2_1_shelf')->nullable();
            $table->double('gomo_2_2_plane')->nullable();
            $table->double('gomo_2_2_output')->nullable();
            $table->double('gomo_2_2_shelf')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('daily_reports');
    }
};
