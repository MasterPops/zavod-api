<?php

use App\Models\ChemicalComposition;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('chemical_tests', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('melting_task_id');

            $table->foreign('melting_task_id')
                ->references('id')
                ->on('melting_tasks')
                ->onDelete('cascade');

            $table->unsignedBigInteger('test_type_id')->nullable();

            $table->foreign('test_type_id')
                ->references('id')
                ->on('test_types')
                ->onDelete('cascade');

            $table->unsignedBigInteger('alloy_grade_id')->nullable();

            $table->foreign('alloy_grade_id')
                ->references('id')
                ->on('alloy_grades')
                ->onDelete('cascade');

            foreach (ChemicalComposition::CHEMICAL_ELEMENTS as $element) {
                $table->string($element)->nullable();
            }

            $table->string('proportions')->nullable();
            $table->string('description')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('chemical_tests');
    }
};
