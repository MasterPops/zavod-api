<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('melting_task_users', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('melting_task_id');

            $table->foreign('melting_task_id')
                ->references('id')
                ->on('melting_tasks')
                ->onDelete('cascade');

            $table->unsignedBigInteger('user_id')->nullable();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->string('role');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('melting_task_users');
    }
};
