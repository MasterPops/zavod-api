<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('melting_tasks', function (Blueprint $table) {
            $table->id();
            $table->string('number');
            $table->smallInteger('daily_melting_number')->nullable();
            $table->unsignedBigInteger('furnace_id');
            $table->dateTime('planned_melting_date')->nullable();
            $table->unsignedBigInteger('product_type_id');
            $table->unsignedBigInteger('chemical_composition_id');
            $table->unsignedBigInteger('mark_id')->nullable();
            $table->boolean('filter')->default(false);
            $table->double('weight');
            $table->enum('status', \App\Models\MeltingTask::STATUSES)
                ->default(\App\Models\MeltingTask::PLANNING_STATUS);

            $table->timestamps();

            $table->foreign('furnace_id')
                ->references('id')
                ->on('furnaces')
                ->onDelete('cascade');

            $table->foreign('product_type_id')
                ->references('id')
                ->on('product_types')
                ->onDelete('cascade');

            $table->foreign('chemical_composition_id')
                ->references('id')
                ->on('chemical_compositions')
                ->onDelete('cascade');

            $table->foreign('mark_id')
                ->references('id')
                ->on('marks')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('melting_tasks');
    }
};
