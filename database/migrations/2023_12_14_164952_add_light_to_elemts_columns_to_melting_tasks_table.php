<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('melting_tasks', function (Blueprint $table) {
            $columns = [
                'al_l',
                'si_l',
                'fe_l',
                'cu_l',
                'mn_l',
                'mg_l',
                'zn_l',
                'ti_l',
                'ni_l',
                'pb_l',
                'sn_l',
                'cr_l',
                'cd_l',
                'proportions_l',
            ];

            foreach ($columns as $column) {
                $table->boolean($column)->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('melting_tasks', function (Blueprint $table) {
            //
        });
    }
};
