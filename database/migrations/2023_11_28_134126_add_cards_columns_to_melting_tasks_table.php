<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('melting_tasks', function (Blueprint $table) {
            $table->double('output_weight')->nullable();
            $table->string('brand')->nullable();
            $table->string('series')->nullable();
            $table->unsignedBigInteger('output_alloy_grade_id')->nullable();

            $table->foreign('output_alloy_grade_id')
                ->references('id')
                ->on('alloy_grades')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('melting_tasks', function (Blueprint $table) {
            //
        });
    }
};
