<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('melting_tasks', function (Blueprint $table) {
            $table->double('output_black_weight')->nullable();
            $table->double('output_black_percent')->nullable();

            $table->double('output_white_weight')->nullable();
            $table->double('output_white_percent')->nullable();

            $table->double('output_trash_weight')->nullable();
            $table->double('output_trash_percent')->nullable();

            $table->double('output_irrevocably_weight')->nullable();
            $table->double('output_irrevocably_percent')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('melting_tasks', function (Blueprint $table) {
            //
        });
    }
};
