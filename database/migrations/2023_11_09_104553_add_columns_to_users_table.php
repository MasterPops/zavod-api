<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('active')->default(true)->after('name')->index();
            $table->string('second_name')->nullable()->after('name')->index();
            $table->string('last_name')->nullable()->after('name')->index();
            $table->string('first_name')->nullable()->after('name')->index();
            $table->dropColumn('name');

            $table->index(['first_name', 'last_name', 'second_name']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('name')->nullable()->after('first_name');
            $table->dropIndex(['first_name', 'last_name', 'second_name']);
            $table->dropColumn(['first_name', 'last_name', 'second_name', 'active']);;
        });
    }
};
