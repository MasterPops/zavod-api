<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        foreach (User::ROLES as $role) {
            Role::updateOrCreate([
                'name' => $role,
            ]);

        }

        foreach (User::PERMISSONS as $permission) {
            Permission::updateOrCreate([
                'name' => $permission,
            ]);

        }

        $roles = Role::all();

        $roles->each(function ($role) {
            switch ($role->name) {
                case User::ADMIN_ROLE:
                    $role->givePermissionTo(User::ADMIN_PERMISSIONS);
                    break;
                case User::DIRECTOR_ROLE:
                    $role->givePermissionTo(User::DIRECTOR_PERMISSIONS);
                    break;
                case User::MASTER_ROLE:
                    $role->givePermissionTo(User::MASTER_PERMISSIONS);
                    break;
                case User::TECHNOLOGIST_ROLE:
                    $role->givePermissionTo(User::TECHNOLOGIST_PERMISSIONS);
                    break;
                case User::LABORATORY_ROLE:
                    $role->givePermissionTo(User::LABORATORY_PERMISSIONS);
                    break;
                default:
                    break;
            }
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Permission::delete();
        Role::delete();
    }
};
