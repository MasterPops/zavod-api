<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cost_energy_parameters', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('melting_task_id');

            $table->foreign('melting_task_id')
                ->references('id')
                ->on('melting_tasks')
                ->onDelete('cascade');

            $table->unsignedBigInteger('energy_parameter_id');

            $table->foreign('energy_parameter_id')
                ->references('id')
                ->on('energy_parameters')
                ->onDelete('cascade');

            $table->string('name');
            $table->double('start_value')->nullable();
            $table->double('end_value')->nullable();
            $table->double('difference')->nullable();
            $table->string('measure');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cost_energy_parameters');
    }
};
