<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cost_additional_materials', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('melting_task_id');

            $table->foreign('melting_task_id')
                ->references('id')
                ->on('melting_tasks')
                ->onDelete('cascade');

            $table->unsignedBigInteger('additional_work_id');

            $table->foreign('additional_work_id')
                ->references('id')
                ->on('additional_works')
                ->onDelete('cascade');

            $table->string('name');
            $table->double('value')->nullable();
            $table->string('measure');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cost_additional_materials');
    }
};
