<?php

use App\Http\Controllers\Api\AlloyGradeController;
use App\Http\Controllers\Api\BatchController;
use App\Http\Controllers\Api\ChargeController;
use App\Http\Controllers\Api\ChemicalCompositionController;
use App\Http\Controllers\Api\ChemicalTestController;
use App\Http\Controllers\Api\DailyReportController;
use App\Http\Controllers\Api\DiameterController;
use App\Http\Controllers\Api\FurnaceController;
use App\Http\Controllers\Api\MarkController;
use App\Http\Controllers\Api\MeltingTaskController;
use App\Http\Controllers\Api\ProductTypeController;
use App\Http\Controllers\Api\TechnicalConditionController;
use App\Http\Controllers\Api\TestTypeController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\LogoutController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', [UserController::class, 'profile']);

Route::post('/login', LoginController::class);
Route::group([
    'middleware' => 'auth:sanctum'
], function () {
    Route::post('/logout', LogoutController::class);
});

Route::group([
    'prefix' => '/v1',
//    'middleware' => 'auth:sanctum'
], function () {
    Route::group(['prefix' => '/product-type',], function () {
        Route::get('/', [ProductTypeController::class, 'search']);
    });

    Route::group(['prefix' => '/daily-report',], function () {
        Route::get('/', [DailyReportController::class, 'read']);
        Route::patch('/{id}', [DailyReportController::class, 'update']);
        Route::get('/{id}/xls', [DailyReportController::class, 'exportDailyReport']);
    });

    Route::group(['prefix' => '/test-type',], function () {
        Route::get('/', [TestTypeController::class, 'search']);
        Route::get('/{id}', [TestTypeController::class, 'read']);
    });

    Route::group(['prefix' => '/diameter',], function () {
        Route::get('/', [DiameterController::class, 'search']);
    });

    Route::group(['prefix' => '/technical-condition',], function () {
        Route::get('/', [TechnicalConditionController::class, 'search']);
    });

    Route::group(['prefix' => '/furnace',], function () {
        Route::get('/', [FurnaceController::class, 'search']);
    });

    Route::group(['prefix' => '/mark',], function () {
        Route::get('/', [MarkController::class, 'search']);
    });

    Route::group(['prefix' => '/chemical-composition',], function () {
        Route::get('/', [ChemicalCompositionController::class, 'search']);
    });

    Route::group(['prefix' => '/charge',], function () {
        Route::get('/', [ChargeController::class, 'search']);
        Route::get('/list', [ChargeController::class, 'index']);
    });

    Route::group(['prefix' => '/alloy_grade',], function () {
        Route::get('/', [AlloyGradeController::class, 'search']);
        Route::get('/{id}', [AlloyGradeController::class, 'c']);
    });

    Route::group(['prefix' => '/material',], function () {
        Route::get('/', [AlloyGradeController::class, 'index']);
    });

    Route::group(['prefix' => '/test',], function () {
        Route::get('/', [ChemicalTestController::class, 'index']);
        Route::post('/', [ChemicalTestController::class, 'create']);
    });

    Route::group(['prefix' => '/role',], function () {
        Route::get('/', [UserController::class, 'roles']);
    });

    Route::group(['prefix' => '/user',], function () {
        Route::get('/', [UserController::class, 'index']);
    });

    Route::group(['prefix' => '/events',], function () {
        Route::get('/', [\App\Http\Controllers\Api\EventController::class, 'index']);
    });

    Route::group(['prefix' => '/melting-task',], function () {
        Route::get('/options', [MeltingTaskController::class, 'options']);

        Route::group(['prefix' => '/batch',], function () {
            Route::patch('/{id}', [BatchController::class, 'update']);
            Route::delete('/{id}', [BatchController::class, 'delete']);
            Route::post('/{id}/duplicate', [BatchController::class, 'duplicate']);
        });

        Route::get('/', [MeltingTaskController::class, 'index']);
        Route::get('/number/{number}', [MeltingTaskController::class, 'getFromNumber']);
        Route::get('/number-search', [MeltingTaskController::class, 'numberSearch']);
        Route::post('/', [MeltingTaskController::class, 'save']);
        Route::get('/filters', [MeltingTaskController::class, 'filters']);
        Route::post('/mass-double', [MeltingTaskController::class, 'massDouble']);
        Route::post('/mass-delete', [MeltingTaskController::class, 'massDelete']);
        Route::post('/mass-date', [MeltingTaskController::class, 'massDate']);
        Route::get('/overdue', [MeltingTaskController::class, 'overdue']);
        Route::get('/working', [MeltingTaskController::class, 'working']);
        Route::get('/planned', [MeltingTaskController::class, 'planned']);
        Route::get('/without-date', [MeltingTaskController::class, 'withoutDate']);
        Route::get('/done', [MeltingTaskController::class, 'done']);
        Route::get('/melt-card/{id}/xls', [MeltingTaskController::class, 'downloadXls']);
        Route::get('/prodplan/xls', [MeltingTaskController::class, 'downloadProdPlan']);
        Route::get('/melt-card/{id}', [MeltingTaskController::class, 'read']);
        Route::get('/{id}', [MeltingTaskController::class, 'read']);
        Route::post('/{id}/add-batch', [BatchController::class, 'create']);
        Route::patch('/{id}', [MeltingTaskController::class, 'update']);


    });
});
