<?php

use Illuminate\Support\Facades\Route;

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.
Route::redirect('/admin/dashboard','user');

Route::redirect('/admin','admin/furnace');
Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes

    Route::crud('furnace', 'FurnaceCrudController');
    Route::crud('user', 'UserCrudController');
    Route::crud('alloy-grade', 'AlloyGradeCrudController');
    Route::crud('charge', 'ChargeCrudController');
    Route::crud('technical-condition', 'TechnicalConditionCrudController');
    Route::crud('diameter', 'DiameterCrudController');
    Route::crud('mark', 'MarkCrudController');
    Route::crud('product-type', 'ProductTypeCrudController');
    Route::crud('material', 'MaterialCrudController');
    Route::crud('additional-work', 'AdditionalWorkCrudController');
    Route::crud('energy-parameter', 'EnergyParameterCrudController');
    Route::crud('test-type', 'TestTypeCrudController');
}); // this should be the absolute last line of this file
