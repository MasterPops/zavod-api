<?php

return [
    'diameter' => 'Диаметр',
    'diameters' => 'Диаметры',
    'diameter_singular' => 'диаметр',
    'diameter_plural' => 'диаметры',
    'labels' => [
        'active' => 'Активность',
        'name' => 'Наименование',
    ],
];
