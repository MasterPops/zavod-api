<?php

return [
    'user' => 'Пользователь',
    'users' => 'Пользователи',
    'user_singular' => 'пользователя',
    'user_plural' => 'пользователи',
    'labels' => [
        'first_name' => 'Имя',
        'last_name' => 'Отчество',
        'second_name' => 'Фамилия',
        'email' => 'E-Mail',
        'active' => 'Активность',
        'password' => 'Пароль',
        'repeat_password' => 'Повторите пароль',
    ],
];
