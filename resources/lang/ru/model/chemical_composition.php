<?php

return [
    'labels' => [
        'technical_condition' => 'ГОСТ/ТУ',
        'dia' => 'Диаметр столба',
        'al' => 'Al',
        'si' => 'Si',
        'fe' => 'Fe',
        'cu' => 'Cu',
        'mn' => 'Mn',
        'mg' => 'Mg',
        'zn' => 'Zn',
        'ti' => 'Ti',
        'ni' => 'Ni',
        'pb' => 'Pb',
        'sn' => 'Sn',
        'cr' => 'Cr',
        'cd' => 'Cd',
        'proportions' => 'Соотношение',
        'description' => 'Примечание',
        'technical_condition_id' => 'ID ТУ/ГОСТа',
        'alloy_grade_id' => 'ID марки сплава',
        'id' => 'ID',
    ],
];
