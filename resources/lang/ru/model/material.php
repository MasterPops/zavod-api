<?php

return [
    'material' => 'Материал',
    'materials' => 'Материалы',
    'material_singular' => 'материал',
    'material_plural' => 'материалы',
    'labels' => [
        'name' => 'Наименование',
        'active' => 'Активность',
        'measure' => 'Единица измерения',
        'coef' => 'Коэффициент',
        'display_measure' => 'Отображаемая единица измерения',
    ],
];
