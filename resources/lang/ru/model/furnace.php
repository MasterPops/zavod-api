<?php

return [
    'furnace' => 'Печь',
    'furnaces' => 'Печи',
    'furnace_singular' => 'печь',
    'furnace_plural' => 'печи',
    'labels' => [
        'active' => 'Активность',
        'name' => 'Наименование',
    ],
];
