<?php

return [
    'charge' => 'Шихта',
    'charges' => 'Шихта',
    'charge_singular' => 'шихту',
    'charge_plural' => 'шихта',
    'labels' => [
        'id' => 'ID',
        'name' => 'Наименование',
        'active' => 'Активность',
    ],
];
