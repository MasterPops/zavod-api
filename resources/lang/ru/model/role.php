<?php

use App\Models\User;

return [
    'role' => 'Роль',
    'roles' => 'Роли',
    'role_singular' => 'роль',
    'role_plural' => 'роли',
    User::ADMIN_ROLE => 'Администратор',
    User::DIRECTOR_ROLE => 'Директор производства',
    User::MASTER_ROLE => 'Мастер',
    User::TECHNOLOGIST_ROLE => 'Технолог',
    User::LABORATORY_ROLE => 'Лаборатория',
    User::SMELTER_ROLE => 'Плавильщик',
    User::DRIVER_ROLE => 'Водитель погрузчика',
    'labels' => [
        'name' => 'Наименование',
    ],
];
