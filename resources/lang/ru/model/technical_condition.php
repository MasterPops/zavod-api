<?php


return [
    'technical_condition' => 'ТУ/ГОСТ',
    'technical_conditions' => 'ТУ/ГОСТы',
    'technical_condition_singular' => 'ТУ/ГОСТ',
    'technical_condition_plural' => 'ТУ/ГОСТы',
    'labels' => [
        'name' => 'Наименование',
        'active' => 'Активность',
        'chemical_compositions' => 'Химические составы',
        'add_chemical_composition' => 'Добавить хим. состав',
    ],
];
