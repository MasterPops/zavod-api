<?php

use App\Models\User;

return [
    'permission' => 'Право',
    'permissions' => 'Права',
    'permission_singular' => 'право',
    'permission_plural' => 'права',
    User::READ_PRODUCTION_PLANE_PERMISSION => 'Просмотр плана производства',
    User::WRITE_PRODUCTION_PLANE_PERMISSION => 'Редактирование плана производства',
    User::READ_MELTING_CARD_PERMISSION => 'Просмотр карты плавки',
    User::WRITE_MELTING_CARD_PERMISSION => 'Редактирование карты плавки',
    User::READ_DAILY_REPORT_PERMISSION => 'Просмотр суточного отчета',
    User::WRITE_DAILY_REPORT_PERMISSION => 'Редактирование суточного отчета',
    User::READ_CHEMICAL_COMPOSITION_PERMISSION => 'Просмотр химического состава плавки',
    User::WRITE_CHEMICAL_COMPOSITION_PERMISSION => 'Редактирование химического состава плавки',
    'labels' => [
        'name' => 'Наименование',
    ],
];
