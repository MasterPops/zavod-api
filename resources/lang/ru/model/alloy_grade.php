<?php

return [
    'alloy_grade' => 'Марка сплава',
    'alloy_grades' => 'Марки сплавов',
    'alloy_grade_singular' => 'марку сплава',
    'alloy_grade_plural' => 'марки сплавов',
    'labels' => [
        'active' => 'Активность',
        'name' => 'Наименование',
    ],
];
