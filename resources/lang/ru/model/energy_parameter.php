<?php

return [
    'energy_parameter' => 'Расход энергии',
    'energy_parameters' => 'Расходы энергии',
    'energy_parameter_singular' => 'расход энергии',
    'energy_parameter_plural' => 'расходы энергии',
    'labels' => [
        'active' => 'Активность',
        'name' => 'Наименование',
    ],
];
