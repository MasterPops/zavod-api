<?php

return [
    'test_type' => 'Тип теста',
    'test_types' => 'Типы тестов',
    'test_type_singular' => 'тип теста',
    'test_type_plural' => 'типы тестов',
    'labels' => [
        'active' => 'Активность',
        'name' => 'Наименование',
    ],
];
