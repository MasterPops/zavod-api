<?php

return [
    'additional_work' => 'Доп. работа',
    'additional_works' => 'Доп. работы',
    'additional_work_singular' => 'доп. работу',
    'additional_work_plural' => 'доп. работы',
    'labels' => [
        'active' => 'Активность',
        'name' => 'Наименование',
    ],
];
