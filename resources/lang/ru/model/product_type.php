<?php

return [
    'product_type' => 'Тип продукции',
    'product_types' => 'Типы продукции',
    'product_type_singular' => 'тип продукции',
    'product_type_plural' => 'типы продукции',
    'labels' => [
        'active' => 'Активность',
        'name' => 'Наименование',
        'code' => 'Код изделия',
        'increment_code' => 'Инкрементировать код изделия после выполнения 99 заданий',
    ],
];
