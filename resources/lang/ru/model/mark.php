<?php

return [
    'mark' => 'Маркировка',
    'marks' => 'Маркировки',
    'mark_singular' => 'маркировку',
    'mark_plural' => 'маркировки',
    'labels' => [
        'active' => 'Активность',
        'name' => 'Наименование',
    ],
];
