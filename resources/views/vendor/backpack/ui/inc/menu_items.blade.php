@if(backpack_user()->hasRole([
\App\Models\User::ADMIN_ROLE,
\App\Models\User::TECHNOLOGIST_ROLE,
\App\Models\User::DIRECTOR_ROLE,
\App\Models\User::MASTER_ROLE,
\App\Models\User::LABORATORY_ROLE,
]))
<x-backpack::menu-item title="{{trans('model/user.users')}}" icon="la la-user-tie"
                       :link="backpack_url('user')"/>
@endif

@if(backpack_user()->hasRole([
\App\Models\User::ADMIN_ROLE,
\App\Models\User::DIRECTOR_ROLE,
\App\Models\User::TECHNOLOGIST_ROLE,
\App\Models\User::MASTER_ROLE,
]))
<x-backpack::menu-item title="{{trans('model/furnace.furnaces')}}" icon="la la-fire-alt"
                       :link="backpack_url('furnace')"/>

<x-backpack::menu-item title="{{trans('model/alloy_grade.alloy_grades')}}" icon="la la-trademark"
                       :link="backpack_url('alloy-grade')"/>

<x-backpack::menu-item title="{{trans('model/charge.charges')}}" icon="la la-database"
                       :link="backpack_url('charge')"/>

<x-backpack::menu-item title="{{trans('model/technical_condition.technical_conditions')}}" icon="la la-copyright"
                       :link="backpack_url('technical-condition')"/>

<x-backpack::menu-item title="{{trans('model/diameter.diameter')}}" icon="la la-ban"
                       :link="backpack_url('diameter')"/>

<x-backpack::menu-item title="{{trans('model/mark.mark')}}" icon="la la-pen"
                       :link="backpack_url('mark')"/>

<x-backpack::menu-item title="{{trans('model/product_type.product_type')}}" icon="la la-box"
                       :link="backpack_url('product-type')"/>


<x-backpack::menu-item title="{{trans('model/material.materials')}}" icon="la la-atom"
                       :link="backpack_url('material')"/>

<x-backpack::menu-item title="{{trans('model/additional_work.additional_works')}}" icon="la la-gas-pump"
                       :link="backpack_url('additional-work')"/>

<x-backpack::menu-item title="{{trans('model/energy_parameter.energy_parameters')}}" icon="la la-charging-station"
                       :link="backpack_url('energy-parameter')"/>
@endif

@if(backpack_user()->hasRole([
\App\Models\User::ADMIN_ROLE,
\App\Models\User::MASTER_ROLE,
\App\Models\User::DIRECTOR_ROLE,
\App\Models\User::TECHNOLOGIST_ROLE,
\App\Models\User::LABORATORY_ROLE,
]))
    <x-backpack::menu-item title="{{trans('model/test_type.test_types')}}" icon="la la-vial"
                           :link="backpack_url('test-type')"/>
@endif
